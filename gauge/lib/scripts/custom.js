//function vc_prettyPhoto() { }  // Disable Visual Composer prettyPhoto override

jQuery( document ).ready( function( $ ) {

	'use strict';

	/*--------------------------------------------------------------
	Screen size class
	--------------------------------------------------------------*/

	function gpScreenSizeClass() {
	
		if ( $( window ).width() <= 767 && $( 'body' ).hasClass( 'gp-responsive' ) ) {
		
			$( 'body' ).addClass( 'gp-mobile' ).removeClass( 'gp-desktop' ).removeClass( 'gp-tablet-portrait' ).removeClass( 'gp-tablet-landscape' );
			
		} else if ( $( window ).width() <= 1023 && $( 'body' ).hasClass( 'gp-responsive' ) ) {
			
			$( 'body' ).addClass( 'gp-tablet-portrait' ).removeClass( 'gp-desktop' ).removeClass( 'gp-tablet-landscape' ).removeClass( 'gp-mobile' );
			
		} else if ( $( window ).width() <= 1082 && $( 'body' ).hasClass( 'gp-responsive' ) ) {
			
			$( 'body' ).addClass( 'gp-tablet-landscape' ).removeClass( 'gp-desktop' ).removeClass( 'gp-tablet-portrait' ).removeClass( 'gp-mobile' );	
		
		} else {
			
			$( 'body' ).addClass( 'gp-desktop' ).removeClass( 'gp-tablet-landscape' ).removeClass( 'gp-tablet-portrait' ).removeClass( 'gp-mobile' );
		
		}
		
	}
	
	gpScreenSizeClass();
	$( window ).resize( gpScreenSizeClass );
	

	/*--------------------------------------------------------------
	Retina images
	--------------------------------------------------------------*/

	if ( $( 'body' ).hasClass( 'gp-retina' ) ) {
	
		window.devicePixelRatio >= 2 && $( '.gp-post-thumbnail img' ).each( function() {
			$( this ).attr( { src: $( this ).attr( 'data-rel' ) } );
		});
	
		window.devicePixelRatio >= 2 && $( '.image-loop img' ).each(function() {
			$( this ).attr( { 'data-original': $( this ).attr( 'data-rel' ) } );
		});

		window.devicePixelRatio >= 2 && $( '.gp-slider .slide-image' ).each(function(){
			var bgImage = $(this).attr( 'data-rel' );
			$( this ).css( 'background-image', 'url(' + bgImage + ')' );
		});
	
	}
	

	/*--------------------------------------------------------------
	Parallax Effect
	--------------------------------------------------------------*/

	if( $( 'div' ).hasClass( 'gp-parallax' ) || $( 'header' ).hasClass( 'gp-parallax' ) ) {
		$( '.gp-parallax' ).css( 'opacity', 0 );		
		$( window ).load( function() {
			$.stellar({
				responsive: true,
				horizontalScrolling: false
			});
			$( '.gp-parallax' ).css( 'opacity', 1 );
		});
	}
			
			
	/*--------------------------------------------------------------
	Blog masonry
	--------------------------------------------------------------*/

	if ( $( '.gp-blog-wrapper' ).hasClass( 'gp-blog-masonry' ) ) {
	
		var container = $( '.gp-blog-masonry .gp-inner-loop' ),
			element = container;

		if ( container.find( 'img' ).length == 0 ) {
			element = $( '<img />' );
		}
			
		imagesLoaded( element, function( instance ) {

			container.isotope({
				itemSelector: 'section',
				percentPosition: true,
				masonry: {
					columnWidth: container.find( 'section' )[0],
					gutter: '.gp-gutter-size'
				}
			});

			container.animate( { 'opacity': 1 }, 1300 );
			$( '.gp-pagination' ).animate( { 'opacity': 1 }, 1300 );

		});
				
	}

	
	/*--------------------------------------------------------------
	Portfolio masonry
	--------------------------------------------------------------*/
		
	if ( $( '#gp-portfolio' ).hasClass( 'gp-portfolio-wrapper' ) ) {
	
		var container = $( '#gp-portfolio .gp-inner-loop' ),
			element = container;

		if ( container.find( 'img' ).length == 0 ) {
			element = $( '<img />' );
		}

		if ( container.find( '.gp-portfolio-item' ).length == 1 ) {
			var columnwidth = '.gp-portfolio-item';
		} else {
			var columnwidth = '.gp-portfolio-item:nth-child(3n)';
		}	

		imagesLoaded( element, function( instance ) {

			container.isotope({
				itemSelector: '.gp-portfolio-item',
				percentPosition: true,
				filter: '*',
				masonry: {
					columnWidth: columnwidth,
					gutter: '.gp-gutter-size'
				}
			});

			container.animate( { 'opacity': 1 }, 1300 );
			$( '.gp-pagination' ).animate( { 'opacity': 1 }, 1300 );

		});

		// Add portfolio filters
		$( '#gp-portfolio-filters ul li a' ).click( function() {

			var selector = $( this ).attr( 'data-filter' );
			container.isotope( { filter: selector } );

			$( '#gp-portfolio-filters ul li a' ).removeClass( 'gp-active' );
			$( this ).addClass( 'gp-active' );

			return false;

		});
		
		// Remove portfolio filters not found on current page
		if ( $( 'div' ).hasClass( 'gp-portfolio-filters' ) ) {

			var isotopeCatArr = [];
			var $portfolioCatCount = 0;
			$( '#gp-portfolio-filters ul li' ).each( function( i ) {
				if ( $( this ).find( 'a' ).length > 0 ) {
					isotopeCatArr[$portfolioCatCount] = $( this ).find( 'a' ).attr( 'data-filter' ).substring( 1 );	
					$portfolioCatCount++;
				}
			});

			isotopeCatArr.shift();

			var itemCats = '';

			$( '#gp-portfolio .gp-inner-loop > .gp-portfolio-item' ).each( function( i ) {
				itemCats += $( this ).attr( 'data-portfolio-cat' );
			});
			itemCats = itemCats.split( ' ' );

			itemCats.pop();

			itemCats = $.unique( itemCats );

			var notFoundCats = [];
			$.grep( isotopeCatArr, function( el ) {
				if ( $.inArray(el, itemCats ) == -1 ) {
					notFoundCats.push( el  );
				}
			});

			if ( notFoundCats.length != 0 ) {
				$( '#gp-portfolio-filters ul li' ).each( function() {
					if ( $( this ).find( 'a' ).length > 0 ) {
						if( $.inArray( $( this ).find( 'a' ).attr( 'data-filter' ).substring( 1 ), notFoundCats ) != -1 ) {
							$( this ).hide();
						}
					}
				});
			}

		}

	}
	
	
	/*--------------------------------------------------------------
	Lazyload Images
	--------------------------------------------------------------*/

	if ( $( 'div' ).hasClass( 'gp-images-lazyload-wrapper' ) ) {

		$( this ).find( '.gp-image-loop img' ).lazyload({
			 effect: 'fadeIn'
		});
	
		$( '.gp-images-lazyload-wrapper').animate( { 'opacity': 1 }, 1300 );

	}
	

	/*--------------------------------------------------------------
	Mega/Tab/Content Menu Width
	--------------------------------------------------------------*/

	/*REMOVED since v6.5
	function gpMenuWidth() {
	
		var logoWidth = $( '#gp-logo' ).outerWidth( true );

		function gpScrollLogoWidth( width ) {
			if ( $( 'body' ).hasClass( 'gp-boxed-layout' ) && $( 'body' ).hasClass( 'gp-header-centered' ) ) {
				var menuLeftMargin = 40;	
			} else if ( $( 'body' ).hasClass( 'gp-boxed-layout' ) ) {
				var menuLeftMargin = width + 40;	
			} else if ( $( 'body' ).hasClass( 'gp-header-centered' ) ) {
				var menuLeftMargin = 0;
			} else {
				var menuLeftMargin = width;
			}
			$( '#gp-main-nav ul > li.megamenu, #gp-main-nav ul > li.content-menu, #gp-main-nav ul > li.tab-content-menu' ).each( function() {
				var navItem = $( this );
				if ( $( navItem ).find( 'ul' ).length > 0 ) {	
					navItem.find( '.sub-menu.menu-depth-1' ).css({ 'left': -menuLeftMargin });
				}	
			});						
		}		
						
		// Before Scrolling
		if ( $( window ).scrollTop() > 50 && $( 'body' ).hasClass( 'gp-desktop' ) && $( 'body' ).hasClass( 'gp-fixed-header' ) && $( 'body' ).hasClass( 'gp-header-resize' ) ) {
			gpScrollLogoWidth( parseInt( logoWidth ) / ghostpool_script.headerSizeReduction );
		} else {
			gpScrollLogoWidth( parseInt( logoWidth ) );
		}
				
		// Upon Scrolling		
		$( window ).scroll( function() {
			if ( $( window ).scrollTop() > 50 && $( 'body' ).hasClass( 'gp-desktop' ) && $( 'body' ).hasClass( 'gp-fixed-header' ) && $( 'body' ).hasClass( 'gp-header-resize' ) ) {
				gpScrollLogoWidth( parseInt( logoWidth ) / ghostpool_script.headerSizeReduction );		
			} else {
				gpScrollLogoWidth( parseInt( logoWidth ) );
			}	
		});	
	
	}
	
	$( window ).load(function() { gpMenuWidth(); });
	$( window ).resize( gpMenuWidth );
	*/
	
		
	/*--------------------------------------------------------------
	Switch navigation position if near edge
	--------------------------------------------------------------*/

	function gpSwitchNavPosition() {
		$( '#gp-main-nav .menu > li.standard-menu, #gp-right-top-nav .menu > li' ).each( function() {
			$( this ).on( 'mouseenter mouseleave', function(e) {
				if ( $( this ).find( 'ul' ).length > 0 ) {
					var menuElement = $( 'ul:first', this ),
						pageWrapper = $( '#gp-page-wrapper' ),
						pageWrapperOffset = pageWrapper.offset(),
						menuOffset = menuElement.offset(),
						menuLeftOffset = menuOffset.left - pageWrapperOffset.left,
						menuWidth = menuElement.width() + 260,	
						pageWrapperWidth = pageWrapper.width(),
						isEntirelyVisible = ( menuLeftOffset + menuWidth <= pageWrapperWidth );	
					if ( ! isEntirelyVisible ) {
						$( this ).addClass( 'gp-nav-edge' );
					} else {
						$( this ).removeClass( 'gp-nav-edge' );
					}
				}   
			});
		});	
	}

	gpSwitchNavPosition();
	$( window ).resize( gpSwitchNavPosition );

		
	/*--------------------------------------------------------------
	Mega menus text/image support
	--------------------------------------------------------------*/
		
	if ( $( '.megamenu' ).length > 0 ) {
		
		$( '.menu-text > a' ).contents().unwrap().wrap( '<span></span>' );
			
		$( '.gp-nav .megamenu .sub-menu .sub-menu li.menu-image' ).each( function() {
			if ( $( this ).find( 'a' ).length > 0 ) {	
				var src = $( this ).find( 'a' ).attr( 'href' );
				$( '<img class="gp-menu-image" alt="">' ).insertAfter( $( this ).children( ':first' ) );
				$( this ).find( '.gp-menu-image' ).attr( 'src', src );
				$( this ).find( 'a' ).remove();				
			}			
		});
	
		$( '#gp-mobile-nav .menu-image' ).hide();
	
	}

		
	/*--------------------------------------------------------------
	FontAwesome menu icons
	--------------------------------------------------------------*/
		
	$( '.menu li.fa' ).each( function() {	
		var all = $( this ).attr( 'class' ).split(' ');
		for ( var i = 0; i < all.length; ++i ) {
			var cls = all[i];
			if ( cls.indexOf( 'fa' ) == 0 ) {
				$( this ).find( '> a:first-child' ).addClass( cls );
				$( this ).removeClass( cls );
			}
		}
	});
		

	/*--------------------------------------------------------------
	Dropdown menu icons
	--------------------------------------------------------------*/
		
	$( '#gp-main-nav .menu > li' ).each( function() {
		if ( $( this ).find( 'ul' ).length > 0 ) {	
			$( '<i class="gp-dropdown-icon gp-primary-dropdown-icon fa fa-caret-down" />' ).appendTo( $( this ).children( ':first' ) );		
		}		
	});
	
	$( '#gp-main-nav .menu > li.standard-menu ul > li' ).each( function() {
		if ( $( this ).find( 'ul' ).length > 0 ) {	
			$( '<i class="gp-dropdown-icon gp-secondary-dropdown-icon fa" />' ).appendTo( $( this ).children( ':first' ) );
		}					
	});		
		
									
	/*--------------------------------------------------------------
	Slide up/down header mobile navigation
	--------------------------------------------------------------*/

	function gpHeaderMobileNav() {
		$( '#gp-mobile-nav-button' ).click( function() {
			$( 'body' ).addClass( 'gp-mobile-nav-active' );
		});
		
		$( '#gp-mobile-nav-close-button, #gp-mobile-nav-bg' ).click( function() {
			$( 'body' ).removeClass( 'gp-mobile-nav-active' );
		});		
	}
	
	gpHeaderMobileNav();


	/*--------------------------------------------------------------
	Slide up/down header mobile dropdown menus
	--------------------------------------------------------------*/

	$( '#gp-mobile-nav .menu li' ).each( function() {
		if ( $( this ).find( 'ul' ).length > 0 ) {
			$( '<i class="gp-mobile-dropdown-icon" />' ).insertAfter( $( this ).children( ':first' ) );		
		}		
	});
	
	function gpHeaderMobileTopNav() {

		$( '#gp-mobile-nav ul > li' ).each( function() {
			
			var navItem = $( this );
			
			if ( $( navItem ).find( 'ul' ).length > 0 ) {	
		
				$( navItem ).children( '.gp-mobile-dropdown-icon' ).toggle( function() {
					$( navItem ).addClass( 'gp-active' );
					$( navItem ).children( '.sub-menu' ).stop().slideDown()
					$( '#gp-mobile-nav' ).addClass( 'gp-auto-height' );
				}, function() {
					$( navItem ).removeClass( 'gp-active' );
					$( navItem ).children( '.sub-menu' ).stop().slideUp();
				});
		
			}
					
		});
	
	}
	
	gpHeaderMobileTopNav();

	
	/*--------------------------------------------------------------
	Slide up/down hub tabs mobile navigation
	--------------------------------------------------------------*/

	function gpTabsMobileNav() {
		$( '#gp-hub-tabs #gp-hub-tabs-mobile-nav-button' ).toggle( function() {
			$( '#gp-hub-tabs ul' ).stop().slideDown();
			$( '#gp-hub-tabs-mobile-nav-button' ).addClass( 'gp-active' );
		}, function() {
			$( '#gp-hub-tabs ul' ).stop().slideUp();
			$( '#gp-hub-tabs-mobile-nav-button' ).removeClass( 'gp-active' );
		});	
	}
	
	gpTabsMobileNav();
			
			
	/*--------------------------------------------------------------
	Smooth scroll
	--------------------------------------------------------------*/

	if ( $( 'body' ).hasClass( 'gp-smooth-scrolling' ) && $( window ).width() > 767 && $( 'body' ).outerHeight( true ) > $( window ).height() ) {
		$( 'html' ).niceScroll({
			cursorcolor: '#424242',
			scrollspeed: 100,
			mousescrollstep: 40,
			cursorwidth: 10,
			cursorborder: '0',
			zindex: 10000,
			cursoropacitymin: 0.3,
			cursoropacitymax: 0.6
		});
	}
	
	
	/*--------------------------------------------------------------
	Back to top button
	--------------------------------------------------------------*/

	if ( $( 'body' ).hasClass( 'gp-back-to-top' ) ) {
		$().UItoTop({ 
			containerID: 'gp-to-top',
			containerHoverID: 'gp-to-top-hover',
			text: '<i class="fa fa-chevron-up"></i>',
			scrollSpeed: 600
		});
	}
		

	/*--------------------------------------------------------------
	prettyPhoto lightbox
	--------------------------------------------------------------*/

	if ( ghostpool_script.lightbox != 'disabled' ) {
		$( 'a.prettyphoto, a[data-rel^="prettyPhoto"]' ).prettyPhoto({
			hook: 'data-rel',
			theme: 'pp_default',
			deeplinking: false,
			social_tools: '',
			default_width: '768'
		});
	}	

	
	/*--------------------------------------------------------------
	Remove gallery shortcode when gallery slider active 
	--------------------------------------------------------------*/

	$( '.gallery-slider-active .gallery' ).remove();


	/*--------------------------------------------------------------
	Follow Items
	--------------------------------------------------------------*/

	$( '.gp-follow-button .gp-unfollow-item' ).show();
	
	$( document ).on( 'click', '.gp-follow-link', function() {
		var dhis = $( this );
		wpfp_do_js( dhis, 1 );
		if ( dhis.hasClass( 'gp-unfollow-item' ) ) {
			dhis.parent( 'section' ).fadeOut();
		}
		return false;
	});

	function wpfp_do_js( dhis, doAjax ) {
		dhis.addClass( 'gp-follow-loading' );
		var url = document.location.href.split( '#' )[0];
		var params = dhis.attr( 'href' ).replace( '?', '' ) + '&ajax=1';
		if ( doAjax ) {
			jQuery.get( url, params, function( data ) {
					dhis.parent().html( data );
					if ( typeof wpfp_after_ajax == 'function' ) {
						wpfp_after_ajax( dhis );
					}
					dhis.removeClass( 'gp-follow-loading' );
				}
			);
		}
	}
	
	
	/*--------------------------------------------------------------
	Title header video
	--------------------------------------------------------------*/
	
	if ( $( '.gp-page-header' ).hasClass( 'gp-has-video' ) ) {
		headerVideo.init({
			mainContainer: $( '.gp-page-header' ),
			videoContainer: $( '.gp-video-header' ),
			header: $( '.gp-video-media' ),
			videoTrigger: $( '.gp-play-video-button' ),
			closeButton: $( '.gp-close-video-button' ),
			autoPlayVideo: false
		});
	}
		
		
	/*--------------------------------------------------------------
	Resize header
	--------------------------------------------------------------*/

	function gpResizeHeader() {

		// Before Scrolling
		
		if ( ( $( window ).width() <= 982 && $( 'body' ).hasClass( 'gp-responsive' ) ) || ( $( window ).scrollTop() > 0 && $( 'body' ).hasClass( 'gp-desktop' ) && $( 'body' ).hasClass( 'gp-fixed-header' ) && $( 'body' ).hasClass( 'gp-header-resize' ) ) ) {

			$( 'body' ).addClass( 'gp-scrolling' );

			$( '.gp-header-standard #gp-main-header' ).data( 'size', 'small' ).addClass( 'gp-header-small' ).removeClass( 'gp-header-large' ).stop().css({
				height: parseInt( ghostpool_script.smallHeaderHeight )
			});
					
			$( '.gp-header-centered #gp-main-header' ).data( 'size', 'small' ).addClass( 'gp-header-small' ).removeClass( 'gp-header-large' ).stop().css({
				height: parseInt( ghostpool_script.smallHeaderHeight ) + parseInt( ghostpool_script.logoMarginTop ) / ghostpool_script.headerSizeReduction + parseInt( ghostpool_script.logoMarginBottom ) / ghostpool_script.headerSizeReduction
			});
														
			$( '#gp-main-header #gp-logo' ).stop().css({
				marginTop: parseInt( ghostpool_script.logoMarginTop ) / ghostpool_script.headerSizeReduction,
				marginBottom: parseInt( ghostpool_script.logoMarginBottom ) / ghostpool_script.headerSizeReduction
			}); 
		
			$( '#gp-main-header #gp-logo img' ).stop().css({
				height: parseInt( ghostpool_script.logoHeight ) / ghostpool_script.headerSizeReduction
			}); 
			
					
			$( '#gp-fixed-header-padding' ).stop().css({
				paddingTop: parseInt( ghostpool_script.smallHeaderHeight )
			});
				
		} else {
		
			$( 'body' ).removeClass( 'gp-scrolling' );

			$( '.gp-header-standard #gp-main-header' ).data( 'size', 'large' ).addClass( 'header-large' ).removeClass( 'gp-header-small' ).stop().css({
				height: ghostpool_script.headerHeight
			});
					
			$( '.gp-header-centered #gp-main-header' ).data( 'size', 'large' ).addClass( 'header-large' ).removeClass( 'gp-header-small' ).stop().css({
				height: parseInt( ghostpool_script.headerHeight ) + parseInt( ghostpool_script.logoMarginTop ) + parseInt( ghostpool_script.logoMarginBottom )
			});
														 
			$( '#gp-main-header #gp-logo' ).stop().css({
				marginTop: parseInt( ghostpool_script.logoMarginTop ),
				marginBottom: parseInt( ghostpool_script.logoMarginBottom )
			}); 
		
			$( '#gp-main-header #gp-logo img' ).stop().css({
				height: parseInt( ghostpool_script.logoHeight )
			});
			
		
			$( '#gp-fixed-header-padding' ).stop().css({
				paddingTop: parseInt( ghostpool_script.headerHeight )
			});
									
		}
					
		// Upon Scrolling

		$( window ).scroll( function() {
		
			if ( $( 'body' ).hasClass( 'gp-desktop' ) && $( 'body' ).hasClass( 'gp-fixed-header' ) && $( 'body' ).hasClass( 'gp-header-resize' ) ) {

				if ( $( document ).scrollTop() > 50 ) {

					if ( $( '#gp-main-header' ).data( 'size' ) == 'large' )  {
					
						$( 'body' ).addClass( 'gp-scrolling' );
	
						$( '.gp-header-standard #gp-main-header' ).data( 'size', 'small' ).addClass( 'gp-header-small' ).removeClass( 'header-large' ).stop().animate({
							height: parseInt( ghostpool_script.smallHeaderHeight )
						}, 300 );
						
						$( '.gp-header-centered #gp-main-header' ).data( 'size', 'small' ).addClass( 'gp-header-small' ).removeClass( 'header-large' ).stop().animate({
							height: parseInt( ghostpool_script.smallHeaderHeight ) + ( parseInt( ghostpool_script.logoMarginTop ) / ghostpool_script.headerSizeReduction ) + ( parseInt( ghostpool_script.logoMarginBottom ) / ghostpool_script.headerSizeReduction )
						}, 300 );
															
						$( '#gp-main-header #gp-logo' ).stop().animate({
							marginTop: parseInt( ghostpool_script.logoMarginTop ) / ghostpool_script.headerSizeReduction,
							marginBottom: parseInt( ghostpool_script.logoMarginBottom ) / ghostpool_script.headerSizeReduction
						}, 300 ); 
			
						$( '#gp-main-header #gp-logo img' ).stop().animate({
							height: parseInt( ghostpool_script.logoHeight ) / ghostpool_script.headerSizeReduction
						}, 300 ); 
				
						
						$( '#gp-fixed-header-padding' ).stop().animate({
							paddingTop: parseInt( ghostpool_script.smallHeaderHeight )
						}, 400 );
						
					}

				} else {

					if ( $( '#gp-main-header' ).data( 'size' ) == 'small' ) {
					
						$( 'body' ).removeClass( 'gp-scrolling' );
	
						$( '.gp-header-standard #gp-main-header' ).data( 'size', 'large' ).addClass( 'gp-header-large' ).removeClass( 'gp-header-small' ).stop().animate({
							height: ghostpool_script.headerHeight
						}, 300 );
						
						$( '.gp-header-centered #gp-main-header' ).data( 'size', 'large' ).addClass( 'gp-header-large' ).removeClass( 'gp-header-small' ).stop().animate({
							height: parseInt( ghostpool_script.headerHeight ) + parseInt( ghostpool_script.logoMarginTop ) + parseInt( ghostpool_script.logoMarginBottom )
						}, 300 );
															 
						$( '#gp-main-header #gp-logo' ).stop().animate({
							marginTop: parseInt( ghostpool_script.logoMarginTop ),
							marginBottom: parseInt( ghostpool_script.logoMarginBottom )
						}, 300 ); 
			
						$( '#gp-main-header #gp-logo img' ).stop().animate({
							height: parseInt( ghostpool_script.logoHeight )
						}, 300 );
				
			
						$( '#gp-fixed-header-padding' ).stop().animate({
							paddingTop: parseInt( ghostpool_script.headerHeight )
						}, 400 );
							 
					}
				
				}
			
			}

		});				

	}

	gpResizeHeader();
	$( window ).resize( gpResizeHeader );


	/*--------------------------------------------------------------
	Set slider height to auto 
	--------------------------------------------------------------*/

	$( window ).load( function() {
		if ( $( window ).width() > 1082 ) {
			$( '#gp-homepage-slider' ).css( 'height', 'auto' );	
		}
	});


	/*--------------------------------------------------------------
	Featured wrapper caption
	--------------------------------------------------------------*/

	$( '.gp-featured-post' ).css( 'opacity', 1 );	
	
	function gpCaptionPadding() {
		var padding = ( $( '#gp-featured-wrapper .gp-container' ).width() - $( '#gp-main-header .gp-container' ).width() ) / 2;
		$( '.gp-large-post .gp-featured-caption' ).css( 'padding-left', padding );
		$( '.gp-small-posts .gp-featured-caption' ).css( 'padding-right', padding );	 	
	}

	gpCaptionPadding();
	$( window ).resize( gpCaptionPadding );


	/*--------------------------------------------------------------
	Close reset success message
	--------------------------------------------------------------*/

	$( '#gp-close-reset-message' ).click( function() {
		$( '#gp-reset-message' ).remove();
	});
	
	
	/*--------------------------------------------------------------
	Remove "|" from BuddyPress item options
	--------------------------------------------------------------*/

	$( '.item-options' ).contents().filter( function() {
		return this.nodeType == 3;
	}).remove();


	/*--------------------------------------------------------------
	Hide BuddyPress item options if width too small
	--------------------------------------------------------------*/

	function gpBPWidgetOptions() {
		
		$( '.widget.buddypress' ).each( function() {
			
			var widget = $( this ),
				optionsWidth = 230,
				widgettitle = widget.find( '.widgettitle' ).html(),
				textWidth = widget.find( '.gp-widget-title' ).width(),
				containerWidth = widget.find( '.widgettitle' ).width();

			if ( ( containerWidth - optionsWidth ) > textWidth ) {
				widget.find( '.item-options' ).removeClass( 'gp-small-item-options' );
				widget.find( '.gp-item-options-button' ).remove();
			} else {	
				widget.find( '.item-options' ).addClass( 'gp-small-item-options' );
				widget.find( '.item-options' ).append( '<div class="gp-item-options-button"></div>' );
			}
			
			widget.find( '.gp-item-options-button' ).toggle( function() {
				widget.find( '.gp-small-item-options' ).addClass( 'gp-active' );
			}, function() {
				widget.find( '.gp-small-item-options' ).removeClass( 'gp-active' );
			});		
						
		});
		
	}
	
	gpBPWidgetOptions();
	$( window ).resize( gpBPWidgetOptions );

	
	/*--------------------------------------------------------------
	BuddyPress tabs for mobile
	--------------------------------------------------------------*/			
						
	$( '.item-list-tabs:not(#subnav)' ).prepend( '<div id="gp-bp-tabs-button"></div>' );
	var bptabs = $( '.item-list-tabs:not(#subnav) > ul' );
	
	function gpBPTabs() {

		if ( $( '.item-list-tabs:not(#subnav)' ).find( 'ul' ).length > 0 ) {	

			if ( $( window ).width() <= 567 && $( 'body' ).hasClass( 'gp-responsive' ) ) {
	
				$( bptabs ).hide();

				$( '#gp-bp-tabs-button' ).toggle( function() {
					$( bptabs ).stop().slideDown();
					$( this ).addClass( 'gp-active' );
				}, function() {
					$( bptabs ).stop().slideUp();
					$( this ).removeClass( 'gp-active' );
				});
		
			} else {
		
				$( bptabs ).css( 'height', 'auto' ).show();
		
			}
		
		}
						
	}
	
	gpBPTabs();
	$( window ).resize( gpBPTabs );
	

	/*--------------------------------------------------------------
	WooCommerce image overlay 
	--------------------------------------------------------------*/

	$( '.gp-product-image-container .gp-image-overlay' ).css( 'opacity', 0 ).hover( function() {
		$( this ).fadeTo( 'fast', 1 );
	}, function() {
		$( this ).fadeTo( 'fast', 0 );
	});	


});
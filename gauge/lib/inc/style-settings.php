<?php

/*--------------------------------------------------------------
Custom Classes
--------------------------------------------------------------*/

if ( !function_exists( 'ghostpool_body_classes' ) ) {
	function ghostpool_body_classes( $gp_classes ) {
		
		global $gp, $post;
			
		$gp_classes[] = 'gp-theme';
		$gp_classes[] = $gp['theme_layout'];
		$gp_classes[] = $gp['responsive'];
		$gp_classes[] = $gp['retina'];
		$gp_classes[] = $gp['smooth_scrolling'];
		$gp_classes[] = $gp['back_to_top'];
		$gp_classes[] = $gp['fixed_header'];	
		$gp_classes[] = $gp['header_resize'];
		$gp_classes[] = $gp['header_layout'];
		$gp_classes[] = $gp['header_overlay'];
		$gp_classes[] = $gp['top_header'];
		$gp_classes[] = $gp['cart_button'];
		$gp_classes[] = 'gp-search-' . $gp['search'];
		$gp_classes[] = $GLOBALS['ghostpool_page_header'];
		$gp_classes[] = $GLOBALS['ghostpool_layout'];

		if ( ( is_singular() && ( get_post_meta( $post->post_parent, '_wp_page_template', true ) == 'hub-template.php' OR get_post_meta( $post->post_parent, '_wp_page_template', true ) == 'hub-review-template.php' ) ) && ! is_archive() && ! is_search() ) { 		
			$gp_classes[] = 'gp-hub-child-page';
		}
						
		return $gp_classes;
		
	}
}
add_filter( 'body_class', 'ghostpool_body_classes' );


/*--------------------------------------------------------------
Inline Styling
--------------------------------------------------------------*/

global $gp;
					
$custom_css = '';

if ( $gp['top_header'] == 'gp-top-header' ) {
	$gp_top_header = 40;
} else {
	$gp_top_header = '';
}

if ( get_post_meta( get_the_ID(), 'flexslider_dimensions', true ) ) {
	$gp_flexslider_height = get_post_meta( get_the_ID(), 'flexslider_dimensions', true );
	$gp_flexslider_height = $gp_flexslider_height['height'];
} else {
	$gp_flexslider_height = '450';
}

if ( get_post_meta( get_the_ID(), 'flexslider_mobile_dimensions', true ) ) {	
	$gp_flexslider_mobile_height = get_post_meta( get_the_ID(), 'flexslider_mobile_dimensions', true );
	$gp_flexslider_mobile_height = $gp_flexslider_mobile_height['height'];
} else {
	$gp_flexslider_mobile_height = '200';
}

$custom_css .= '
#gp-fixed-header-padding{padding-top: ' . $gp['main_header_height']['height'] . 'px;}
#gp-logo img{width: ' . $gp['logo_dimensions']['width'] . 'px; height: ' . $gp['logo_dimensions']['height'] . 'px;}
.gp-page-header .gp-container{padding-top: ' . $gp['title_padding']['padding-top'] . 'px;padding-bottom: ' . $gp['title_padding']['padding-bottom'] . 'px;}
.gp-active{color: ' . $gp['general_link']['hover'] . ';}
.gp-score-spinner{
background: ' . $gp['low_rating_gradient']['to'] . ';
background: -moz-linear-gradient(' . $gp['low_rating_gradient']['to'] . ' 0%,' . $gp['low_rating_gradient']['from'] . '70%);
background: -webkit-gradient(color-stop(0%,' . $gp['low_rating_gradient']['to'] . ' ), color-stop(70%,' . $gp['low_rating_gradient']['from'] . ' ));
background: -webkit-linear-gradient(' . $gp['low_rating_gradient']['to'] . '  0%,' . $gp['low_rating_gradient']['from'] . '  70%);
background: -o-linear-gradient(' . $gp['low_rating_gradient']['to'] . '  0%,' . $gp['low_rating_gradient']['from'] . '  70%);
background: -ms-linear-gradient(' . $gp['low_rating_gradient']['to'] . '  0%,' . $gp['low_rating_gradient']['from'] . ' 70%);
background: linear-gradient(' . $gp['low_rating_gradient']['to'] . '  0%,' . $gp['low_rating_gradient']['from'] . ' 70%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="' . $gp['low_rating_gradient']['from'] . '", endColorstr="' . $gp['low_rating_gradient']['to'] . '",GradientType=1 );
}
.gp-no-score-clip-1 .gp-score-spinner{
background: ' . $gp['low_rating_gradient']['to'] . ';
}
.gp-no-score-clip-2 .gp-score-filler{
background: ' . $gp['low_rating_gradient']['to'] . ';
background: -moz-linear-gradient(' . $gp['low_rating_gradient']['from'] . ' 0%,' . $gp['low_rating_gradient']['to'] . '70%);
background: -webkit-gradient(color-stop(0%,' . $gp['low_rating_gradient']['from'] . ' ), color-stop(70%,' . $gp['low_rating_gradient']['to'] . ' ));
background: -webkit-linear-gradient(' . $gp['low_rating_gradient']['from'] . '  0%,' . $gp['low_rating_gradient']['to'] . '  70%);
background: -o-linear-gradient(' . $gp['low_rating_gradient']['from'] . '  0%,' . $gp['low_rating_gradient']['to'] . '  70%);
background: -ms-linear-gradient(' . $gp['low_rating_gradient']['from'] . '  0%,' . $gp['low_rating_gradient']['to'] . ' 70%);
background: linear-gradient(' . $gp['low_rating_gradient']['from'] . '  0%,' . $gp['low_rating_gradient']['to'] . ' 70%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="' . $gp['low_rating_gradient']['to'] . '", endColorstr="' . $gp['low_rating_gradient']['from'] . '",GradientType=1 );
}
select{background-color: ' . $gp['input_bg']['background-color'] . ';}
.gp-responsive #gp-sidebar{border-color: ' . $gp['input_border']['border-color'] . ';}
.gp-slider .gp-slide-image {
height: ' . $gp_flexslider_height . 'px;
}' .

'.gp-theme #buddypress .activity-list .activity-content blockquote a{color: ' . $gp['general_link']['regular'] . '}' . 

'.gp-theme #buddypress .activity-list .activity-content blockquote a:hover{color: ' . $gp['general_link']['hover'] . '}' . 

'.gp-wide-layout.gp-header-standard .gp-nav .menu li.megamenu > .sub-menu, .gp-wide-layout.gp-header-standard .gp-nav .menu li.tab-content-menu .sub-menu, .gp-wide-layout.gp-header-standard .gp-nav .menu li.content-menu .sub-menu{left: -' . ( $gp['logo_dimensions']['width'] ) . 'px;}' .

'.gp-scrolling.gp-wide-layout.gp-header-standard .gp-nav .menu li.megamenu > .sub-menu, .gp-scrolling.gp-wide-layout.gp-header-standard .gp-nav .menu li.tab-content-menu .sub-menu, .gp-scrolling.gp-wide-layout.gp-header-standard .gp-nav .menu li.content-menu .sub-menu{left: -' . ( $gp['logo_dimensions']['width'] / $gp['header_size_reduction'] ) . 'px;}' .

'.gp-boxed-layout.gp-header-standard .gp-nav .menu li.megamenu > .sub-menu, .gp-boxed-layout.gp-header-standard .gp-nav .menu li.tab-content-menu .sub-menu, .gp-boxed-layout.gp-header-standard .gp-nav .menu li.content-menu .sub-menu{left: -' . ( $gp['logo_dimensions']['width'] + 40 ) . 'px;}' .

'.gp-scrolling.gp-boxed-layout.gp-header-standard .gp-nav .menu li.megamenu > .sub-menu, .gp-scrolling.gp-boxed-layout.gp-header-standard .gp-nav .menu li.tab-content-menu .sub-menu, .gp-scrolling.gp-boxed-layout.gp-header-standard .gp-nav .menu li.content-menu .sub-menu{left: -' . ( ( $gp['logo_dimensions']['width'] / $gp['header_size_reduction'] ) + 40 ) . 'px;}';

$custom_css .= '@media only screen and (max-width: 1023px) {
	.gp-responsive #gp-main-header {height: ' . round( $gp['main_header_height']['height'] / $gp['header_size_reduction'] ) . 'px!important;}
	.gp-responsive #gp-fixed-header-padding {padding-top: ' . round( $gp['main_header_height']['height'] / $gp['header_size_reduction'] ) . 'px!important;}
	.gp-responsive #gp-logo {margin: ' . round( $gp['logo_spacing']['margin-top'] / $gp['header_size_reduction'] ) . 'px ' . round( $gp['logo_spacing']['margin-right'] / $gp['header_size_reduction'] ) . 'px ' . round( $gp['logo_spacing']['margin-bottom'] / $gp['header_size_reduction'] ) . 'px ' . round( $gp['logo_spacing']['margin-left'] / $gp['header_size_reduction'] ) . 'px; width: ' . round( $gp['logo_dimensions']['width'] / $gp['header_size_reduction'] ) . 'px; height: ' . round( $gp['logo_dimensions']['height'] / $gp['header_size_reduction'] ) . 'px;}
	.gp-responsive #gp-logo img {width: ' . round( $gp['logo_dimensions']['width'] / $gp['header_size_reduction'] ) . 'px; height: ' . ( $gp['logo_dimensions']['height'] / $gp['header_size_reduction'] ) . 'px;}
	.gp-responsive .gp-page-header .gp-container {
	padding-top: ' .  round( $gp['title_padding']['padding-top'] / $gp['header_size_reduction'] ) . 'px;
	padding-bottom: ' .  round( $gp['title_padding']['padding-bottom'] / $gp['header_size_reduction'] ) . 'px;
	}
}
@media only screen and (max-width: 767px) {
	.gp-responsive .gp-slider .gp-slide-image {
	height: ' . $gp_flexslider_mobile_height . 'px !important;
	}	
}	
@media only screen and (max-width: 320px) {
	.gp-responsive.gp-theme .woocommerce div.product .woocommerce-tabs ul.tabs li.active a,.gp-responsive.gp-theme .woocommerce #gp-content div.product .woocommerce-tabs ul.tabs li.active a,.gp-responsive.gp-theme.woocommerce-page div.product .woocommerce-tabs ul.tabs li.active a,.gp-responsive.gp-theme.woocommerce-page #gp-content div.product .woocommerce-tabs ul.tabs li.active a {border-color: ' . $gp['input_border']['border-color'] . ';}}
	hr,.gp-theme .woocommerce .widget_price_filter .price_slider_wrapper .ui-widget-content,.gp-theme.woocommerce-page .widget_price_filter .price_slider_wrapper .ui-widget-content {background: ' . $gp['input_border']['border-color'] . ';
}';
	
	
/*--------------------------------------------------------------
Theme Widths
--------------------------------------------------------------*/

// Larger Desktop - Wide
if ( $gp['theme_layout'] == 'gp-wide-layout' && ( $gp['desktop_wide_container']['width'] != '1170' OR $gp['desktop_wide_content']['width'] != '810' OR $gp['desktop_wide_sidebar']['width'] != '330' ) ) {
	$custom_css .= '@media only screen and (min-width: 1201px) {';
		if ( $gp['desktop_wide_container']['width'] != '1170') {
			$custom_css .= '.gp-container,.gp-fullwidth .vc_col-sm-12.wpb_column > .wpb_wrapper > .wpb_row,.gp-fullwidth .vc_col-sm-12.wpb_column > .wpb_wrapper > .wpb_accordion,.gp-fullwidth .vc_col-sm-12.wpb_column > .wpb_wrapper > .wpb_tabs,.gp-fullwidth .vc_col-sm-12.wpb_column > .wpb_wrapper > .wpb_tour,.gp-fullwidth .vc_col-sm-12.wpb_column > .wpb_wrapper > .wpb_teaser_grid,.gp-slide-caption{width: ' . $gp['desktop_wide_container']['width'] . 'px;}
			.gp-slide-caption{margin-left: -' . round( $gp['desktop_wide_container']['width'] / 2 ) . 'px;}
			.gp-hub-header-info{width:' . round( $gp['desktop_wide_container']['width'] / 2 ) . 'px;}';
		}	
		if ( $gp['desktop_wide_content']['width'] != '810') {
			$custom_css .= '#gp-content,.gp-top-sidebar #gp-review-content{width: ' . $gp['desktop_wide_content']['width'] . 'px;}';
		}	
		if ( $gp['desktop_wide_sidebar']['width'] != '330') {
			$custom_css .= '#gp-sidebar{width: ' . $gp['desktop_wide_sidebar']['width'] . 'px;}';
		}	
	$custom_css .= '}';
}

// Larger Desktop - Boxed
if ( $gp['theme_layout'] == 'gp-boxed-layout' && ( $gp['desktop_boxed_page']['width'] != '1170' OR $gp['desktop_boxed_container']['width'] != '1090' OR $gp['desktop_boxed_content']['width'] != '730' OR $gp['desktop_boxed_sidebar']['width'] != '330' ) ) {
	$custom_css .= '@media only screen and (min-width: 1201px) {';
		if ( $gp['desktop_boxed_page']['width'] != '1170' ) {
			$custom_css .= '.gp-boxed-layout #gp-page-wrapper,.gp-boxed-layout #gp-main-header,.gp-boxed-layout #gp-top-header{width: ' . $gp['desktop_boxed_page']['width'] . 'px;}';
		}	
		if ( $gp['desktop_boxed_container']['width'] != '1090') {
			$custom_css .= '.gp-boxed-layout .gp-container,.gp-boxed-layout .gp-side-bg-gradient-overlay,.gp-boxed-layout.gp-fullwidth .vc_col-sm-12.wpb_column > .wpb_wrapper > .wpb_row,.gp-boxed-layout.gp-fullwidth .vc_col-sm-12.wpb_column > .wpb_wrapper > .wpb_accordion,.gp-boxed-layout.gp-fullwidth .vc_col-sm-12.wpb_column > .wpb_wrapper > .wpb_tabs,.gp-boxed-layout.gp-fullwidth .vc_col-sm-12.wpb_column > .wpb_wrapper > .wpb_tour,.gp-boxed-layout.gp-fullwidth .vc_col-sm-12.wpb_column > .wpb_wrapper > .wpb_teaser_grid,.gp-boxed-layout .gp-slide-caption{width: ' . $gp['desktop_boxed_container']['width'] . 'px;}
			.gp-boxed-layout .gp-slide-caption{margin-left: -' . round( $gp['desktop_boxed_container']['width'] / 2 ) . 'px;}
			.gp-boxed-layout .gp-hub-header-info{width:' . round( $gp['desktop_boxed_container']['width'] / 2 ) . 'px;}';
		}	
		if ( $gp['desktop_boxed_content']['width'] != '730' ) {
			$custom_css .= '.gp-boxed-layout #gp-content,.gp-boxed-layout .gp-top-sidebar #gp-review-content{width: ' . $gp['desktop_boxed_content']['width'] . 'px;}';
		}	
		if ( $gp['desktop_boxed_sidebar']['width'] != '330' ) {
			$custom_css .= '.gp-boxed-layout #gp-sidebar{width: ' . $gp['desktop_boxed_sidebar']['width'] . 'px;}';
		}	
	$custom_css .= '}';
}	

// Smaller Desktop - Wide
if ( $gp['theme_layout'] == 'gp-wide-layout' && ( $gp['sm_desktop_wide_container']['width'] != '1040' OR $gp['sm_desktop_wide_content']['width'] != '680' OR $gp['sm_desktop_wide_sidebar']['width'] != '330' ) ) {
	$custom_css .= '@media only screen and (max-width: 1200px) and (min-width: 1083px) {';
		if ( $gp['sm_desktop_wide_container']['width'] != '1040' ) {
			$custom_css .= '.gp-responsive .gp-container,.gp-responsive.gp-fullwidth .vc_col-sm-12.wpb_column > .wpb_wrapper > .wpb_row,.gp-responsive.gp-fullwidth .vc_col-sm-12.wpb_column > .wpb_wrapper > .wpb_accordion,.gp-responsive.gp-fullwidth .vc_col-sm-12.wpb_column > .wpb_wrapper > .wpb_tabs,.gp-responsive.gp-fullwidth .vc_col-sm-12.wpb_column > .wpb_wrapper > .wpb_tour,.gp-responsive.gp-fullwidth .vc_col-sm-12.wpb_column > .wpb_wrapper > .wpb_teaser_grid,.gp-responsive .gp-slide-caption{width: ' . $gp['sm_desktop_wide_container']['width'] . 'px;}
			.gp-responsive .gp-slide-caption{margin-left: -' . round( $gp['sm_desktop_wide_container']['width'] / 2 ) . 'px;}
			.gp-responsive .gp-hub-header-info{width:' . round( $gp['sm_desktop_wide_container']['width'] / 2 ) . 'px;}';
		}	
		if ( $gp['sm_desktop_wide_content']['width'] != '680' ) {
			$custom_css .= '.gp-responsive #gp-content,.gp-responsive .gp-top-sidebar #gp-review-content{width: ' . $gp['sm_desktop_wide_content']['width'] . 'px;}';
		}	
		if ( $gp['sm_desktop_wide_sidebar']['width'] != '330' ) {
			$custom_css .= '.gp-responsive #gp-sidebar,.gp-responsive.gp-no-sidebar #gp-user-rating-wrapper,.gp-responsive.gp-fullwidth #gp-user-rating-wrapper{width: ' . $gp['sm_desktop_wide_sidebar']['width'] . 'px;}';
		}	
	$custom_css .= '}';
}

// Smaller Desktop - Boxed
if ( $gp['theme_layout'] == 'gp-boxed-layout' && ( $gp['sm_desktop_boxed_page']['width'] != '1040' OR $gp['sm_desktop_boxed_container']['width'] != '960' OR $gp['sm_desktop_boxed_content']['width'] != '600' OR $gp['sm_desktop_boxed_sidebar']['width'] != '330' ) ) {
	$custom_css .= '@media only screen and (max-width: 1200px) and (min-width: 1083px) {';
		if ( $gp['sm_desktop_boxed_page']['width'] != '1040') {
			$custom_css .= '.gp-responsive.gp-boxed-layout #gp-page-wrapper,.gp-responsive.gp-boxed-layout #gp-main-header,.gp-responsive.gp-boxed-layout #gp-top-header{width: ' . $gp['sm_desktop_boxed_page']['width'] . 'px;}';
		}	
		if ( $gp['sm_desktop_boxed_container']['width'] != '960' ) {
			$custom_css .= '.gp-responsive.gp-boxed-layout .gp-container,.gp-responsive.gp-boxed-layout .gp-side-bg-gradient-overlay,.gp-responsive.gp-boxed-layout.gp-fullwidth .vc_col-sm-12.wpb_column > .wpb_wrapper > .wpb_row,.gp-responsive.gp-boxed-layout.gp-fullwidth .vc_col-sm-12.wpb_column > .wpb_wrapper > .wpb_accordion,.gp-responsive.gp-boxed-layout.gp-fullwidth .vc_col-sm-12.wpb_column > .wpb_wrapper > .wpb_tabs,.gp-responsive.gp-boxed-layout.gp-fullwidth .vc_col-sm-12.wpb_column > .wpb_wrapper > .wpb_tour,.gp-responsive.gp-boxed-layout.gp-fullwidth .vc_col-sm-12.wpb_column > .wpb_wrapper > .wpb_teaser_grid,.gp-responsive.gp-boxed-layout .gp-slide-caption{width: ' . $gp['sm_desktop_boxed_container']['width'] . 'px;}
			.gp-responsive.gp-boxed-layout .gp-slide-caption{margin-left: -' . round( $gp['sm_desktop_boxed_container']['width'] / 2 ) . 'px;}
			.gp-responsive.gp-boxed-layout .gp-hub-header-info{width:' . round( $gp['sm_desktop_boxed_container']['width'] / 2 ) . 'px;}';
		}	
		if ( $gp['sm_desktop_boxed_content']['width'] != '600' ) {
			$custom_css .= '.gp-responsive.gp-boxed-layout #gp-content,.gp-responsive.gp-boxed-layout .gp-top-sidebar #gp-review-content{width: ' . $gp['sm_desktop_boxed_content']['width'] . 'px;}';
		}	
		if ( $gp['sm_desktop_boxed_sidebar']['width'] != '330' ) {
			$custom_css .= '.gp-responsive.gp-boxed-layout #gp-sidebar{width: ' . $gp['sm_desktop_boxed_sidebar']['width'] . 'px;}';
		}	
	$custom_css .= '}';
}	

// Tablet (Landscape) - Wide
if ( $gp['theme_layout'] == 'gp-wide-layout' && ( $gp['tablet_wide_container']['width'] != '980' OR $gp['tablet_wide_content']['width'] != '630' OR $gp['tablet_wide_sidebar']['width'] != '330' ) ) {
	$custom_css .= '@media only screen and (max-width: 1082px) and (min-width: 1024px) {';
		if ( $gp['tablet_wide_container']['width'] != '980' ) {
			$custom_css .= '.gp-responsive .gp-container,.gp-responsive.gp-fullwidth .vc_col-sm-12.wpb_column > .wpb_wrapper > .wpb_row,.gp-responsive.gp-fullwidth .vc_col-sm-12.wpb_column > .wpb_wrapper > .wpb_accordion,.gp-responsive.gp-fullwidth .vc_col-sm-12.wpb_column > .wpb_wrapper > .wpb_tabs,.gp-responsive.gp-fullwidth .vc_col-sm-12.wpb_column > .wpb_wrapper > .wpb_tour,.gp-responsive.gp-fullwidth .vc_col-sm-12.wpb_column > .wpb_wrapper > .wpb_teaser_grid,.gp-responsive .gp-slide-caption{width: ' . $gp['tablet_wide_container']['width'] . 'px;}
			.gp-responsive .gp-slide-caption{margin-left: -' . round( $gp['tablet_wide_container']['width'] / 2 ) . 'px;}
			.gp-responsive .hub-header-info{width:' . round( $gp['tablet_wide_container']['width'] / 2 ) . 'px;}';
		}
		if ( $gp['tablet_wide_content']['width'] != '630' ) {
			$custom_css .= '.gp-responsive #gp-content,.gp-responsive .gp-top-sidebar #gp-review-content{width: ' . $gp['tablet_wide_content']['width'] . 'px;}';
		}	
		if ( $gp['tablet_wide_sidebar']['width'] != '330' ) {
			$custom_css .= '.gp-responsive #gp-sidebar {width: ' . $gp['tablet_wide_sidebar']['width'] . 'px;}';
		}	
	$custom_css .= '}';
}	

// Tablet (Landscape) - Boxed
if ( $gp['theme_layout'] == 'gp-boxed-layout' && ( $gp['tablet_boxed_page']['width'] != '980' OR $gp['tablet_boxed_container']['width'] != '900' OR $gp['tablet_boxed_content']['width'] != '630' OR $gp['tablet_boxed_sidebar']['width'] != '330' ) ) {
	$custom_css .= '@media only screen and (max-width: 1082px) and (min-width: 1024px) {';
		if ( $gp['tablet_boxed_page']['width'] != '980' ) {
			$custom_css .= '.gp-responsive.gp-boxed-layout #gp-page-wrapper,.gp-responsive.gp-boxed-layout #gp-main-header,.gp-responsive.gp-boxed-layout #gp-top-header{width: ' . $gp['tablet_boxed_page']['width'] . 'px;}';
		}
		if ( $gp['tablet_boxed_container']['width'] != '900' ) {
			$custom_css .= '.gp-responsive.gp-boxed-layout .gp-container,.gp-responsive.gp-boxed-layout .gp-side-bg-gradient-overlay,.gp-responsive.gp-boxed-layout.gp-fullwidth .vc_col-sm-12.wpb_column > .wpb_wrapper > .wpb_row,.gp-responsive.gp-boxed-layout.gp-fullwidth .vc_col-sm-12.wpb_column > .wpb_wrapper > .wpb_accordion,.gp-responsive.gp-boxed-layout.gp-fullwidth .vc_col-sm-12.wpb_column > .wpb_wrapper > .wpb_tabs,.gp-responsive.gp-boxed-layout.gp-fullwidth .vc_col-sm-12.wpb_column > .wpb_wrapper > .wpb_tour,.gp-responsive.gp-boxed-layout.gp-fullwidth .vc_col-sm-12.wpb_column > .wpb_wrapper > .wpb_teaser_grid,	.gp-responsive.gp-boxed-layout .gp-slide-caption{width: ' . $gp['tablet_boxed_container']['width'] . 'px;}
			.gp-responsive.gp-boxed-layout .gp-slide-caption{margin-left: -' . round( $gp['tablet_boxed_container']['width'] / 2 ) . 'px;}
			.gp-responsive.gp-boxed-layout .gp-hub-header-info{width:' . round( $gp['tablet_boxed_container']['width'] / 2 ) . 'px;}';
		}		
		if ( $gp['tablet_boxed_content']['width'] != '630' ) {
			$custom_css .= '.gp-responsive.gp-boxed-layout #gp-content,.gp-responsive.gp-boxed-layout .gp-top-sidebar #gp-review-content{width: ' . $gp['tablet_boxed_content']['width'] . 'px;}';
		}	
		if ( $gp['tablet_boxed_sidebar']['width'] != '330' ) {
			$custom_css .= '.gp-responsive.gp-boxed-layout #gp-sidebar {width: ' . $gp['tablet_boxed_sidebar']['width'] . 'px;}';
		}	
	$custom_css .= '}';
}	


/*--------------------------------------------------------------
Custom CSS
--------------------------------------------------------------*/

if ( isset( $gp['custom_css'] ) && ! empty( $gp['custom_css'] ) ) {
	$custom_css .= $gp['custom_css'];
}

if ( ! empty( $custom_css ) ) {
	echo '<style>' . $custom_css . '</style>';
}

?>
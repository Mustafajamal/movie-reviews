<?php
/*
Version: 1.6.2
Author: Huseyin Berberoglu
Author URI: http://nxsn.com
*/

define( 'GHOSTPOOL_META_KEY', '_gp_followers' );
define( 'GHOSTPOOL_USER_OPTION_KEY', 'ghostpool_user_options' );
define( 'GHOSTPOOL_COOKIE_KEY', 'gp-follow-items' );

// Manage default privacy of users followed items lists by adding this constant to wp-config.php
if ( ! defined( 'GHOSTPOOL_DEFAULT_PRIVACY_SETTING' ) ) {
    define( 'GHOSTPOOL_DEFAULT_PRIVACY_SETTING', false );
}

$gp_ajax_mode = 1;

function ghostpool_follow_items() {
    if ( isset( $_REQUEST['wpfpaction'] ) ) {
        global $gp_ajax_mode;
        $gp_ajax_mode = isset( $_REQUEST['ajax'] ) ? $_REQUEST['ajax'] : false;
        if ( $_REQUEST['wpfpaction'] == 'add' ) {
            ghostpool_follow();
        } elseif ( $_REQUEST['wpfpaction'] == 'remove' ) {
            ghostpool_unfollow();
        } elseif ( $_REQUEST['wpfpaction'] == 'clear' ) {
            if ( ghostpool_clear_followed() ) {
            	ghostpool_die_or_go( '<strong class="gp-no-items-found">' . esc_html__( 'All items removed', 'gauge' ) . '</strong>' );
            } else {
            	ghostpool_die_or_go( 'Error' );
            }	
        }
    }
}
add_action( 'wp_loaded', 'ghostpool_follow_items' );

function ghostpool_follow( $gp_post_id = '' ) {
	global $gp;
    if ( empty( $gp_post_id ) ) $gp_post_id = $_REQUEST['postid'];
    if ( $gp['hub_following_items'] == 'members' && ! is_user_logged_in() ) {
        ghostpool_die_or_go( esc_html__( 'Only registered users can follow items.', 'gauge' ) );
        return false;
    }

    if ( ghostpool_do_add_to_list( $gp_post_id  ) ) {
        do_action( 'ghostpool_after_add', $gp_post_id );
        ghostpool_update_post_meta( $gp_post_id, 1 );   	    
		$gp_str = ghostpool_follow_button( 1, 'remove', 0, array( 'post_id' => $gp_post_id ) );
		ghostpool_die_or_go( $gp_str );
    }
}

function ghostpool_do_add_to_list( $gp_post_id ) {
    if ( ghostpool_check_followed( $gp_post_id ) )
        return false;
    if ( is_user_logged_in() ) {
        return ghostpool_add_to_usermeta( $gp_post_id );
    } else {
        return ghostpool_set_cookie( $gp_post_id, 'added' );
    }
}

function ghostpool_unfollow( $gp_post_id = '' ) {
    if ( empty( $gp_post_id ) ) $gp_post_id = $_REQUEST['postid'];
    if ( ghostpool_do_unfollow( $gp_post_id ) ) {
        do_action( 'ghostpool_after_remove', $gp_post_id );
        ghostpool_update_post_meta( $gp_post_id, -1 );
		if ( isset( $_REQUEST['page'] ) && $_REQUEST['page'] == 1 ) {
			$gp_str = '';
		} else {
			$gp_str = ghostpool_follow_button( $gp_post_id, 1, 'add', 0, array( 'post_id' => $gp_post_id ) );
		}
		ghostpool_die_or_go( $gp_str );
     
    }
    else return false;
}

function ghostpool_die_or_go( $gp_str ) {
    global $gp_ajax_mode;
    if ( $gp_ajax_mode) {
        die( $gp_str );
    } else {
        wp_redirect( $_SERVER['HTTP_REFERER'] );
    }
}

function ghostpool_add_to_usermeta( $gp_post_id ) {
    $gp_followed = ghostpool_get_user_meta();
    $gp_followed[] = $gp_post_id;
    ghostpool_update_user_meta( $gp_followed );
    return true;
}

function ghostpool_check_followed( $gp_cid ) {
    if ( is_user_logged_in() ) {
        $gp_following_page_ids = ghostpool_get_user_meta();
        if ( $gp_following_page_ids )
            foreach ( $gp_following_page_ids as $gp_fpost_id )
                if ( $gp_fpost_id == $gp_cid ) return true;
	} else {
	    if ( ghostpool_get_cookie() ) :
	        foreach ( ghostpool_get_cookie() as $gp_fpost_id => $gp_val )
	            if ( $gp_fpost_id == $gp_cid ) return true;
	    endif;
	}
    return false;
}

function ghostpool_follow_button( $gp_post_id, $gp_return = 0, $gp_action = '', $show_span = 1, $gp_args = array() ) {

    extract( $gp_args );
   
    $gp_str = '<span class="gp-follow-button">';

		if ( $gp_action == 'remove' ) :
			$gp_str .= ghostpool_follow_button_html( $gp_post_id, esc_html__( 'Unfollow', 'gauge' ), 'remove' );
		elseif ( $gp_action == 'add' ) :
			$gp_str .= ghostpool_follow_button_html( $gp_post_id, esc_html__( 'Follow', 'gauge' ), 'add' );
		elseif ( ghostpool_check_followed( $gp_post_id ) ) :
			$gp_str .= ghostpool_follow_button_html( $gp_post_id, esc_html__( 'Unfollow', 'gauge' ), 'remove' );
		else:
			$gp_str .= ghostpool_follow_button_html( $gp_post_id, esc_html__( 'Follow', 'gauge' ), 'add' );
		endif;
    
    $gp_str .= '</span>';

    if ( $gp_return ) {
    	return $gp_str;
    } else {
    	echo html_entity_decode( $gp_str );
    }
}

function ghostpool_follow_button_html( $gp_post_id, $gp_opt, $gp_action ) {
    $gp_link = "<a class='gp-follow-link gp-follow-item' href='?wpfpaction=".$gp_action."&amp;postid=". $gp_post_id . "' title='". $gp_opt ."' rel='nofollow'>" . ghostpool_loader() . $gp_opt . "</a>";
    $gp_link = apply_filters( 'ghostpool_follow_button_html', $gp_link );
    return $gp_link;
}

function ghostpool_get_users_following( $gp_user = '' ) {
    $gp_following_page_ids = array();

    if ( !empty( $gp_user ) ) :
        return ghostpool_get_user_meta( $gp_user );
    endif;

    // Collect favorites from cookie and if user is logged in from database
    if ( is_user_logged_in() ) :
        $gp_following_page_ids = ghostpool_get_user_meta();
	else:
	    if ( ghostpool_get_cookie() ) :
	        foreach ( ghostpool_get_cookie() as $gp_post_id => $gp_post_title ) {
	            array_push( $gp_following_page_ids, $gp_post_id );
	        }
	    endif;
	endif;
    return $gp_following_page_ids;
}

function ghostpool_list_follow_items( $gp_args = array() ) {
    $gp_user = isset( $_REQUEST['user'] ) ? $_REQUEST['user'] : '';
    extract( $gp_args );
    global $gp_following_page_ids;
    if ( !empty( $gp_user  ) ) {
        if ( ghostpool_is_user_favlist_public( $gp_user ) )
            $gp_following_page_ids = ghostpool_get_users_following( $gp_user );

    } else {
        $gp_following_page_ids = ghostpool_get_users_following();
    }

}

function ghostpool_list_most_followed( $limit = 5 ) {
    global $wpdb;
    $gp_query = "SELECT post_id, meta_value, post_status FROM $wpdb->postmeta";
    $gp_query .= " LEFT JOIN $wpdb->posts ON post_id=$wpdb->posts.ID";
    $gp_query .= " WHERE post_status='publish' AND meta_key='" . GHOSTPOOL_META_KEY . "' AND meta_value > 0 ORDER BY ROUND( meta_value ) DESC LIMIT 0, $limit";
    $gp_results = $wpdb->get_results( $gp_query );
    if ( $gp_results ) {
        echo "<ul>";
        foreach ( $gp_results as $gp_o ) :
            $gp_p = get_post( $gp_o->post_id );
            echo '<li>';
            echo "<a href='" . get_permalink( $gp_o->post_id ) . "' title='" . $gp_p->post_title . "'>" . $gp_p->post_title . "</a> ( $gp_o->meta_value )";
            echo "</li>";
        endforeach;
        echo "</ul>";
    }
}

function ghostpool_loader() {
    return '<span class="fa-spin gp-follow-loader"></span>';
}

function ghostpool_clear_followed() {
    if ( ghostpool_get_cookie() ) :
        foreach ( ghostpool_get_cookie() as $gp_post_id => $gp_val ) {
            ghostpool_set_cookie( $gp_post_id, '' );
            ghostpool_update_post_meta( $gp_post_id, -1 );
        }
    endif;
    if ( is_user_logged_in() ) {
        $gp_following_page_ids = ghostpool_get_user_meta();
        if ( $gp_following_page_ids ) :
            foreach ( $gp_following_page_ids as $gp_post_id ) {
                ghostpool_update_post_meta( $gp_post_id, -1 );
            }
        endif;
        if ( !delete_user_meta( ghostpool_get_user_id(), GHOSTPOOL_META_KEY ) ) {
            return false;
        }
    }
    return true;
}

function ghostpool_do_unfollow( $gp_post_id ) {
    if ( ! ghostpool_check_followed( $gp_post_id ) )
        return true;

    $gp_a = true;
    if ( is_user_logged_in() ) {
        $gp_user_following = ghostpool_get_user_meta();
        $gp_user_following = array_diff( $gp_user_following, array( $gp_post_id ) );
        $gp_user_following = array_values( $gp_user_following );
        $gp_a = ghostpool_update_user_meta( $gp_user_following );
    }
    if ( $gp_a ) $gp_a = ghostpool_set_cookie( $_REQUEST['postid'], '' );
    return $gp_a;
}

function ghostpool_update_user_meta( $gp_arr ) {
    return update_user_meta( ghostpool_get_user_id(),GHOSTPOOL_META_KEY, $gp_arr );
}

function ghostpool_update_post_meta( $gp_post_id, $gp_val ) {
	$gp_oldval = ghostpool_get_post_meta( $gp_post_id );
	if ( $gp_val == -1 && $gp_oldval == 0 ) {
    	$gp_val = 0;
	} else {
		$gp_val = $gp_oldval + $gp_val;
	}
    return add_post_meta( $gp_post_id, GHOSTPOOL_META_KEY, $gp_val, true ) or update_post_meta( $gp_post_id, GHOSTPOOL_META_KEY, $gp_val );
}

function ghostpool_delete_post_meta( $gp_post_id ) {
    return delete_post_meta( $gp_post_id, GHOSTPOOL_META_KEY );
}

function ghostpool_get_cookie() {
    if ( !isset( $_COOKIE[GHOSTPOOL_COOKIE_KEY] ) ) return;
    return $_COOKIE[GHOSTPOOL_COOKIE_KEY];
}

function ghostpool_get_user_id() {
    $gp_current_user = wp_get_current_user();
    return $gp_current_user->ID;
}

function ghostpool_get_user_meta( $gp_user = '' ) {
    if ( !empty( $gp_user ) ) :
        $gp_userdata = get_user_by( 'login', $gp_user );
        $gp_user_id = $gp_userdata->ID;
        return get_user_meta( $gp_user_id, GHOSTPOOL_META_KEY, true );
    else:
        return get_user_meta( ghostpool_get_user_id(), GHOSTPOOL_META_KEY, true );
    endif;
}

function ghostpool_get_post_meta( $gp_post_id ) {
    $gp_val = get_post_meta( $gp_post_id, GHOSTPOOL_META_KEY, true );
    if ( $gp_val < 0 ) $gp_val = 0;
    return $gp_val;
}

function ghostpool_set_cookie( $gp_post_id, $gp_str ) {
    $gp_expire = time()+60*60*24*30;
    return setcookie( "gp-follow-items[$gp_post_id]", $gp_str, $gp_expire, "/" );
}

function ghostpool_is_user_favlist_public( $gp_user ) {
    $gp_user_opts = ghostpool_get_user_options( $gp_user );
    if ( empty( $gp_user_opts ) ) return GHOSTPOOL_DEFAULT_PRIVACY_SETTING;
    if ( $gp_user_opts['is_ghostpool_list_public'] )
        return true;
    else
        return false;
}

function ghostpool_get_user_options( $gp_user ) {
    $gp_userdata = get_user_by( 'login', $gp_user );
    $gp_user_id = $gp_userdata->ID;
    return get_user_meta( $gp_user_id, GHOSTPOOL_USER_OPTION_KEY, true );
}

function ghostpool_is_user_can_edit() {
    if ( isset( $_REQUEST['user'] ) && $_REQUEST['user'] )
        return false;
    return true;
}

function ghostpool_remove_follow_button( $gp_post_id ) {
    if ( ghostpool_is_user_can_edit() && ( is_page_template( 'following-template.php' ) OR ( isset( $_GET['type'] ) && $_GET['type'] == 'following-template' ) ) ) {
        $gp_link = "<a id='rem_$gp_post_id' class='gp-follow-link gp-unfollow-item button' href='?wpfpaction=remove&amp;page=1&amp;postid=" . $gp_post_id . "' title='". esc_html__( 'Remove', 'gauge' ) . "' rel='nofollow'>" . esc_html__( 'Remove', 'gauge' ) . "</a>";
        $gp_link = apply_filters( 'ghostpool_remove_follow_button', $gp_link );
        echo html_entity_decode( $gp_link );
    }
}

function ghostpool_clear_list_link() {
    if ( ghostpool_is_user_can_edit() ) {
        echo "<a class='gp-follow-link gp-unfollow-all-items button' href='?wpfpaction=clear' rel='nofollow'>" . ghostpool_loader() . esc_html__( 'Unfollow All Items', 'gauge' ) . "</a>";
    }
}

function ghostpool_cookie_warning() {
    if ( ! is_user_logged_in() && ! isset( $_GET['user'] ) ) {
        echo '<div class="gp-cookie-notice">' . esc_html__( 'If you clear your cookies these items will be deleted. Please login so you can save items to your account.', 'gauge' ) . '</div>';
   }
}

?>
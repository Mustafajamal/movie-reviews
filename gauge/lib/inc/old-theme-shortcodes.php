<?php

/*--------------------------------------------------------------
Custom Visual Composer Fields
--------------------------------------------------------------*/

// Icon Selection
function ghostpool_icon_selection( $gp_settings, $gp_value ) {
   $gp_output = '';
	foreach ( $gp_settings['value'] as $gp_val ) {		   
		$gp_output .= '<a href="' . esc_attr( $gp_val ) . '" class="gp-icon-link"><i class="fa fa-lg ' . esc_attr( $gp_val ) . '"></i></a>';		
	}
	$gp_output .= '<input name="' . esc_attr( $gp_settings['param_name'] ) . '" id="gp-icon-selection-value" class="wpb_vc_param_value ' . esc_attr( $gp_settings['param_name'] ) . ' ' . esc_attr( $gp_settings['type'] ) . '_field" type="hidden" value="' . esc_attr( $gp_value ) . '" />';    
	return $gp_output;
}
vc_add_shortcode_param( 'icon_selection', 'ghostpool_icon_selection', ghostpool_scripts_uri . 'icon-selection.js' );	


/*--------------------------------------------------------------
Old Custom Shortcodes
--------------------------------------------------------------*/

if ( ! function_exists( 'ghostpool_old_custom_shortcodes' ) ) {

	function ghostpool_old_custom_shortcodes() {

		$icons = array( '', 'fa-glass','fa-music','fa-search','fa-envelope-o','fa-heart','fa-star','fa-star-o','fa-user','fa-film','fa-th-large','fa-th','fa-th-list','fa-check','fa-times','fa-search-plus','fa-search-minus','fa-power-off','fa-signal','fa-cog','fa-trash-o','fa-home','fa-file-o','fa-clock-o','fa-road','fa-download','fa-arrow-circle-o-down','fa-arrow-circle-o-up','fa-inbox','fa-play-circle-o','fa-repeat','fa-refresh','fa-list-alt','fa-lock','fa-flag','fa-headphones','fa-volume-off','fa-volume-down','fa-volume-up','fa-qrcode','fa-barcode','fa-tag','fa-tags','fa-book','fa-bookmark','fa-print','fa-camera','fa-font','fa-bold','fa-italic','fa-text-height','fa-text-width','fa-align-left','fa-align-center','fa-align-right','fa-align-justify','fa-list','fa-outdent','fa-indent','fa-video-camera','fa-picture-o','fa-pencil','fa-map-marker','fa-adjust','fa-tint','fa-pencil-square-o','fa-share-square-o','fa-check-square-o','fa-arrows','fa-step-backward','fa-fast-backward','fa-backward','fa-play','fa-pause','fa-stop','fa-forward','fa-fast-forward','fa-step-forward','fa-eject','fa-chevron-left','fa-chevron-right','fa-plus-circle','fa-minus-circle','fa-times-circle','fa-check-circle','fa-question-circle','fa-info-circle','fa-crosshairs','fa-times-circle-o','fa-check-circle-o','fa-ban','fa-arrow-left','fa-arrow-right','fa-arrow-up','fa-arrow-down','fa-share','fa-expand','fa-compress','fa-plus','fa-minus','fa-asterisk','fa-exclamation-circle','fa-gift','fa-leaf','fa-fire','fa-eye','fa-eye-slash','fa-exclamation-triangle','fa-plane','fa-calendar','fa-random','fa-comment','fa-magnet','fa-chevron-up','fa-chevron-down','fa-retweet','fa-shopping-cart','fa-folder','fa-folder-open','fa-arrows-v','fa-arrows-h','fa-bar-chart-o','fa-twitter-square','fa-facebook-square','fa-camera-retro','fa-key','fa-cogs','fa-comments','fa-thumbs-o-up','fa-thumbs-o-down','fa-star-half','fa-heart-o','fa-sign-out','fa-linkedin-square','fa-thumb-tack','fa-external-link','fa-sign-in','fa-trophy','fa-github-square','fa-upload','fa-lemon-o','fa-phone','fa-square-o','fa-bookmark-o','fa-phone-square','fa-twitter','fa-facebook','fa-github','fa-unlock','fa-credit-card','fa-rss','fa-hdd-o','fa-bullhorn','fa-bell','fa-certificate','fa-hand-o-right','fa-hand-o-left','fa-hand-o-up','fa-hand-o-down','fa-arrow-circle-left','fa-arrow-circle-right','fa-arrow-circle-up','fa-arrow-circle-down','fa-globe','fa-wrench','fa-tasks','fa-filter','fa-briefcase','fa-arrows-alt','fa-users','fa-link','fa-cloud','fa-flask','fa-scissors','fa-files-o','fa-paperclip','fa-floppy-o','fa-square','fa-bars','fa-list-ul','fa-list-ol','fa-strikethrough','fa-underline','fa-table','fa-magic','fa-truck','fa-pinterest','fa-pinterest-square','fa-google-plus-square','fa-google-plus','fa-money','fa-caret-down','fa-caret-up','fa-caret-left','fa-caret-right','fa-columns','fa-sort','fa-sort-asc','fa-sort-desc','fa-envelope','fa-linkedin','fa-undo','fa-gavel','fa-tachometer','fa-comment-o','fa-comments-o','fa-bolt','fa-sitemap','fa-umbrella','fa-clipboard','fa-lightbulb-o','fa-exchange','fa-cloud-download','fa-cloud-upload','fa-user-md','fa-stethoscope','fa-suitcase','fa-bell-o','fa-coffee','fa-cutlery','fa-file-text-o','fa-building-o','fa-hospital-o','fa-ambulance','fa-medkit','fa-fighter-jet','fa-beer','fa-h-square','fa-plus-square','fa-angle-double-left','fa-angle-double-right','fa-angle-double-up','fa-angle-double-down','fa-angle-left','fa-angle-right','fa-angle-up','fa-angle-down','fa-desktop','fa-laptop','fa-tablet','fa-mobile','fa-circle-o','fa-quote-left','fa-quote-right','fa-spinner','fa-circle','fa-reply','fa-github-alt','fa-folder-o','fa-folder-open-o','fa-smile-o','fa-frown-o','fa-meh-o','fa-gamepad','fa-keyboard-o','fa-flag-o','fa-flag-checkered','fa-terminal','fa-code','fa-reply-all','fa-mail-reply-all','fa-star-half-o','fa-location-arrow','fa-crop','fa-code-fork','fa-chain-broken','fa-question','fa-info','fa-exclamation','fa-superscript','fa-subscript','fa-eraser','fa-puzzle-piece','fa-microphone','fa-microphone-slash','fa-shield','fa-calendar-o','fa-fire-extinguisher','fa-rocket','fa-maxcdn','fa-chevron-circle-left','fa-chevron-circle-right','fa-chevron-circle-up','fa-chevron-circle-down','fa-html5','fa-css3','fa-anchor','fa-unlock-alt','fa-bullseye','fa-ellipsis-h','fa-ellipsis-v','fa-rss-square','fa-play-circle','fa-ticket','fa-minus-square','fa-minus-square-o','fa-level-up','fa-level-down','fa-check-square','fa-pencil-square','fa-external-link-square','fa-share-square','fa-compass','fa-caret-square-o-down','fa-caret-square-o-up','fa-caret-square-o-right','fa-eur','fa-gbp','fa-usd','fa-inr','fa-jpy','fa-rub','fa-krw','fa-btc','fa-file','fa-file-text','fa-sort-alpha-asc','fa-sort-alpha-desc','fa-sort-amount-asc','fa-sort-amount-desc','fa-sort-numeric-asc','fa-sort-numeric-desc','fa-thumbs-up','fa-thumbs-down','fa-youtube-square','fa-youtube','fa-xing','fa-xing-square','fa-youtube-play','fa-dropbox','fa-stack-overflow','fa-instagram','fa-flickr','fa-adn','fa-bitbucket','fa-bitbucket-square','fa-tumblr','fa-tumblr-square','fa-long-arrow-down','fa-long-arrow-up','fa-long-arrow-left','fa-long-arrow-right','fa-apple','fa-windows','fa-android','fa-linux','fa-dribbble','fa-skype','fa-foursquare','fa-trello','fa-female','fa-male','fa-gittip','fa-sun-o','fa-moon-o','fa-archive','fa-bug','fa-vk','fa-weibo','fa-renren','fa-pagelines','fa-stack-exchange','fa-arrow-circle-o-right','fa-arrow-circle-o-left','fa-caret-square-o-left','fa-dot-circle-o','fa-wheelchair','fa-vimeo-square','fa-try','fa-plus-square-o','fa-angellist','fa-area-chart','fa-at','fa-bell-slash','fa-bell-slash-o','fa-bicycle','fa-binoculars','fa-birthday-cake','fa-bus','fa-calculator','fa-cc','fa-cc-amex','fa-cc-discover','fa-cc-mastercard','fa-cc-paypal','fa-cc-stripe','fa-cc-visa','fa-copyright','fa-eyedropper','fa-futbol-o','fa-google-wallet','fa-ils','fa-ioxhost','fa-lastfm','fa-lastfm-square','fa-line-chart','fa-meanpath','fa-newspaper-o','fa-paint-brush','fa-paypal','fa-pie-chart','fa-plug','fa-shekel','fa-sheqel','fa-slideshare','fa-soccer-ball-o','fa-toggle-off','fa-toggle-on','fa-trash','fa-tty','fa-twitch','fa-wifi','fa-yelp' );
		
		
		/*--------------------------------------------------------------
		Button Shortcode
		--------------------------------------------------------------*/

		require_once( ghostpool_vc . '_gp_vc_button.php' );

		vc_map( array( 
			'name' => esc_html__( 'Button', 'gauge' ),
			'base' => 'button',
			'description' => esc_html__( 'Add buttons to your page.', 'gauge' ),
			'class' => 'wpb_vc_button',
			'controls' => 'full',
			'icon' => 'gp-icon-button',
			'category' => esc_html__( 'Content', 'gauge' ),
			'params' => array( 	
				array( 
				'heading' => esc_html__( 'Button Text', 'gauge' ),
				'description' => esc_html__( 'The text in the button.', 'gauge' ),
				'param_name' => 'text',
				'holder' => 'span',
				'value' => 'Button Text',
				'type' => 'textfield'
				),
				array( 
				'heading' => esc_html__( 'Link', 'gauge' ),
				'description' => esc_html__( 'The link for the button.', 'gauge' ),
				'param_name' => 'link',
				'value' => '',
				'type' => 'textfield',
				),		
				array( 
				'heading' => esc_html__( 'Target', 'gauge' ),
				'description' => esc_html__( 'The link target for the button.', 'gauge' ),
				'param_name' => 'target',
				'value' => array( esc_html__( 'Same Window', 'gauge' ) => '', esc_html__( 'New Window', 'gauge' ) => '_blank' ),
				'type' => 'dropdown',
				'dependency' => array( 'element' => 'link', 'not_empty' => true )
				),
				array( 
				'heading' => esc_html__( 'Text Color', 'gauge' ),
				'description' => esc_html__( 'The text color for the button.', 'gauge' ),
				'param_name' => 'text_color',
				'value' => '#ffffff',
				'type' => 'colorpicker',
				),			
				array( 
				'heading' => esc_html__( 'Text Size', 'gauge' ),
				'param_name' => 'text_size',
				'description' => esc_html__( 'The text size of the button (supports px, em, %).', 'gauge' ),
				'value' => '14px',
				'type' => 'textfield',
				),			
				array( 
				'heading' => esc_html__( 'Background Color', 'gauge' ),
				'description' => esc_html__( 'The background color for the button.', 'gauge' ),
				'param_name' => 'background_color',
				'value' => '#23a1b8',
				'type' => 'colorpicker',
				),									
				array( 
				'heading' => esc_html__( 'Border Color', 'gauge' ),
				'description' => esc_html__( 'The border color for the button.', 'gauge' ),
				'param_name' => 'border_color',
				'value' => '',
				'type' => 'colorpicker',
				),									
				array( 
				'heading' => esc_html__( 'Border Radius', 'gauge' ),
				'param_name' => 'border_radius',
				'value' => '3px',
				'description' => esc_html__( 'The border radius of the button to create rounded corners (supports px, em, %).', 'gauge' ),
				'type' => 'textfield'
				),
				array( 
				'heading' => esc_html__( 'Button Alignment', 'gauge' ),
				'description' => esc_html__( 'The alignment of the button.', 'gauge' ),
				'param_name' => 'align',
				'value' => array( esc_html__( 'Align Left', 'gauge' ) => '', esc_html__( 'Align Center', 'gauge' ) => 'text-center', esc_html__( 'Align Right', 'gauge' ) => 'text-right' ),
				'type' => 'dropdown',
				),		
				array( 
				'heading' => esc_html__( 'Width', 'gauge' ),
				'param_name' => 'width',
				'value' => 'auto',
				'description' => esc_html__( 'The width of the button (supports px, em, %).', 'gauge' ),
				'type' => 'textfield'
				),
				array( 
				'heading' => esc_html__( 'Padding', 'gauge' ),
				'param_name' => 'padding',
				'value' => '14px 20px',
				'description' => esc_html__( 'The padding of the button (supports px, em, %).', 'gauge' ),
				'type' => 'textfield',
				),	
				array( 
				'heading' => esc_html__( 'Margins', 'gauge' ),
				'param_name' => 'margins',
				'value' => '',
				'description' => esc_html__( 'The margins of the button (supports px, em, %).', 'gauge' ),
				'type' => 'textfield',
				),		 		
				array( 
				'heading' => esc_html__( 'Icon', 'gauge' ),
				'param_name' => 'icon',
				'value' => $icons,
				'description' => esc_html__( 'The icon you want to display.', 'gauge' ),
				'type' => 'icon_selection',
				),				
				array( 
				'heading' => esc_html__( 'Extra Class Names', 'gauge' ),
				'param_name' => 'classes',
				'value' => '',
				'description' => esc_html__( 'If you wish to style this particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'gauge' ),
				'type' => 'textfield',
				),																																								
			 )
		) );


		/*--------------------------------------------------------------
		Icon Shortcode
		--------------------------------------------------------------*/

		require_once( ghostpool_vc . '_gp_vc_icon.php' );
		
		vc_map( array( 
			'name' => esc_html__( 'Icon', 'gauge' ),
			'base' => 'icon',
			'description' => esc_html__( 'Add icons to your page.', 'gauge' ),
			'class' => 'wpb_vc_icon',
			'controls' => 'full',
			'icon' => 'gp-icon-icon',
			'category' => esc_html__( 'Content', 'gauge' ),
			'params' => array( 	
				array( 
				'heading' => esc_html__( 'Icon', 'gauge' ),
				'param_name' => 'image',
				'value' => $icons,
				'admin_label' => true,
				'description' => esc_html__( 'The icon image you want to display.', 'gauge' ),
				'type' => 'icon_selection',
				),		
				array( 
				'heading' => esc_html__( 'Size', 'gauge' ),
				'param_name' => 'size',
				'value' => array( esc_html__( 'Medium', 'gauge' ) => 'medium', esc_html__( 'Extra Small', 'gauge' ) => 'xsmall', esc_html__( 'Small', 'gauge' ) => 'small', esc_html__( 'Large', 'gauge' ) => 'large', esc_html__( 'Extra Large', 'gauge' ) => 'xlarge' ),
				'description' => esc_html__( 'The size of the icon.', 'gauge' ),
				'type' => 'dropdown',
				),			
				array( 
				'heading' => esc_html__( 'Color', 'gauge' ),
				'param_name' => 'color',
				'value' => '#ffffff',
				'description' => esc_html__( 'The color of the icon.', 'gauge' ),
				'type' => 'colorpicker'
				),	
				array( 
				'heading' => esc_html__( 'Animation', 'gauge' ),
				'param_name' => 'animation',
				'value' => array( esc_html__( 'None', 'gauge' ) => 'no-animation', esc_html__( 'Slide Up', 'gauge' ) => 'slideUp', esc_html__( 'Slide Down', 'gauge' ) => 'slideDown', esc_html__( 'Slide Left', 'gauge' ) => 'slideLeft', esc_html__( 'Slide Right', 'gauge' ) => 'slideRight', esc_html__( 'Slide Expand Up', 'gauge' ) => 'slideExpandUp', esc_html__( 'Expand Up', 'gauge' ) => 'expandUp', esc_html__( 'Fade In', 'gauge' ) => 'fadeIn', esc_html__( 'Expand Open', 'gauge' ) => 'expandOpen', esc_html__( 'Big Entrance', 'gauge' ) => 'bigEntrance', esc_html__( 'Hatch', 'gauge' ) => 'hatch', esc_html__( 'Bounce', 'gauge' ) => 'bounce', esc_html__( 'Pulse', 'gauge' ) => 'pulse', esc_html__( 'Floating', 'gauge' ) => 'Floating', esc_html__( 'Tossing', 'gauge' ) => 'tossing', esc_html__( 'Pull Up', 'gauge' ) => 'pullUp', esc_html__( 'Pull Down', 'gauge' ) => 'pullDown', esc_html__( 'Stretch Left', 'gauge' ) => 'stretchLeft', esc_html__( 'Stretch Right', 'gauge' ) => 'stretchRight' ),
				'description' => esc_html__( 'The animation effect of the icon.', 'gauge' ),
				'type' => 'dropdown',
				),		
				array( 
				'heading' => esc_html__( 'Scroll Animation', 'gauge' ),
				'param_name' => 'scroll_animation',
				'value' => array( esc_html__( 'Disabled', 'gauge' ) => 'disabled', esc_html__( 'Enabled', 'gauge' ) => 'enabled' ),
				'description' => esc_html__( 'Choose whether the animation effect is activated when the element is scrolled into viewed.', 'gauge' ),
				'type' => 'dropdown',
				'dependency' => array( 'element' => 'animation', 'value' => array( 'slideUp', 'slideDown', 'slideLeft', 'slideRight', 'slideExpandUp', 'expandUp', 'fadeIn', 'expandOpen', 'bigEntrance', 'hatch' ) )
				),
				array( 
				'heading' => esc_html__( 'Delay', 'gauge' ),
				'param_name' => 'delay',
				'value' => '0.1',
				'description' => esc_html__( 'The number of seconds to delay the animation effect of the icon.', 'gauge' ),
				'type' => 'textfield',
				'dependency' => array( 'element' => 'animation', 'value' => array( 'slideUp', 'slideDown', 'slideLeft', 'slideRight', 'slideExpandUp', 'expandUp', 'fadeIn', 'expandOpen', 'bigEntrance', 'hatch' ) )		
				),		
				array( 
				'heading' => esc_html__( 'Spin Icon', 'gauge' ),
				'param_name' => 'spin',
				'value' => array( esc_html__( 'False', 'gauge' ) => 'false', esc_html__( 'True', 'gauge' ) => 'true' ),
				'description' => esc_html__( 'The icon spins around (useful for loading icons).', 'gauge' ),
				'type' => 'dropdown',
				),					
				array( 
				'heading' => esc_html__( 'Background Color 1', 'gauge' ),
				'param_name' => 'background_color_1',
				'value' => '#42caf6',
				'description' => esc_html__( 'The top background color of the icon.', 'gauge' ),
				'type' => 'colorpicker',
				),			
				array( 
				'heading' => esc_html__( 'Background Color 2', 'gauge' ),
				'param_name' => 'background_color_2',
				'value' => '#00c0ff',
				'description' => esc_html__( 'The bottom background color of the icon.', 'gauge' ),
				'type' => 'colorpicker'
				),
				array( 
				'heading' => esc_html__( 'Background Size', 'gauge' ),
				'param_name' => 'background_size',
				'value' => '98px',
				'description' => esc_html__( 'The width/height of the background of the icon (supports px, em, %).', 'gauge' ),
				'type' => 'textfield',
				),						
				array( 
				'heading' => esc_html__( 'Border Color', 'gauge' ),
				'param_name' => 'border_color',
				'value' => '',
				'description' => esc_html__( 'The border color of the icon.', 'gauge' ),
				'type' => 'colorpicker',
				),									
				array( 
				'heading' => esc_html__( 'Border Radius', 'gauge' ),
				'param_name' => 'border_radius',
				'value' => '100%',
				'description' => esc_html__( 'The border radius of the icon to create rounded corners (supports px, em, %).', 'gauge' ),
				'type' => 'textfield'
				),
				array( 
				'heading' => esc_html__( 'Margins', 'gauge' ),
				'param_name' => 'margins',
				'value' => '0',
				'description' => esc_html__( 'The margins of the icon (supports px, em, %).', 'gauge' ),
				'type' => 'textfield',
				),			
				array( 
				'heading' => esc_html__( 'Alignment', 'gauge' ),
				'param_name' => 'align',
				'value' => array( esc_html__( 'Center', 'gauge' ) => 'aligncenter', esc_html__( 'Left', 'gauge' ) => 'alignleft', esc_html__( 'Right', 'gauge' ) => 'alignright', esc_html__( 'None', 'gauge' ) => 'alignnone' ),
				'description' => esc_html__( 'The alignment of the icon.', 'gauge' ),
				'type' => 'dropdown',
				),				
				array( 
				'heading' => esc_html__( 'Extra Class Names', 'gauge' ),
				'param_name' => 'classes',
				'value' => '',
				'description' => esc_html__( 'If you wish to style this particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'gauge' ),
				'type' => 'textfield',
				),																																								
			 )
		 ) );


		/*--------------------------------------------------------------
		Single Image Shortcode (add-on)
		--------------------------------------------------------------*/

		vc_remove_param( 'vc_single_image', 'css_animation' ); 
		vc_remove_param( 'vc_single_image', 'alignment' ); 

		vc_add_param( 'vc_single_image', array( 
		'heading' => esc_html__( 'Animation', 'gauge' ),
		'param_name' => 'animation',
		'class' => '',
		'value' => array( esc_html__( 'None', 'gauge' ) => 'no-animation', esc_html__( 'Slide Up', 'gauge' ) => 'slideUp', esc_html__( 'Slide Down', 'gauge' ) => 'slideDown', esc_html__( 'Slide Left', 'gauge' ) => 'slideLeft', esc_html__( 'Slide Right', 'gauge' ) => 'slideRight', esc_html__( 'Slide Expand Up', 'gauge' ) => 'slideExpandUp', esc_html__( 'Expand Up', 'gauge' ) => 'expandUp', esc_html__( 'Fade In', 'gauge' ) => 'fadeIn', esc_html__( 'Expand Open', 'gauge' ) => 'expandOpen', esc_html__( 'Big Entrance', 'gauge' ) => 'bigEntrance', esc_html__( 'Hatch', 'gauge' ) => 'hatch', esc_html__( 'Bounce', 'gauge' ) => 'bounce', esc_html__( 'Pulse', 'gauge' ) => 'pulse', esc_html__( 'Floating', 'gauge' ) => 'Floating', esc_html__( 'Tossing', 'gauge' ) => 'tossing', esc_html__( 'Pull Up', 'gauge' ) => 'pullUp', esc_html__( 'Pull Down', 'gauge' ) => 'pullDown', esc_html__( 'Stretch Left', 'gauge' ) => 'stretchLeft', esc_html__( 'Stretch Right', 'gauge' ) => 'stretchRight' ),
		'description' => esc_html__( 'Select the animation effect of this element (only works in modern browsers).', 'gauge' ),
		'type' => 'dropdown',
		 ) );
		vc_add_param( 'vc_single_image', array( 
		'heading' => esc_html__( 'Scroll Animation', 'gauge' ),
		'param_name' => 'scroll_animation',
		'class' => '',
		'value' => array( esc_html__( 'Disabled', 'gauge' ) => 'disabled', esc_html__( 'Enabled', 'gauge' ) => 'enabled' ),
		'description' => esc_html__( 'Choose whether the animation effect is activated when the element is scrolled into viewed.', 'gauge' ),
		'type' => 'dropdown',
		'dependency' => array( 'element' => 'animation', 'value' => array( 'slideUp', 'slideDown', 'slideLeft', 'slideRight', 'slideExpandUp', 'expandUp', 'fadeIn', 'expandOpen', 'bigEntrance', 'hatch' ) )
		) );
		vc_add_param( 'vc_single_image', array( 
		'heading' => esc_html__( 'Delay', 'gauge' ),
		'param_name' => 'delay',
		'class' => '',
		'value' => '0.1',
		'description' => esc_html__( 'The number of seconds to delay the animation effect of the image.', 'gauge' ),
		'type' => 'textfield',
		'dependency' => array( 'element' => 'animation', 'value' => array( 'slideUp', 'slideDown', 'slideLeft', 'slideRight', 'slideExpandUp', 'expandUp', 'fadeIn', 'expandOpen', 'bigEntrance', 'hatch' ) )		
		) );							
		vc_add_param( 'vc_single_image', array( 
		'heading' => esc_html__( 'Styling', 'gauge' ),
		'param_name' => 'styling',
		'class' => '',
		'value' => '',
		'description' => esc_html__( 'The styling for the image.', 'gauge' ),
		'type' => 'textfield',
		) );
		vc_add_param( 'vc_single_image', array( 
		'heading' => esc_html__( 'Alignment', 'gauge' ),
		'param_name' => 'align',
		'class' => '',
		'value' => array( esc_html__( 'Left', 'gauge' ) => '', esc_html__( 'Center', 'gauge' ) => 'vc_align_center', esc_html__( 'Right', 'gauge' ) => 'vc_align_right', esc_html__( 'None', 'gauge' ) => 'vc_align_none' ),
		'description' => esc_html__( 'The alignment of the image.', 'gauge' ),
		'type' => 'dropdown',
		) );	

	}
	
}

add_action( 'init', 'ghostpool_old_custom_shortcodes' );
		
?>			
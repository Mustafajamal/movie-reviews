<?php

/*--------------------------------------------------------------
Shortcode Options
--------------------------------------------------------------*/

if ( ! function_exists( 'ghostpool_shortcode_options' ) ) {
	function ghostpool_shortcode_options( $atts ) {

		global $gp;

		$GLOBALS['ghostpool_cats'] = isset( $atts['cats'] ) ? $atts['cats'] : '';
		$GLOBALS['ghostpool_hub_field_slugs'] = isset( $atts['hub_fields'] ) ? $atts['hub_fields'] : '';
		$GLOBALS['ghostpool_post_association'] = isset( $atts['post_association'] ) ? $atts['post_association'] : 'enabled';
		$GLOBALS['ghostpool_post_types'] = isset( $atts['post_types'] ) ? $atts['post_types'] : 'post';
		if ( isset( $GLOBALS['ghostpool_shortcode'] ) && $GLOBALS['ghostpool_shortcode'] == 'portfolio' ) {
			$GLOBALS['ghostpool_format'] = isset( $atts['format'] ) ? $atts['format'] : 'portfolio-columns-2';
		} else {
			$GLOBALS['ghostpool_format'] = isset( $atts['format'] ) ? $atts['format'] : 'blog-standard';
		}	
		if ( isset( $GLOBALS['ghostpool_shortcode'] ) && $GLOBALS['ghostpool_shortcode'] == 'videos' ) {
			$GLOBALS['ghostpool_size'] = isset( $atts['size'] ) ? $atts['size'] : 'blog-small-size';
		} else {
			$GLOBALS['ghostpool_size'] = isset( $atts['size'] ) ? $atts['size'] : 'blog-standard-size';
		}	
		if ( isset( $GLOBALS['ghostpool_shortcode'] ) && $GLOBALS['ghostpool_shortcode'] == 'ranking' ) {
			$GLOBALS['ghostpool_orderby'] =  isset( $atts['orderby'] ) ? $atts['orderby'] : 'site_rating';
		} else {
			$GLOBALS['ghostpool_orderby'] =  isset( $atts['orderby'] ) ? $atts['orderby'] : 'newest';
		}
		$GLOBALS['ghostpool_date_posted'] = isset( $atts['date_posted'] ) ? $atts['date_posted'] : 'all';
		$GLOBALS['ghostpool_date_modified'] = isset( $atts['date_modified'] ) ? $atts['date_modified'] : 'all';
		$GLOBALS['ghostpool_filter'] = isset( $atts['filter'] ) ? $atts['filter'] : 'disabled';
		$GLOBALS['ghostpool_filter_cats'] = isset( $atts['filter_cats'] ) ? $atts['filter_cats'] : '';
		$GLOBALS['ghostpool_filter_date'] = isset( $atts['filter_date'] ) ? $atts['filter_date'] : '';
		$GLOBALS['ghostpool_filter_title'] = isset( $atts['filter_title'] ) ? $atts['filter_title'] : '';
		$GLOBALS['ghostpool_filter_comment_count'] = isset( $atts['filter_comment_count'] ) ? $atts['filter_comment_count'] : '';
		$GLOBALS['ghostpool_filter_views'] = isset( $atts['filter_views'] ) ? $atts['filter_views'] : '';
		$GLOBALS['ghostpool_filter_followers'] = isset( $atts['filter_followers'] ) ? $atts['filter_followers'] : '';
		$GLOBALS['ghostpool_filter_site_rating'] = isset( $atts['filter_site_rating'] ) ? $atts['filter_site_rating'] : '';
		$GLOBALS['ghostpool_filter_user_rating'] = isset( $atts['filter_user_rating'] ) ? $atts['filter_user_rating'] : '';
		$GLOBALS['ghostpool_filter_hub_awards'] = isset( $atts['filter_hub_awards'] ) ? $atts['filter_hub_awards'] : '';
		$GLOBALS['ghostpool_filter_date_posted'] = isset( $atts['filter_date_posted'] ) ? $atts['filter_date_posted'] : '';
		$GLOBALS['ghostpool_filter_date_modified'] = isset( $atts['filter_date_modified'] ) ? $atts['filter_date_modified'] : '';
		$GLOBALS['ghostpool_filter_cats_id'] = isset( $atts['filter_cats_id'] ) ? $atts['filter_cats_id'] : '';
		if ( isset( $GLOBALS['ghostpool_shortcode'] ) && ( $GLOBALS['ghostpool_shortcode'] == 'blog' OR $GLOBALS['ghostpool_shortcode'] == 'portfolio' ) ) {
			$GLOBALS['ghostpool_per_page'] = isset( $atts['per_page'] ) ? $atts['per_page'] : '12';
		} else {
			$GLOBALS['ghostpool_per_page'] = isset( $atts['per_page'] ) ? $atts['per_page'] : '5';
		}		
		$GLOBALS['ghostpool_offset'] = isset( $atts['offset'] ) ? $atts['offset'] : '';
		$GLOBALS['ghostpool_featured_image'] = isset( $atts['featured_image'] ) ? $atts['featured_image'] : 'enabled';
		if ( isset( $GLOBALS['ghostpool_shortcode'] ) && $GLOBALS['ghostpool_shortcode'] == 'videos' ) {
			$GLOBALS['ghostpool_image_width'] = isset( $atts['image_width'] ) ? $atts['image_width'] : '75';
		} elseif ( isset( $GLOBALS['ghostpool_shortcode'] ) && $GLOBALS['ghostpool_shortcode'] == 'featured' ) {
			$GLOBALS['ghostpool_image_width'] = isset( $atts['image_width'] ) ? $atts['image_width'] : '264';
		} else {
			$GLOBALS['ghostpool_image_width'] = isset( $atts['image_width'] ) ? $atts['image_width'] : '140';
		}
		if ( isset( $GLOBALS['ghostpool_shortcode'] ) && $GLOBALS['ghostpool_shortcode'] == 'videos' ) {
			$GLOBALS['ghostpool_image_height'] = isset( $atts['image_height'] ) ? $atts['image_height'] : '75';
		} elseif ( isset( $GLOBALS['ghostpool_shortcode'] ) && $GLOBALS['ghostpool_shortcode'] == 'featured' ) {
			$GLOBALS['ghostpool_image_height'] = isset( $atts['image_height'] ) ? $atts['image_height'] : '264';			
		} else {
			$GLOBALS['ghostpool_image_height'] = isset( $atts['image_height'] ) ? $atts['image_height'] : '140';	
		}
		$GLOBALS['ghostpool_hard_crop'] = isset( $atts['hard_crop'] ) ? $atts['hard_crop'] : true;
		$GLOBALS['ghostpool_image_alignment'] = isset( $atts['image_alignment'] ) ? $atts['image_alignment'] : 'image-align-left';
		$GLOBALS['ghostpool_title_position'] = isset( $atts['title_position'] ) ? $atts['title_position'] : 'title-next-to-thumbnail';
		$GLOBALS['ghostpool_content_display'] = isset( $atts['content_display'] ) ? $atts['content_display'] : 'excerpt';
		if ( isset( $GLOBALS['ghostpool_shortcode'] ) && $GLOBALS['ghostpool_shortcode'] == 'blog' ) {
			$GLOBALS['ghostpool_excerpt_length'] = isset( $atts['excerpt_length'] ) ? $atts['excerpt_length'] : '160';
		} elseif ( isset( $GLOBALS['ghostpool_shortcode'] ) && $GLOBALS['ghostpool_shortcode'] == 'news' ) {
			$GLOBALS['ghostpool_excerpt_length'] = isset( $atts['excerpt_length'] ) ? $atts['excerpt_length'] : '100';
		} elseif ( isset( $GLOBALS['ghostpool_shortcode'] ) && $GLOBALS['ghostpool_shortcode'] == 'featured' ) {
			$GLOBALS['ghostpool_excerpt_length'] = isset( $atts['excerpt_length'] ) ? $atts['excerpt_length'] : '250';
		} else {
			$GLOBALS['ghostpool_excerpt_length'] = isset( $atts['excerpt_length'] ) ? $atts['excerpt_length'] : '0';		
		}
		$GLOBALS['ghostpool_meta_author'] = isset( $atts['meta_author'] ) ? $atts['meta_author'] : '';
		$GLOBALS['ghostpool_meta_date'] = isset( $atts['meta_date'] ) ? $atts['meta_date'] : '';
		$GLOBALS['ghostpool_meta_comment_count'] = isset( $atts['meta_comment_count'] ) ? $atts['meta_comment_count'] : '';
		$GLOBALS['ghostpool_meta_views'] = isset( $atts['meta_views'] ) ? $atts['meta_views'] : '';
		$GLOBALS['ghostpool_meta_followers'] = isset( $atts['meta_followers'] ) ? $atts['meta_followers'] : '';
		$GLOBALS['ghostpool_meta_cats'] = isset( $atts['meta_cats'] ) ? $atts['meta_cats'] : '';
		$GLOBALS['ghostpool_meta_tags'] = isset( $atts['meta_tags'] ) ? $atts['meta_tags'] : '';
		$GLOBALS['ghostpool_meta_hub_cats'] = isset( $atts['meta_hub_cats'] ) ? $atts['meta_hub_cats'] : '';
		$GLOBALS['ghostpool_meta_hub_fields'] = isset( $atts['meta_hub_fields'] ) ? $atts['meta_hub_fields'] : '';
		$GLOBALS['ghostpool_meta_hub_award'] = isset( $atts['meta_hub_award'] ) ? $atts['meta_hub_award'] : '';	
		$GLOBALS['ghostpool_hub_cats_selected'] = isset( $gp['hub_cat_cats'] ) ? $gp['hub_cat_cats'] : '';
		$GLOBALS['ghostpool_hub_fields'] = isset( $gp['hub_cat_fields'] ) ? $gp['hub_cat_fields'] : '';		
		$GLOBALS['ghostpool_display_site_rating'] = isset( $atts['display_site_rating'] ) ? $atts['display_site_rating'] : '';
		$GLOBALS['ghostpool_display_user_rating'] = isset( $atts['display_user_rating'] ) ? $atts['display_user_rating'] : '';
		$GLOBALS['ghostpool_read_more_link'] = isset( $atts['read_more_link'] ) ? $atts['read_more_link'] : 'enabled';
		$GLOBALS['ghostpool_page_numbers'] = isset( $atts['page_numbers'] ) ? $atts['page_numbers'] : 'disabled';

		// Fallbacks
		if ( ! isset( $atts['meta_comment_count'] ) ) {
			$GLOBALS['ghostpool_meta_comment_count'] = isset( $atts['meta_comments'] ) ? $atts['meta_comments'] : $GLOBALS['ghostpool_meta_comment_count'];
		}	
		if ( ! isset( $atts['page_numbers'] ) ) {
			$GLOBALS['ghostpool_page_numbers'] = isset( $atts['pages'] ) ? $atts['pages'] : $GLOBALS['ghostpool_page_numbers'];
		}
		if ( ( isset( $atts['format'] ) && $atts['format'] == 'blog-columns' ) && ( isset( $atts['column_type'] ) && $atts['column_type'] == 'multiple-columns' ) ) {
			$GLOBALS['ghostpool_format'] = 'blog-columns-3';
		}
		if ( ( isset( $atts['format'] ) && $atts['format'] == 'blog-columns' ) && ( isset( $atts['column_type'] ) && $atts['column_type'] == 'single-column' ) ) {
			$GLOBALS['ghostpool_format'] = 'blog-columns-1';
		}
				
		// Add slug support for filter categories option
		if ( preg_match( '/[a-zA-Z\-]+/', $GLOBALS['ghostpool_filter_cats_id'] ) ) {
			$taxonomies = get_taxonomies();
			foreach ( $taxonomies as $taxonomy ) {
				$term = term_exists( $GLOBALS['ghostpool_filter_cats_id'], $taxonomy );
				$tax_name = '';
				if ( $term !== 0 && $term !== null ) {
					$tax_name = $taxonomy;
					break;
				}
			}		
			$filter_cats_slug = get_term_by( 'slug', $GLOBALS['ghostpool_filter_cats_id'], $tax_name );
			if ( $filter_cats_slug ) {
				$GLOBALS['ghostpool_filter_cats_id'] = $filter_cats_slug->term_id;
			}
		}
	
	}
}


/*--------------------------------------------------------------
Custom Shortcodes
--------------------------------------------------------------*/

if ( ! function_exists( 'ghostpool_custom_shortcodes' ) ) {
	function ghostpool_custom_shortcodes() {


		/*--------------------------------------------------------------
		Advertisement Shortcode
		--------------------------------------------------------------*/

		require_once( ghostpool_vc . 'gp_vc_advertisement.php' );

		vc_map( array( 
			'name' => esc_html__( 'Advertisement', 'gauge' ),
			'deprecated' => '5.7',
			'base' => 'advertisement',
			'description' => esc_html__( 'Insert an advertisement anywhere you can insert this element.', 'gauge' ),
			'class' => 'wpb_vc_advertisement',
			'controls' => 'full',
			'icon' => 'gp-icon-advertisement',
			'category' => esc_html__( 'Content', 'gauge' ),
			'params' => array(	
				array( 
				'heading' => esc_html__( 'Title', 'gauge' ),
				'description' => esc_html__( 'The title at the top of the element.', 'gauge' ),
				'param_name' => 'widget_title',
				'type' => 'textfield',
				'admin_label' => true,
				'value' => '',
				),	
				array( 
				'heading' => esc_html__( 'Advertisement Code', 'gauge' ),
				'description' => esc_html__( 'The advertisement code.', 'gauge' ),
				'param_name' => 'content',
				'value' => '',
				'type' => 'textarea_html',
				),	
				array( 
				'heading' => esc_html__( 'Extra Class Names', 'gauge' ),
				'description' => esc_html__( 'If you wish to style this particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'gauge' ),
				'param_name' => 'classes',
				'value' => '',
				'type' => 'textfield',
				),
			 )
		) );
		
		
		/*--------------------------------------------------------------
		Blog Shortcode
		--------------------------------------------------------------*/

		require_once( ghostpool_vc . 'gp_vc_blog.php' );

		vc_map( array( 
			'name' => esc_html__( 'Blog', 'gauge' ),
			'base' => 'blog',
			'description' => esc_html__( 'Display posts, pages and custom post types in a variety of ways.', 'gauge' ),
			'class' => 'wpb_vc_blog',
			'controls' => 'full',
			'icon' => 'gp-icon-blog',
			'category' => esc_html__( 'Content', 'gauge' ),			
			'admin_enqueue_css' => ghostpool_css_uri . 'admin.css',
			'front_enqueue_css' => ghostpool_css_uri . 'admin.css',
			'front_enqueue_js' => array( ghostpool_scripts_uri . 'isotope.pkgd.min.js', ghostpool_scripts_uri . 'imagesLoaded.min.js' ),
			'params' => array(		
				array( 
				'heading' => esc_html__( 'Title', 'gauge' ),
				'description' => esc_html__( 'The title at the top of the element.', 'gauge' ),
				'param_name' => 'widget_title',
				'type' => 'textfield',
				'admin_label' => true,
				'value' => '',
				),				
				array( 
				'heading' => esc_html__( 'Categories', 'gauge' ),
				'description' => esc_html__( 'Enter the slugs or IDs separating each one with a comma e.g. xbox,ps3,pc.', 'gauge' ),
				'param_name' => 'cats',
				'type' => 'textfield',
				),			
				array( 
				'heading' => esc_html__( 'Hub Fields', 'gauge' ),
				'description' => wp_kses( __( 'Enter the hub fields you want to filter by. Add your taxonomy slug followed by a colon. Next enter your terms separating each by a colon also. Next add a comma and then enter the next taxonomy and so on e.g. <code>taxonomy-1:term-1:term-2,taxonomy-2:term-1,taxonomy-3:term-1:term-2</code>, this would translate to <code>genre:action:role-playing,publisher:namco,developed-by:namco:bluepoint-games</code>', 'gauge' ),  array( 'code' => array() ) ),
				'param_name' => 'hub_fields',
				'type' => 'textfield',
				'value' => '',
				),								
				array( 
				'heading' => esc_html__( 'Post Association', 'gauge' ),
				'description' => esc_html__( 'Only show posts associated with the parent hub page.', 'gauge' ),
				'param_name' => 'post_association',
				'value' => array( esc_html__( 'Enabled', 'gauge' ) => 'enabled', esc_html__( 'Disabled', 'gauge' ) => 'disabled' ),
				'type' => 'dropdown',
				),
				array( 
				'heading' => esc_html__( 'Post Types', 'gauge' ),
				'description' => esc_html__( 'The post types to display.', 'gauge' ),
				'param_name' => 'post_types',
				'value' => 'post',
				'type' => 'posttypes',
				),
				array( 
				'heading' => esc_html__( 'Format', 'gauge' ),
				'description' => esc_html__( 'The format to display the items in.', 'gauge' ),
				'param_name' => 'format',
				'value' => array( esc_html__( 'Standard', 'gauge' ) => 'blog-standard', esc_html__( 'Large', 'gauge' ) => 'blog-large', esc_html__( '1 Column', 'gauge' ) => 'blog-columns-1', esc_html__( '2 Columns', 'gauge' ) => 'blog-columns-2', esc_html__( '3 Columns', 'gauge' ) => 'blog-columns-3', esc_html__( '4 Columns', 'gauge' ) => 'blog-columns-4', esc_html__( '5 Columns', 'gauge' ) => 'blog-columns-5', esc_html__( '6 Columns', 'gauge' ) => 'blog-columns-6', esc_html__( 'Masonry', 'gauge' ) => 'blog-masonry' ),
				'type' => 'dropdown',
				),		
				array( 
				'heading' => esc_html__( 'Size', 'gauge' ),
				'description' => esc_html__( 'The size to display the items at.', 'gauge' ),
				'param_name' => 'size',
				'value' => array( esc_html__( 'Standard', 'gauge' ) => 'blog-standard-size', esc_html__( 'Small', 'gauge' ) => 'blog-small-size' ),
				'type' => 'dropdown',
				),
				array( 
				'heading' => esc_html__( 'Order By', 'gauge' ),
				'description' => esc_html__( 'The criteria which the items are ordered by.', 'gauge' ),
				'param_name' => 'orderby',
				'value' => array(
					esc_html__( 'Newest', 'gauge' ) => 'newest',
					esc_html__( 'Oldest', 'gauge' ) => 'oldest',
					esc_html__( 'Title (A-Z)', 'gauge' ) => 'title_az',
					esc_html__( 'Title (Z-A)', 'gauge' ) => 'title_za',
					esc_html__( 'Most Comments', 'gauge' ) => 'comment_count',
					esc_html__( 'Most Views', 'gauge' ) => 'views',
					esc_html__( 'Most Followers', 'gauge' ) => 'followers',
					esc_html__( 'Top Site Rated', 'gauge' ) => 'site_rating',
					esc_html__( 'Top User Rated', 'gauge' ) => 'user_rating',
					esc_html__( 'Hub Awards', 'gauge' ) => 'hub_awards',
					esc_html__( 'Menu Order', 'gauge' ) => 'menu_order',
					esc_html__( 'Random', 'gauge' ) => 'rand',
				),
				'type' => 'dropdown',
				),	
				array( 
				'heading' => esc_html__( 'Date Posted', 'gauge' ),
				'description' => esc_html__( 'The date the items were posted.', 'gauge' ),
				'param_name' => 'date_posted',
				'value' => array(
					esc_html__( 'Any date', 'gauge' ) => 'all',
					esc_html__( 'In the last year', 'gauge' ) => 'year',
					esc_html__( 'In the last month', 'gauge' ) => 'month',
					esc_html__( 'In the last week', 'gauge' ) => 'week',
					esc_html__( 'In the last day', 'gauge' ) => 'day',
				),
				'type' => 'dropdown',
				),
				array( 
				'heading' => esc_html__( 'Date Modified', 'gauge' ),
				'description' => esc_html__( 'The date the items were modified.', 'gauge' ),
				'param_name' => 'date_modified',
				'value' => array(
					esc_html__( 'Any date', 'gauge' ) => 'all',
					esc_html__( 'In the last year', 'gauge' ) => 'year',
					esc_html__( 'In the last month', 'gauge' ) => 'month',
					esc_html__( 'In the last week', 'gauge' ) => 'week',
					esc_html__( 'In the last day', 'gauge' ) => 'day',
				),
				'type' => 'dropdown',
				),	
				array( 
				'heading' => esc_html__( 'Filter', 'gauge' ),
				'description' => esc_html__( 'Add a dropdown filter menu to the page.', 'gauge' ),
				'param_name' => 'filter',
				'value' => array( esc_html__( 'Disabled', 'gauge' ) => 'disabled', esc_html__( 'Enabled', 'gauge' ) => 'enabled' ),
				'type' => 'dropdown',
				),	
				array(
				'heading' => esc_html__( 'Filter Options', 'gauge' ),
				'param_name' => 'filter_cats',
				'value' => array( esc_html__( 'Categories', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				'dependency' => array( 'element' => 'filter', 'value' => 'enabled' ),
				),	
				array(
				'param_name' => 'filter_date',
				'value' => array( esc_html__( 'Date', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				'dependency' => array( 'element' => 'filter', 'value' => 'enabled' ),
				),	
				array(
				'param_name' => 'filter_title',
				'value' => array( esc_html__( 'Title', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				'dependency' => array( 'element' => 'filter', 'value' => 'enabled' ),
				),								
				array(
				'param_name' => 'filter_comment_count',
				'value' => array( esc_html__( 'Comment Count', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				'dependency' => array( 'element' => 'filter', 'value' => 'enabled' ),
				),
				array(
				'param_name' => 'filter_views',
				'value' => array( esc_html__( 'Views', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				'dependency' => array( 'element' => 'filter', 'value' => 'enabled' ),
				),	
				array(
				'param_name' => 'filter_followers',
				'value' => array( esc_html__( 'Followers', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				'dependency' => array( 'element' => 'filter', 'value' => 'enabled' ),
				),									
				array(
				'param_name' => 'filter_site_rating',
				'value' => array( esc_html__( 'Site Rating', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				'dependency' => array( 'element' => 'filter', 'value' => 'enabled' ),
				),	
				array(
				'param_name' => 'filter_user_rating',
				'value' => array( esc_html__( 'User Rating', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				'dependency' => array( 'element' => 'filter', 'value' => 'enabled' ),
				),
				array( 
				'param_name' => 'filter_hub_awards',
				'value' => array( esc_html__( 'Hub Awards', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				'dependency' => array( 'element' => 'filter', 'value' => 'enabled' ),
				),
				array( 
				'param_name' => 'filter_date_posted',
				'value' => array( esc_html__( 'Date Posted', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				'dependency' => array( 'element' => 'filter', 'value' => 'enabled' ),
				),				
				array( 
				'description' => esc_html__( 'Choose what options to display in the dropdown filter menu.', 'gauge' ),
				'param_name' => 'filter_date_modified',
				'value' => array( esc_html__( 'Date Modified', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				'dependency' => array( 'element' => 'filter', 'value' => 'enabled' ),
				),				
				array( 
				'heading' => esc_html__( 'Filter Category', 'gauge' ),
				'description' => esc_html__( 'Enter the slug or ID number of the category you want to filter by, leave blank to display all categories - the sub categories of this category will also be displayed.', 'gauge' ),
				'param_name' => 'filter_cats_id',
				'type' => 'textfield',
				'dependency' => array( 'element' => 'filter', 'value' => 'enabled' ),
				),																				 
				array( 
				'heading' => esc_html__( 'Items Per Page', 'gauge' ),
				'description' => esc_html__( 'The number of items on each page.', 'gauge' ),
				'param_name' => 'per_page',
				'value' => '12',
				'type' => 'textfield',
				),
				array( 
				'heading' => esc_html__( 'Offset', 'gauge' ),
				'description' => esc_html__( 'The number of posts to offset by e.g. set to 3 to exclude the first 3 posts.', 'gauge' ),
				'param_name' => 'offset',
				'value' => '',
				'type' => 'textfield',
				),
				array( 
				'heading' => esc_html__( 'Featured Image', 'gauge' ),
				'description' => esc_html__( 'Display the featured images.', 'gauge' ),
				'param_name' => 'featured_image',
				'value' => array( esc_html__( 'Enabled', 'gauge' ) => 'enabled', esc_html__( 'Disabled', 'gauge' ) => 'disabled' ),
				'type' => 'dropdown',
				),	
				array( 
				'heading' => esc_html__( 'Image Width', 'gauge' ),
				'description' => esc_html__( 'The width of the featured images.', 'gauge' ),
				'param_name' => 'image_width',
				'value' => '140',
				'type' => 'textfield',
				'dependency' => array( 'element' => 'featured_image', 'value' => 'enabled' ),
				),		 
				array( 
				'heading' => esc_html__( 'Image Height', 'gauge' ),
				'description' => esc_html__( 'The height of the featured images.', 'gauge' ),
				'param_name' => 'image_height',
				'value' => '140',
				'type' => 'textfield',
				'dependency' => array( 'element' => 'featured_image', 'value' => 'enabled' ),
				),	
				array( 
				'heading' => esc_html__( 'Hard Crop', 'gauge' ),
				'description' => esc_html__( 'Images are cropped even if it is smaller than the dimensions you want to crop it to.', 'gauge' ),
				'param_name' => 'hard_crop',
				'value' => array( esc_html__( 'Enabled', 'gauge' ) => 'enabled', esc_html__( 'Disabled', 'gauge' ) => 'disabled' ),
				'type' => 'dropdown',
				'dependency' => array( 'element' => 'featured_image', 'value' => 'enabled' ),
				),	
				array( 
				'heading' => esc_html__( 'Image Alignment', 'gauge' ),
				'description' => esc_html__( 'Choose how the image aligns with the content.', 'gauge' ),
				'param_name' => 'image_alignment',
				'value' => array( esc_html__( 'Left Align', 'gauge' ) => 'image-align-left', esc_html__( 'Right Align', 'gauge' ) => 'image-align-right', esc_html__( 'Left Wrap', 'gauge' ) => 'image-wrap-left', esc_html__( 'Right Wrap', 'gauge' ) => 'image-wrap-right', esc_html__( 'Above Content', 'gauge' ) => 'image-above' ),
				'type' => 'dropdown',
				'dependency' => array( 'element' => 'featured_image', 'value' => 'enabled' ),
				),
				array( 
				'heading' => esc_html__( 'Title Position', 'gauge' ),
				'description' => esc_html__( 'The position of the title.', 'gauge' ),
				'param_name' => 'title_position',
				'value' => array( esc_html__( 'Next To Thumbnail', 'gauge' ) => 'title-next-to-thumbnail', esc_html__( 'Over Thumbnail', 'gauge' ) => 'title-over-thumbnail' ),
				'type' => 'dropdown',
				),
				array( 
				'heading' => esc_html__( 'Content Display', 'gauge' ),
				'description' => esc_html__( 'The amount of content displayed.', 'gauge' ),
				'param_name' => 'content_display',
				'value' => array( esc_html__( 'Excerpt', 'gauge' ) => 'excerpt', esc_html__( 'Full Content', 'gauge' ) => 'full_content' ),
				'type' => 'dropdown',
				),
				array( 
				'heading' => esc_html__( 'Excerpt Length', 'gauge' ),
				'description' => esc_html__( 'The number of characters in excerpts.', 'gauge' ),
				'param_name' => 'excerpt_length',
				'value' => '160',
				'type' => 'textfield',
				'dependency' => array( 'element' => 'content_display', 'value' => 'excerpt' ),
				),	
				array(
				'heading' => esc_html__( 'Post Meta', 'gauge' ),
				'param_name' => 'meta_author',
				'value' => array( esc_html__( 'Author Name', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				),	
				array(
				'param_name' => 'meta_date',
				'value' => array( esc_html__( 'Post Date', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				),			
				array(
				'param_name' => 'meta_comment_count',
				'value' => array( esc_html__( 'Comment Count', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				),		
				array(
				'param_name' => 'meta_views',
				'value' => array( esc_html__( 'Views', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				),		
				array(
				'param_name' => 'meta_followers',
				'value' => array( esc_html__( 'Followers', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				),
				array( 
				'param_name' => 'meta_cats',
				'value' => array( esc_html__( 'Post Categories', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				),
				array(
				'param_name' => 'meta_tags',
				'value' => array( esc_html__( 'Post Tags', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				),	
				array(
				'param_name' => 'meta_hub_cats',
				'value' => array( esc_html__( 'Hub Categories', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				),					
				array(
				'param_name' => 'meta_hub_fields',
				'value' => array( esc_html__( 'Hub Fields', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				),
				array(
				'description' => esc_html__( 'Select the meta data you want to display.', 'gauge' ),
				'param_name' => 'meta_hub_award',
				'value' => array( esc_html__( 'Hub Award', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				),
				array(
				'heading' => esc_html__( 'Ratings', 'gauge' ),
				'param_name' => 'display_site_rating',
				'value' => array( esc_html__( 'Site Rating', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				),					
				array(
				'description' => esc_html__( 'Select the ratings you want to display.', 'gauge' ),
				'param_name' => 'display_user_rating',
				'value' => array( esc_html__( 'User Rating', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				),								
				array( 
				'heading' => esc_html__( 'Read More Link', 'gauge' ),
				'description' => esc_html__( 'Add a read more link below the content.', 'gauge' ),
				'param_name' => 'read_more_link',
				'value' => array( esc_html__( 'Enabled', 'gauge' ) => 'enabled', esc_html__( 'Disabled', 'gauge' ) => 'disabled' ),
				'type' => 'dropdown',
				),		 
				array( 
				'heading' => esc_html__( 'Pagination', 'gauge' ),
				'description' => esc_html__( 'Add pagination.', 'gauge' ),
				'param_name' => 'page_numbers',
				'value' => array( esc_html__( 'Disabled', 'gauge' ) => 'disabled', esc_html__( 'Enabled', 'gauge' ) => 'enabled' ),
				'type' => 'dropdown',
				),	
				array( 
				'heading' => esc_html__( 'See All', 'gauge' ),
				'description' => esc_html__( 'Add a "See All" link.', 'gauge' ),
				'param_name' => 'see_all',
				'value' => array( esc_html__( 'Disabled', 'gauge' ) => 'disabled', esc_html__( 'Enabled', 'gauge' ) => 'enabled' ),
				'type' => 'dropdown',
				),
				array( 
				'heading' => esc_html__( 'See All Link', 'gauge' ),
				'description' => esc_html__( 'URL for the "See All" link.', 'gauge' ),
				'param_name' => 'see_all_link',
				'type' => 'textfield',
				'dependency' => array( 'element' => 'see_all', 'value' => 'enabled' ),
				),				 			 
				array( 
				'heading' => esc_html__( 'See All Text', 'gauge' ),
				'description' => esc_html__( 'Custom text for the "See All" link.', 'gauge' ),
				'param_name' => 'see_all_text',
				'type' => 'textfield',
				'value' => esc_html__( 'See All Items', 'gauge' ),
				'dependency' => array( 'element' => 'see_all', 'value' => 'enabled' ),
				),	 			 				 		   			 			 
				array( 
				'heading' => esc_html__( 'Extra Class Names', 'gauge' ),
				'description' => esc_html__( 'If you wish to style this particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'gauge' ),
				'param_name' => 'classes',
				'value' => '',
				'type' => 'textfield',
				),																																								
			 )
		) );


		/*--------------------------------------------------------------
		Featured Shortcode
		--------------------------------------------------------------*/

		require_once( ghostpool_vc . 'gp_vc_featured.php' );

		vc_map( array( 
			'name' => esc_html__( 'Featured', 'gauge' ),
			'base' => 'featured',
			'description' => esc_html__( 'Catch the attention of your visitors with a featured post or page.', 'gauge' ),
			'class' => 'wpb_vc_featured',
			'controls' => 'full',
			'icon' => 'gp-icon-featured',
			'category' => esc_html__( 'Content', 'gauge' ),
			'params' => array(		
				array( 
				'heading' => esc_html__( 'Title', 'gauge' ),
				'description' => esc_html__( 'The title that appears faded in the background.', 'gauge' ),
				'param_name' => 'widget_title',
				'type' => 'textfield',
				'admin_label' => true,
				'value' => '',
				),				
				array( 
				'heading' => esc_html__( 'Post Slug/ID', 'gauge' ),
				'description' => esc_html__( 'Enter the slug or ID.', 'gauge' ),
				'param_name' => 'post_id',
				'type' => 'textfield',
				),
				array( 
				'heading' => esc_html__( 'Page Slug/ID', 'gauge' ),
				'description' => esc_html__( 'Enter the slug or ID.', 'gauge' ),
				'param_name' => 'page_id',
				'type' => 'textfield',
				),				
				array( 
				'heading' => esc_html__( 'Featured Image', 'gauge' ),
				'description' => esc_html__( 'Display the featured images.', 'gauge' ),
				'param_name' => 'featured_image',
				'value' => array( esc_html__( 'Enabled', 'gauge' ) => 'enabled', esc_html__( 'Disabled', 'gauge' ) => 'disabled' ),
				'type' => 'dropdown',
				),	
				array( 
				'heading' => esc_html__( 'Image Width', 'gauge' ),
				'description' => esc_html__( 'The width of the featured images.', 'gauge' ),
				'param_name' => 'image_width',
				'value' => '264',
				'type' => 'textfield',
				'dependency' => array( 'element' => 'featured_image', 'value' => 'enabled' ),
				),		 
				array( 
				'heading' => esc_html__( 'Image Height', 'gauge' ),
				'description' => esc_html__( 'The height of the featured images.', 'gauge' ),
				'param_name' => 'image_height',
				'value' => '264',
				'type' => 'textfield',
				'dependency' => array( 'element' => 'featured_image', 'value' => 'enabled' ),
				),	
				array( 
				'heading' => esc_html__( 'Hard Crop', 'gauge' ),
				'description' => esc_html__( 'Images are cropped even if it is smaller than the dimensions you want to crop it to.', 'gauge' ),
				'param_name' => 'hard_crop',
				'value' => array( esc_html__( 'Enabled', 'gauge' ) => 'enabled', esc_html__( 'Disabled', 'gauge' ) => 'disabled' ),
				'type' => 'dropdown',
				'dependency' => array( 'element' => 'featured_image', 'value' => 'enabled' ),
				),	
				array( 
				'heading' => esc_html__( 'Image Alignment', 'gauge' ),
				'description' => esc_html__( 'Choose how the image aligns with the content.', 'gauge' ),
				'param_name' => 'image_alignment',
				'value' => array( esc_html__( 'Left Align', 'gauge' ) => 'image-align-left', esc_html__( 'Left Wrap', 'gauge' ) => 'image-wrap-left', esc_html__( 'Right Wrap', 'gauge' ) => 'image-wrap-right', esc_html__( 'Above Content', 'gauge' ) => 'image-above', esc_html__( 'Right Align', 'gauge' ) => 'image-align-right' ),
				'type' => 'dropdown',
				'dependency' => array( 'element' => 'featured_image', 'value' => 'enabled' ),
				),
				array( 
				'heading' => esc_html__( 'Title Position', 'gauge' ),
				'description' => esc_html__( 'The position of the title.', 'gauge' ),
				'param_name' => 'title_position',
				'value' => array( esc_html__( 'Next To Thumbnail', 'gauge' ) => 'title-next-to-thumbnail', esc_html__( 'Over Thumbnail', 'gauge' ) => 'title-over-thumbnail' ),
				'type' => 'dropdown',
				),
				array( 
				'heading' => esc_html__( 'Content Display', 'gauge' ),
				'description' => esc_html__( 'The amount of content displayed.', 'gauge' ),
				'param_name' => 'content_display',
				'value' => array( esc_html__( 'Excerpt', 'gauge' ) => 'excerpt', esc_html__( 'Full Content', 'gauge' ) => 'full_content' ),
				'type' => 'dropdown',
				),
				array( 
				'heading' => esc_html__( 'Excerpt Length', 'gauge' ),
				'description' => esc_html__( 'The number of characters in excerpts.', 'gauge' ),
				'param_name' => 'excerpt_length',
				'value' => '250',
				'type' => 'textfield',
				'dependency' => array( 'element' => 'content_display', 'value' => 'excerpt' ),
				),	
				array(
				'heading' => esc_html__( 'Post Meta', 'gauge' ),
				'param_name' => 'meta_author',
				'value' => array( esc_html__( 'Author Name', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				),	
				array(
				'param_name' => 'meta_date',
				'value' => array( esc_html__( 'Post Date', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				),				
				array(
				'param_name' => 'meta_views',
				'value' => array( esc_html__( 'Views', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				),				
				array(
				'param_name' => 'meta_followers',
				'value' => array( esc_html__( 'Followers', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				),
				array(
				'param_name' => 'meta_comment_count',
				'value' => array( esc_html__( 'Comment Count', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				),
				array( 
				'param_name' => 'meta_cats',
				'value' => array( esc_html__( 'Post Categories', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				),
				array(
				'param_name' => 'meta_tags',
				'value' => array( esc_html__( 'Post Tags', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				),	
				array(
				'param_name' => 'meta_hub_cats',
				'value' => array( esc_html__( 'Hub Categories', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				),					
				array(
				'param_name' => 'meta_hub_fields',
				'value' => array( esc_html__( 'Hub Fields', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				),
				array(
				'description' => esc_html__( 'Select the meta data you want to display.', 'gauge' ),
				'param_name' => 'meta_hub_award',
				'value' => array( esc_html__( 'Hub Award', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				),				
				array(
				'heading' => esc_html__( 'Ratings', 'gauge' ),
				'param_name' => 'display_site_rating',
				'value' => array( esc_html__( 'Site Rating', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				),					
				array(
				'description' => esc_html__( 'Select the ratings you want to display.', 'gauge' ),
				'param_name' => 'display_user_rating',
				'value' => array( esc_html__( 'User Rating', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				),								
				array( 
				'heading' => esc_html__( 'Read More Link', 'gauge' ),
				'description' => esc_html__( 'Add a read more link below the content.', 'gauge' ),
				'param_name' => 'read_more_link',
				'value' => array( esc_html__( 'Enabled', 'gauge' ) => 'enabled', esc_html__( 'Disabled', 'gauge' ) => 'disabled' ),
				'type' => 'dropdown',
				),		 		   			 			 
				array( 
				'heading' => esc_html__( 'Extra Class Names', 'gauge' ),
				'param_name' => 'classes',
				'value' => '',
				'description' => esc_html__( 'If you wish to style this particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'gauge' ),
				'type' => 'textfield',
				),
				array(
				'heading' => esc_html__( 'CSS', 'gauge' ),
				'type' => 'css_editor',
				'param_name' => 'css',
				'group' => esc_html__( 'Design options', 'gauge' ),
				),	
				array( 
				'heading' => esc_html__( 'Text Color', 'gauge' ),
				'description' => esc_html__( 'The color of the text.', 'gauge' ),
				'param_name' => 'text_color',
				'value' => '',
				'type' => 'colorpicker',
				'group' => esc_html__( 'Design options', 'gauge' ),
				),
				array( 
				'heading' => esc_html__( 'Background Overlay', 'gauge' ),
				'description' => esc_html__( 'Add a background overlay to the featured content.', 'gauge' ),
				'param_name' => 'background_overlay',
				'value' => array( esc_html__( 'Enabled', 'gauge' ) => 'enabled', esc_html__( 'Disabled', 'gauge' ) => 'disabled' ),
				'type' => 'dropdown',
				'group' => esc_html__( 'Design options', 'gauge' ),
				),																																														
			 )
		) );


		/*--------------------------------------------------------------
		Filters Shortcode
		--------------------------------------------------------------*/

		require_once( ghostpool_vc . 'gp_vc_filters.php' );

		vc_map( array( 
			'name' => esc_html__( 'Filters', 'gauge' ),
			'base' => 'filters',
			'description' => esc_html__( 'Add filters to your category pages.', 'gauge' ),
			'class' => 'wpb_vc_filters',
			'controls' => 'full',
			'icon' => 'gp-icon-filters',
			'category' => esc_html__( 'Content', 'gauge' ),
			'params' => array( 			
				array( 
				'heading' => esc_html__( 'Title', 'gauge' ),
				'description' => esc_html__( 'The title at the top of the element.', 'gauge' ),
				'param_name' => 'widget_title',
				'type' => 'textfield',
				'admin_label' => true,
				'value' => '',
				),
				array( 
				'heading' => esc_html__( 'Parent Hub Category', 'gauge' ),
				'description' => esc_html__( 'Enter the slug or ID of the parent category you want your filters based on.', 'gauge' ),
				'param_name' => 'parent_cat',
				'type' => 'textfield',
				),
				/*array( 
				'heading' => esc_html__( 'Categories', 'gauge' ),
				'description' => esc_html__( 'Enter the slugs or IDs separating each one with a comma e.g. xbox,ps3,pc.', 'gauge' ),
				'param_name' => 'cats',
				'type' => 'textfield',
				),*/
				array( 
				'heading' => esc_html__( 'Date Posted', 'gauge' ),
				'param_name' => 'date_posted',
				'value' => array( esc_html__( 'Enabled', 'gauge' ) => 'enabled', esc_html__( 'Disabled', 'gauge' ) => 'disabled' ),
				'description' => esc_html__( 'Choose whether to add the date posted filter.', 'gauge' ),
				'type' => 'dropdown',
				),
				array( 
				'heading' => esc_html__( 'Date Modified', 'gauge' ),
				'param_name' => 'date_modified',
				'value' => array( esc_html__( 'Disabled', 'gauge' ) => 'disabled', esc_html__( 'Enabled', 'gauge' ) => 'enabled' ),
				'description' => esc_html__( 'Choose whether to add the date modified filter.', 'gauge' ),
				'type' => 'dropdown',
				),
				array( 
				'heading' => esc_html__( 'Hub Fields', 'gauge' ),
				'description' => esc_html__( 'Enter the slugs of the hub fields you want to add to the filter options, separating each with a comma e.g. genre,release-date,developed-by.', 'gauge' ),
				'param_name' => 'fields',
				'type' => 'textfield',
				),	
				array( 
				'heading' => esc_html__( 'Date Posted Text', 'gauge' ),
				'description' => esc_html__( 'The text used for the date posted dropdown menu.', 'gauge' ),
				'param_name' => 'date_posted_text',
				'value' =>  esc_html__( 'Release Date', 'gauge' ),
				'type' => 'textfield',
				),
				array( 
				'heading' => esc_html__( 'Date Modified Text', 'gauge' ),
				'description' => esc_html__( 'The text used for the date modified dropdown menu.', 'gauge' ),
				'param_name' => 'date_modified_text',
				'value' => esc_html__( 'Last Updated', 'gauge' ),
				'type' => 'textfield',
				),
				array( 
				'heading' => esc_html__( 'Parent Category Text', 'gauge' ),
				'description' => esc_html__( 'The text used for the parent category dropdown menu.', 'gauge' ),
				'param_name' => 'parent_cat_text',
				'value' =>  esc_html__( 'Categories', 'gauge' ),
				'type' => 'textfield',
				),		
				array( 
				'heading' => esc_html__( 'Submit Button Text', 'gauge' ),
				'description' => esc_html__( 'The text used for the submit button.', 'gauge' ),
				'param_name' => 'submit_button_text',
				'value' =>  esc_html__( 'Filter Items', 'gauge' ),
				'type' => 'textfield',
				),			
				array( 
				'heading' => esc_html__( 'Background Color', 'gauge' ),
				'description' => esc_html__( 'The background color.', 'gauge' ),
				'param_name' => 'bg_color',
				'value' => '',
				'type' => 'colorpicker',
				'group' => esc_html__( 'Design options', 'gauge' ),
				),
				array( 
				'heading' => esc_html__( 'Border Color', 'gauge' ),
				'description' => esc_html__( 'The border color.', 'gauge' ),
				'param_name' => 'border_color',
				'value' => '',
				'type' => 'colorpicker',
				'group' => esc_html__( 'Design options', 'gauge' ),
				),																																									
			 )
		) );
		
		
		/*--------------------------------------------------------------
		Images Shortcode
		--------------------------------------------------------------*/

		require_once( ghostpool_vc . 'gp_vc_images.php' );
		
		vc_map( array( 
			'name' => esc_html__( 'Images', 'gauge' ),
			'base' => 'images',
			'description' => esc_html__( 'Display a list of images.', 'gauge' ),
			'class' => 'wpb_vc_images',
			'controls' => 'full',
			'icon' => 'gp-icon-images',
			'category' => esc_html__( 'Content', 'gauge' ),
			'params' => array(
				array( 
				'heading' => esc_html__( 'Title', 'gauge' ),
				'description' => esc_html__( 'The title at the top of the element.', 'gauge' ),
				'param_name' => 'widget_title',
				'type' => 'textfield',
				'admin_label' => true,
				'value' => '',
				),
				array( 
				'heading' => esc_html__( 'Upload Images', 'gauge' ),
				'description' => esc_html__( 'Manually upload images or leave empty to display images from the child page using the Images page template.', 'gauge' ),
				'param_name' => 'upload_images',
				'type' => 'attach_images',
				'value' => '',
				),				
				array( 
				'heading' => esc_html__( 'Number Of Images', 'gauge' ),
				'description' => esc_html__( 'The number of images to display.', 'gauge' ),
				'param_name' => 'number',
				'type' => 'textfield',
				'value' => '8',
				),	
				array( 
				'heading' => esc_html__( 'Image Width', 'gauge' ),
				'description' => esc_html__( 'The width of the images.', 'gauge' ),
				'param_name' => 'image_width',
				'value' => '114',
				'type' => 'textfield',
				),		 
				array( 
				'heading' => esc_html__( 'Image Height', 'gauge' ),
				'description' => esc_html__( 'The height of the images.', 'gauge' ),
				'param_name' => 'image_height',
				'value' => '118',
				'type' => 'textfield',
				),	
				array( 
				'heading' => esc_html__( 'Hard Crop', 'gauge' ),
				'description' => esc_html__( 'Images are cropped even if it is smaller than the dimensions you want to crop it to.', 'gauge' ),
				'param_name' => 'hard_crop',
				'value' => array( esc_html__( 'Enabled', 'gauge' ) => 'enabled', esc_html__( 'Disabled', 'gauge' ) => 'disabled' ),
				'type' => 'dropdown',
				'dependency' => array( 'element' => 'featured_image', 'value' => 'enabled' ),
				),			 			
				array( 
				'heading' => esc_html__( 'See All', 'gauge' ),
				'description' => esc_html__( 'Add a "See All" link.', 'gauge' ),
				'param_name' => 'see_all',
				'value' => array( esc_html__( 'Disabled', 'gauge' ) => 'disabled', esc_html__( 'Enabled', 'gauge' ) => 'enabled' ),
				'type' => 'dropdown',
				),
				array( 
				'heading' => esc_html__( 'See All Link', 'gauge' ),
				'description' => esc_html__( 'URL for the "See All" link.', 'gauge' ),
				'param_name' => 'see_all_link',
				'type' => 'textfield',
				'dependency' => array( 'element' => 'see_all', 'value' => 'enabled' ),
				),				 			 
				array( 
				'heading' => esc_html__( 'See All Text', 'gauge' ),
				'description' => esc_html__( 'Custom text for the "See All" link.', 'gauge' ),
				'param_name' => 'see_all_text',
				'type' => 'textfield',
				'value' => esc_html__( 'See All Images', 'gauge' ),
				'dependency' => array( 'element' => 'see_all', 'value' => 'enabled' ),
				),					 			 						 		   			 			 
				array( 
				'heading' => esc_html__( 'Extra Class Names', 'gauge' ),
				'description' => esc_html__( 'If you wish to style this particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'gauge' ),
				'type' => 'textfield',
				'param_name' => 'classes',
				'value' => '',
				),																																								
			 )
		) );
		

		/*--------------------------------------------------------------
		News Shortcode
		--------------------------------------------------------------*/

		require_once( ghostpool_vc . 'gp_vc_news.php' );

		vc_map( array( 
			'name' => esc_html__( 'News', 'gauge' ),
			'base' => 'news',
			'description' => esc_html__( 'Display your news posts in a variety of ways.', 'gauge' ),
			'class' => 'wpb_vc_news',
			'controls' => 'full',
			'icon' => 'gp-icon-news',
			'category' => esc_html__( 'Content', 'gauge' ),
			'front_enqueue_js' => array( ghostpool_scripts_uri . 'isotope.pkgd.min.js', ghostpool_scripts_uri . 'imagesLoaded.min.js' ),
			'params' => array( 		
				array( 
				'heading' => esc_html__( 'Title', 'gauge' ),
				'description' => esc_html__( 'The title at the top of the element.', 'gauge' ),
				'param_name' => 'widget_title',
				'type' => 'textfield',
				'admin_label' => true,
				'value' => '',
				),		
				array( 
				'heading' => esc_html__( 'Categories', 'gauge' ),
				'description' => esc_html__( 'Enter the slugs or IDs separating each one with a comma e.g. xbox,ps3,pc.', 'gauge' ),
				'param_name' => 'cats',
				'type' => 'textfield',
				),				
				array( 
				'heading' => esc_html__( 'Post Association', 'gauge' ),
				'description' => esc_html__( 'Only show posts associated with the parent hub page.', 'gauge' ),
				'param_name' => 'post_association',
				'value' => array( esc_html__( 'Enabled', 'gauge' ) => 'enabled', esc_html__( 'Disabled', 'gauge' ) => 'disabled' ),
				'type' => 'dropdown',
				),
				array( 
				'heading' => esc_html__( 'Format', 'gauge' ),
				'description' => esc_html__( 'The format to display the items in.', 'gauge' ),
				'param_name' => 'format',
				'value' => array( esc_html__( 'Standard', 'gauge' ) => 'blog-standard', esc_html__( 'Large', 'gauge' ) => 'blog-large', esc_html__( '1 Column', 'gauge' ) => 'blog-columns-1', esc_html__( '2 Columns', 'gauge' ) => 'blog-columns-2', esc_html__( '3 Columns', 'gauge' ) => 'blog-columns-3', esc_html__( '4 Columns', 'gauge' ) => 'blog-columns-4', esc_html__( '5 Columns', 'gauge' ) => 'blog-columns-5', esc_html__( '6 Columns', 'gauge' ) => 'blog-columns-6', esc_html__( 'Masonry', 'gauge' ) => 'blog-masonry' ),
				'type' => 'dropdown',
				),						
				array( 
				'heading' => esc_html__( 'Size', 'gauge' ),
				'description' => esc_html__( 'The size to display the items at.', 'gauge' ),
				'param_name' => 'size',
				'value' => array( esc_html__( 'Standard', 'gauge' ) => 'blog-standard-size', esc_html__( 'Small', 'gauge' ) => 'blog-small-size'  ),
				'type' => 'dropdown',
				),	
				array( 
				'heading' => esc_html__( 'Order By', 'gauge' ),
				'description' => esc_html__( 'The criteria which the items are ordered by.', 'gauge' ),
				'param_name' => 'orderby',
				'value' => array(
					esc_html__( 'Newest', 'gauge' ) => 'newest',
					esc_html__( 'Oldest', 'gauge' ) => 'oldest',
					esc_html__( 'Title (A-Z)', 'gauge' ) => 'title_az',
					esc_html__( 'Title (Z-A)', 'gauge' ) => 'title_za',
					esc_html__( 'Most Comments', 'gauge' ) => 'comment_count',
					esc_html__( 'Most Views', 'gauge' ) => 'views',
					esc_html__( 'Menu Order', 'gauge' ) => 'menu_order',
					esc_html__( 'Random', 'gauge' ) => 'rand',
				),
				'type' => 'dropdown',
				),	
				array( 
				'heading' => esc_html__( 'Date Posted', 'gauge' ),
				'description' => esc_html__( 'The date the items were posted.', 'gauge' ),
				'param_name' => 'date_posted',
				'value' => array(
					esc_html__( 'Any date', 'gauge' ) => 'all',
					esc_html__( 'In the last year', 'gauge' ) => 'year',
					esc_html__( 'In the last month', 'gauge' ) => 'month',
					esc_html__( 'In the last week', 'gauge' ) => 'week',
					esc_html__( 'In the last day', 'gauge' ) => 'day',
				),
				'type' => 'dropdown',
				),
				array( 
				'heading' => esc_html__( 'Date Modified', 'gauge' ),
				'description' => esc_html__( 'The date the items were modified.', 'gauge' ),
				'param_name' => 'date_modified',
				'value' => array(
					esc_html__( 'Any date', 'gauge' ) => 'all',
					esc_html__( 'In the last year', 'gauge' ) => 'year',
					esc_html__( 'In the last month', 'gauge' ) => 'month',
					esc_html__( 'In the last week', 'gauge' ) => 'week',
					esc_html__( 'In the last day', 'gauge' ) => 'day',
				),
				'type' => 'dropdown',
				),	
				array( 
				'heading' => esc_html__( 'Filter', 'gauge' ),
				'description' => esc_html__( 'Add a dropdown filter menu to the page.', 'gauge' ),
				'param_name' => 'filter',
				'value' => array( esc_html__( 'Disabled', 'gauge' ) => 'disabled', esc_html__( 'Enabled', 'gauge' ) => 'enabled' ),
				'type' => 'dropdown',
				),	
				array(
				'heading' => esc_html__( 'Filter Options', 'gauge' ),
				'param_name' => 'filter_cats',
				'value' => array( esc_html__( 'Categories', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				'dependency' => array( 'element' => 'filter', 'value' => 'enabled' ),
				),	
				array(
				'param_name' => 'filter_date',
				'value' => array( esc_html__( 'Date', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				'dependency' => array( 'element' => 'filter', 'value' => 'enabled' ),
				),	
				array(
				'param_name' => 'filter_title',
				'value' => array( esc_html__( 'Title', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				'dependency' => array( 'element' => 'filter', 'value' => 'enabled' ),
				),								
				array(
				'param_name' => 'filter_comment_count',
				'value' => array( esc_html__( 'Comment Count', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				'dependency' => array( 'element' => 'filter', 'value' => 'enabled' ),
				),
				array(
				'param_name' => 'filter_views',
				'value' => array( esc_html__( 'Views', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				'dependency' => array( 'element' => 'filter', 'value' => 'enabled' ),
				),
				array( 
				'param_name' => 'filter_date_posted',
				'value' => array( esc_html__( 'Date Posted', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				'dependency' => array( 'element' => 'filter', 'value' => 'enabled' ),
				),				
				array( 
				'description' => esc_html__( 'Choose what options to display in the dropdown filter menu.', 'gauge' ),
				'param_name' => 'filter_date_modified',
				'value' => array( esc_html__( 'Date Modified', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				'dependency' => array( 'element' => 'filter', 'value' => 'enabled' ),
				),						
				array( 
				'heading' => esc_html__( 'Filter Category', 'gauge' ),
				'description' => esc_html__( 'Enter the slug or ID number of the category you want to filter by, leave blank to display all categories - the sub categories of this category will also be displayed.', 'gauge' ),
				'param_name' => 'filter_cats_id',
				'type' => 'textfield',
				'dependency' => array( 'element' => 'filter', 'value' => 'enabled' ),
				),		
				array( 
				'heading' => esc_html__( 'Items Per Page', 'gauge' ),
				'description' => esc_html__( 'The number of items on each page.', 'gauge' ),
				'param_name' => 'per_page',
				'value' => '5',
				'type' => 'textfield',
				),
				array( 
				'heading' => esc_html__( 'Offset', 'gauge' ),
				'description' => esc_html__( 'The number of posts to offset by e.g. set to 3 to exclude the first 3 posts.', 'gauge' ),
				'param_name' => 'offset',
				'value' => '',
				'type' => 'textfield',
				),
				array( 
				'heading' => esc_html__( 'Featured Image', 'gauge' ),
				'description' => esc_html__( 'Display the featured images.', 'gauge' ),
				'param_name' => 'featured_image',
				'value' => array( esc_html__( 'Enabled', 'gauge' ) => 'enabled', esc_html__( 'Disabled', 'gauge' ) => 'disabled' ),
				'type' => 'dropdown',
				),	
				array( 
				'heading' => esc_html__( 'Image Width', 'gauge' ),
				'description' => esc_html__( 'The width of the featured images.', 'gauge' ),
				'param_name' => 'image_width',
				'value' => '140',
				'type' => 'textfield',
				'dependency' => array( 'element' => 'featured_image', 'value' => 'enabled' ),
				),		 
				array( 
				'heading' => esc_html__( 'Image Height', 'gauge' ),
				'description' => esc_html__( 'The height of the featured images.', 'gauge' ),
				'param_name' => 'image_height',
				'value' => '140',
				'type' => 'textfield',
				'dependency' => array( 'element' => 'featured_image', 'value' => 'enabled' ),
				),	
				array( 
				'heading' => esc_html__( 'Hard Crop', 'gauge' ),
				'description' => esc_html__( 'Images are cropped even if it is smaller than the dimensions you want to crop it to.', 'gauge' ),
				'param_name' => 'hard_crop',
				'value' => array( esc_html__( 'Enabled', 'gauge' ) => 'enabled', esc_html__( 'Disabled', 'gauge' ) => 'disabled' ),
				'type' => 'dropdown',
				'dependency' => array( 'element' => 'featured_image', 'value' => 'enabled' ),
				),	
				array( 
				'heading' => esc_html__( 'Image Alignment', 'gauge' ),
				'description' => esc_html__( 'Choose how the image aligns with the content.', 'gauge' ),
				'param_name' => 'image_alignment',
				'value' => array( esc_html__( 'Left Align', 'gauge' ) => 'image-align-left', esc_html__( 'Left Wrap', 'gauge' ) => 'image-wrap-left', esc_html__( 'Right Wrap', 'gauge' ) => 'image-wrap-right', esc_html__( 'Above Content', 'gauge' ) => 'image-above', esc_html__( 'Right Align', 'gauge' ) => 'image-align-right' ),
				'type' => 'dropdown',
				'dependency' => array( 'element' => 'featured_image', 'value' => 'enabled' ),
				),
				array( 
				'heading' => esc_html__( 'Title Position', 'gauge' ),
				'description' => esc_html__( 'The position of the title.', 'gauge' ),
				'param_name' => 'title_position',
				'value' => array( esc_html__( 'Next To Thumbnail', 'gauge' ) => 'title-next-to-thumbnail', esc_html__( 'Over Thumbnail', 'gauge' ) => 'title-over-thumbnail' ),
				'type' => 'dropdown',
				),				
				array( 
				'heading' => esc_html__( 'Content Display', 'gauge' ),
				'description' => esc_html__( 'The amount of content displayed.', 'gauge' ),
				'param_name' => 'content_display',
				'value' => array( esc_html__( 'Excerpt', 'gauge' ) => 'excerpt', esc_html__( 'Full Content', 'gauge' ) => 'full_content' ),
				'type' => 'dropdown',
				),
				array( 
				'heading' => esc_html__( 'Excerpt Length', 'gauge' ),
				'description' => esc_html__( 'The number of characters in excerpts.', 'gauge' ),
				'param_name' => 'excerpt_length',
				'value' => '100',
				'type' => 'textfield',
				'dependency' => array( 'element' => 'content_display', 'value' => 'excerpt' ),
				),	
				array(
				'heading' => esc_html__( 'Post Meta', 'gauge' ),
				'param_name' => 'meta_author',
				'value' => array( esc_html__( 'Author Name', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				),	
				array(
				'param_name' => 'meta_date',
				'value' => array( esc_html__( 'Post Date', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				),				
				array(
				'param_name' => 'meta_views',
				'value' => array( esc_html__( 'Views', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				),		
				array(
				'param_name' => 'meta_comment_count',
				'value' => array( esc_html__( 'Comment Count', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				),
				array( 
				'param_name' => 'meta_cats',
				'value' => array( esc_html__( 'Post Categories', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				),
				array(
				'description' => esc_html__( 'Select the meta data you want to display.', 'gauge' ),
				'param_name' => 'meta_tags',
				'value' => array( esc_html__( 'Post Tags', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				),	
				array( 
				'heading' => esc_html__( 'Read More Link', 'gauge' ),
				'description' => esc_html__( 'Add a read more link below the content.', 'gauge' ),
				'param_name' => 'read_more_link',
				'value' => array( esc_html__( 'Enabled', 'gauge' ) => 'enabled', esc_html__( 'Disabled', 'gauge' ) => 'disabled' ),
				'type' => 'dropdown',
				),				 
				array( 
				'heading' => esc_html__( 'Pagination', 'gauge' ),
				'description' => esc_html__( 'Add pagination.', 'gauge' ),
				'param_name' => 'page_numbers',
				'value' => array( esc_html__( 'Disabled', 'gauge' ) => 'disabled', esc_html__( 'Enabled', 'gauge' ) => 'enabled' ),
				'type' => 'dropdown',
				),		 			
				array( 
				'heading' => esc_html__( 'See All', 'gauge' ),
				'description' => esc_html__( 'Add a "See All" link.', 'gauge' ),
				'param_name' => 'see_all',
				'value' => array( esc_html__( 'Disabled', 'gauge' ) => 'disabled', esc_html__( 'Enabled', 'gauge' ) => 'enabled' ),
				'type' => 'dropdown',
				),
				array( 
				'heading' => esc_html__( 'See All Link', 'gauge' ),
				'description' => esc_html__( 'URL for the "See All" link.', 'gauge' ),
				'param_name' => 'see_all_link',
				'type' => 'textfield',
				'dependency' => array( 'element' => 'see_all', 'value' => 'enabled' ),
				),					 			 
				array( 
				'heading' => esc_html__( 'See All Text', 'gauge' ),
				'description' => esc_html__( 'Custom text for the "See All" link.', 'gauge' ),
				'param_name' => 'see_all_text',
				'type' => 'textfield',
				'value' => esc_html__( 'See All News', 'gauge' ),
				'dependency' => array( 'element' => 'see_all', 'value' => 'enabled' ),
				),		 				 		   			 			 
				array( 
				'heading' => esc_html__( 'Extra Class Names', 'gauge' ),
				'param_name' => 'classes',
				'value' => '',
				'description' => esc_html__( 'If you wish to style this particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'gauge' ),
				'type' => 'textfield',
				),																																								
			 )
		) );
		
				
		/*--------------------------------------------------------------
		Portfolio Shortcode
		--------------------------------------------------------------*/

		require_once( ghostpool_vc . 'gp_vc_portfolio.php' );
		
		vc_map( array( 
			'name' => esc_html__( 'Portfolio', 'gauge' ),
			'base' => 'portfolio',
			'description' => esc_html__( 'Display your portfolio items in a variety of ways.', 'gauge' ),
			'class' => 'wpb_vc_portfolio',
			'controls' => 'full',
			'icon' => 'gp-icon-portfolio',
			'category' => esc_html__( 'Content', 'gauge' ),
			'front_enqueue_js' => array( ghostpool_scripts_uri . 'isotope.pkgd.min.js', ghostpool_scripts_uri . 'imagesLoaded.min.js' ),
			'params' => array( 					
				array( 
				'heading' => esc_html__( 'Title', 'gauge' ),
				'description' => esc_html__( 'The title at the top of the element.', 'gauge' ),
				'param_name' => 'widget_title',
				'type' => 'textfield',
				'admin_label' => true,
				'value' => '',
				),		
				array( 
				'heading' => esc_html__( 'Format', 'gauge' ),
				'description' => esc_html__( 'The format to display the items in.', 'gauge' ),
				'param_name' => 'format',
				'value' => array( esc_html__( '2 Columns', 'gauge' ) => 'portfolio-columns-2', esc_html__( '3 Columns', 'gauge' ) => 'portfolio-columns-3', esc_html__( '4 Columns', 'gauge' ) => 'portfolio-columns-4', esc_html__( '5 Columns', 'gauge' ) => 'portfolio-columns-5', esc_html__( '6 Columns', 'gauge' ) => 'portfolio-columns-6', esc_html__( 'Masonry', 'gauge' ) => 'portfolio-masonry' ),
				'type' => 'dropdown',
				),
				array( 
				'heading' => esc_html__( 'Categories', 'gauge' ),
				'description' => esc_html__( 'Enter the slugs or IDs separating each one with a comma e.g. xbox,ps3,pc.', 'gauge' ),
				'param_name' => 'cats',
				'value' => '',
				'type' => 'textfield',
				),		 
				array( 
				'heading' => esc_html__( 'Order By', 'gauge' ),
				'description' => esc_html__( 'The criteria which the items are ordered by.', 'gauge' ),
				'param_name' => 'orderby',
				'value' => array(
					esc_html__( 'Newest', 'gauge' ) => 'newest',
					esc_html__( 'Oldest', 'gauge' ) => 'oldest',
					esc_html__( 'Title (A-Z)', 'gauge' ) => 'title_az',
					esc_html__( 'Title (Z-A)', 'gauge' ) => 'title_za',
					esc_html__( 'Most Comments', 'gauge' ) => 'comment_count',
					esc_html__( 'Most Views', 'gauge' ) => 'views',
					esc_html__( 'Menu Order', 'gauge' ) => 'menu_order',
					esc_html__( 'Random', 'gauge' ) => 'rand',
				),
				'type' => 'dropdown',
				),
				array( 
				'heading' => esc_html__( 'Date Posted', 'gauge' ),
				'description' => esc_html__( 'The date the items were posted.', 'gauge' ),
				'param_name' => 'date_posted',
				'value' => array(
					esc_html__( 'Any date', 'gauge' ) => 'all',
					esc_html__( 'In the last year', 'gauge' ) => 'year',
					esc_html__( 'In the last month', 'gauge' ) => 'month',
					esc_html__( 'In the last week', 'gauge' ) => 'week',
					esc_html__( 'In the last day', 'gauge' ) => 'day',
				),
				'type' => 'dropdown',
				),
				array( 
				'heading' => esc_html__( 'Date Modified', 'gauge' ),
				'description' => esc_html__( 'The date the items were modified.', 'gauge' ),
				'param_name' => 'date_modified',
				'value' => array(
					esc_html__( 'Any date', 'gauge' ) => 'all',
					esc_html__( 'In the last year', 'gauge' ) => 'year',
					esc_html__( 'In the last month', 'gauge' ) => 'month',
					esc_html__( 'In the last week', 'gauge' ) => 'week',
					esc_html__( 'In the last day', 'gauge' ) => 'day',
				),
				'type' => 'dropdown',
				),				
				array( 
				'heading' => esc_html__( 'Filter', 'gauge' ),
				'description' => esc_html__( 'Add category filter links to the page.', 'gauge' ),
				'param_name' => 'filter',
				'value' => array( esc_html__( 'Enabled', 'gauge' ) => 'enabled', esc_html__( 'Disabled', 'gauge' ) => 'disabled' ),
				'type' => 'dropdown',
				),			 
				array( 
				'heading' => esc_html__( 'Items Per Page', 'gauge' ),
				'description' => esc_html__( 'The number of items on each page.', 'gauge' ),
				'param_name' => 'per_page',
				'value' => '12',
				'type' => 'textfield',
				),
				array( 
				'heading' => esc_html__( 'Offset', 'gauge' ),
				'description' => esc_html__( 'The number of posts to offset by e.g. set to 3 to exclude the first 3 posts.', 'gauge' ),
				'param_name' => 'offset',
				'value' => '',
				'type' => 'textfield',
				),
				array( 
				'heading' => esc_html__( 'Pagination', 'gauge' ),
				'description' => esc_html__( 'Add pagination.', 'gauge' ),
				'param_name' => 'page_numbers',
				'value' => array( esc_html__( 'Enabled', 'gauge' ) => 'enabled', esc_html__( 'Disabled', 'gauge' ) => 'disabled' ),
				'type' => 'dropdown',
				),	
				array( 
				'heading' => esc_html__( 'See All', 'gauge' ),
				'description' => esc_html__( 'Add a "See All" link.', 'gauge' ),
				'param_name' => 'see_all',
				'value' => array( esc_html__( 'Disabled', 'gauge' ) => 'disabled', esc_html__( 'Enabled', 'gauge' ) => 'enabled' ),
				'type' => 'dropdown',
				),
				array( 
				'heading' => esc_html__( 'See All Link', 'gauge' ),
				'description' => esc_html__( 'URL for the "See All" link.', 'gauge' ),
				'param_name' => 'see_all_link',
				'type' => 'textfield',
				'dependency' => array( 'element' => 'see_all', 'value' => 'enabled' ),
				),					 			 
				array( 
				'heading' => esc_html__( 'See All Text', 'gauge' ),
				'description' => esc_html__( 'Custom text for the "See All" link.', 'gauge' ),
				'param_name' => 'see_all_text',
				'type' => 'textfield',
				'value' => esc_html__( 'See All Items', 'gauge' ),
				'dependency' => array( 'element' => 'see_all', 'value' => 'enabled' ),
				),						 		 				 		   			 			 
				array( 
				'heading' => esc_html__( 'Extra Class Names', 'gauge' ),
				'description' => esc_html__( 'If you wish to style this particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'gauge' ),
				'param_name' => 'classes',
				'value' => '',
				'type' => 'textfield',
				),																																								
			 )
		) );


		/*--------------------------------------------------------------
		Pricing Table Shortcode
		--------------------------------------------------------------*/

		// Pricing Table
		vc_map( array( 
			'name' => esc_html__( 'Pricing Table', 'gauge' ),
			'base' => 'pricing_table',
			'description' => esc_html__( 'A table to compare the prices of different items.', 'gauge' ),
			'as_parent' => array( 'only' => 'pricing_column' ),
			'controls' => 'full',
			'icon' => 'gp-icon-pricing-table',
			'category' => esc_html__( 'Content', 'gauge' ),
			'js_view' => 'VcColumnView',
			'params' => array( 
				array( 
				'heading' => esc_html__( 'Extra Class Names', 'gauge' ),
				'param_name' => 'classes',
				'value' => '',
				'description' => esc_html__( 'If you wish to style this particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'gauge' ),
				'type' => 'textfield',
				),	
			),
			'js_view' => 'VcColumnView'
		 ) );


		// Pricing Column
		vc_map( array( 
			'name' => esc_html__( 'Pricing Column', 'gauge' ),
			'base' => 'pricing_column',
			'content_element' => true,
			'as_child' => array( 'only' => 'pricing_table' ),
			'icon' => 'gp-icon-pricing-table',
			'params' => array( 	
				array( 
				'heading' => esc_html__( 'Column Title', 'gauge' ),
				'description' => esc_html__( 'The title for the column.', 'gauge' ),
				'param_name' => 'title',
				'value' => '',
				'type' => 'textfield'
				),
				array( 
				'heading' => esc_html__( 'Price', 'gauge' ),
				'description' => esc_html__( 'The price for the column.', 'gauge' ),
				'param_name' => 'price',
				'value' => '',
				'type' => 'textfield'
				),
				array( 
				'heading' => esc_html__( 'Currency Symbol', 'gauge' ),
				'description' => esc_html__( 'The currency symbol.', 'gauge' ),
				'param_name' => 'currency_symbol',
				'value' => '',
				'type' => 'textfield',
				),		
				array( 
				'heading' => esc_html__( 'Interval', 'gauge' ),
				'description' => esc_html__( 'The interval for the column e.g. per week, per month.', 'gauge' ),
				'param_name' => 'interval',
				'value' => '',
				'type' => 'textfield',
				),		
				array( 
				'heading' => esc_html__( 'Highlight Column', 'gauge' ),
				'description' => esc_html__( 'Make this column stand out.', 'gauge' ),
				'param_name' => 'highlight',
				'value' => array( esc_html__( 'Disabled', 'gauge' ) => 'disabled', esc_html__( 'Enabled', 'gauge' ) => 'enabled' ),
				'type' => 'dropdown'
				),	
				array( 
				'heading' => esc_html__( 'Highlight Text', 'gauge' ),
				'description' => esc_html__( 'Add highlight text above the column title.', 'gauge' ),
				'param_name' => 'highlight_text',
				'value' => '',
				'dependency' => array( 'element' => 'highlight', 'value' => 'enabled' ),
				'type' => 'textfield',
				),	
				array( 
				'heading' => esc_html__( 'Content', 'gauge' ),
				'description' => esc_html__( 'Use the Unordered List button to create the points in your pricing column. You can also add shortcodes such as the [button link="#"] shortcode seen in the site demo.', 'gauge' ),
				'param_name' => 'content',
				'type' => 'textarea_html',
				),
				array( 
				'heading' => esc_html__( 'Highlight Color', 'gauge' ),
				'description' => esc_html__( 'The highlight color.', 'gauge' ),
				'param_name' => 'highlight_color',
				'value' => '#f84103',
				'dependency' => array( 'element' => 'highlight', 'value' => 'enabled' ),
				'type' => 'colorpicker',
				),		
				array( 
				'heading' => esc_html__( 'Title Color', 'gauge' ),
				'description' => esc_html__( 'The title color.', 'gauge' ),
				'param_name' => 'title_color',
				'value' => '#f84103',
				'dependency' => array( 'element' => 'highlight', 'value' => 'disabled' ),
				'type' => 'colorpicker',
				),	
				array( 
				'heading' => esc_html__( 'Highlight Title Color', 'gauge' ),
				'description' => esc_html__( 'The highlight title color.', 'gauge' ),
				'param_name' => 'highlight_title_color',
				'value' => '#fff',
				'dependency' => array( 'element' => 'highlight', 'value' => 'enabled' ),
				'type' => 'colorpicker',
				),	
				array( 
				'heading' => esc_html__( 'Background Color', 'gauge' ),
				'description' => esc_html__( 'The background color.', 'gauge' ),
				'param_name' => 'background_color',
				'value' => '#f7f7f7',
				'dependency' => array( 'element' => 'highlight', 'value' => 'disabled' ),
				'type' => 'colorpicker',
				),		 
				array( 
				'heading' => esc_html__( 'Highlight Background Color', 'gauge' ),
				'description' => esc_html__( 'The highlight background color.', 'gauge' ),
				'param_name' => 'highlight_background_color',
				'value' => '#fff',
				'dependency' => array( 'element' => 'highlight', 'value' => 'enabled' ),
				'type' => 'colorpicker',
				),		 		 		 
				array( 
				'heading' => esc_html__( 'Text Color', 'gauge' ),
				'description' => esc_html__( 'The text color.', 'gauge' ),
				'param_name' => 'text_color',
				'value' => '#747474',
				'type' => 'colorpicker',
				),	
				array( 
				'heading' => esc_html__( 'Border', 'gauge' ),
				'description' => esc_html__( 'Add a border around the columns.', 'gauge' ),
				'param_name' => 'border',
				'value' => array( esc_html__( 'Enabled', 'gauge' ) => 'enabled', esc_html__( 'Disabled', 'gauge' ) => 'disabled' ),
				'type' => 'dropdown',
				),			 
				array( 
				'heading' => esc_html__( 'Border Color', 'gauge' ),
				'description' => esc_html__( 'The border color.', 'gauge' ),
				'param_name' => 'border_color',
				'value' => '#e7e7e7',
				'dependency' => array( 'element' => 'border', 'value' => 'enabled' ),
				'type' => 'colorpicker',
				),	 		 																																							
			 )
		 ) );

		class WPBakeryShortCode_Pricing_Table extends WPBakeryShortCodesContainer {}
		class WPBakeryShortCode_Pricing_Column extends WPBakeryShortCode {}	


		/*--------------------------------------------------------------
		Progress Bar Shortcode (add-on)
		--------------------------------------------------------------*/

		vc_add_param( 'vc_progress_bar', array( 
		'heading' => esc_html__( 'Text Color', 'gauge' ),
		'param_name' => 'textcolor',
		'class' => '',
		'description' => esc_html__( 'Select custom text color for bars.', 'gauge' ),
		'type' => 'colorpicker',
		 ) );


		/*--------------------------------------------------------------
		Ranking Shortcode
		--------------------------------------------------------------*/

		require_once( ghostpool_vc . 'gp_vc_ranking.php' );

		vc_map( array( 
			'name' => esc_html__( 'Ranking', 'gauge' ),
			'base' => 'ranking',
			'description' => esc_html__( 'Rank hub pages in a variety of ways.', 'gauge' ),
			'class' => 'wpb_vc_ranking',
			'controls' => 'full',
			'icon' => 'gp-icon-ranking',
			'category' => esc_html__( 'Content', 'gauge' ),
			'params' => array(		
				array( 
				'heading' => esc_html__( 'Title', 'gauge' ),
				'description' => esc_html__( 'The title at the top of the element.', 'gauge' ),
				'param_name' => 'widget_title',
				'type' => 'textfield',
				'admin_label' => true,
				'value' => '',
				),		
				array(
				'heading' => esc_html__( 'Type', 'gauge' ),
				'description' => esc_html__( 'Select type of content you want to display.', 'gauge' ),
				'param_name' => 'type',
				'value' => array(
					esc_html__( 'Hubs', 'gauge' ) => 'hubs',
					esc_html__( 'Reviews', 'gauge' ) => 'reviews',
					esc_html__( 'Hubs and Reviews', 'gauge' ) => 'both',
				),
				'type' => 'dropdown',
				),				
				array( 
				'heading' => esc_html__( 'Categories', 'gauge' ),
				'description' => esc_html__( 'Enter the slugs or IDs separating each one with a comma e.g. xbox,ps3,pc.', 'gauge' ),
				'param_name' => 'cats',
				'type' => 'textfield',
				),				
				array( 
				'heading' => esc_html__( 'Hub Fields', 'gauge' ),
				'description' => wp_kses( __( 'Enter the hub field slugs you want to filter by. Add your taxonomy slug followed by a colon. Next enter your terms separating each by a colon also. Next add a comma and then enter the next taxonomy and so on e.g. <code>taxonomy-1:term-1:term-2,taxonomy-2:term-1,taxonomy-3:term-1:term-2</code>, this would translate to <code>genre:action:role-playing,publisher:namco,developed-by:namco:bluepoint-games</code>', 'gauge' ),  array( 'code' => array() ) ),
				'param_name' => 'hub_fields',
				'type' => 'textfield',
				'value' => '',
				),	
				array( 
				'heading' => esc_html__( 'Order By', 'gauge' ),
				'description' => esc_html__( 'The criteria which the items are ordered by.', 'gauge' ),
				'param_name' => 'orderby',
				'value' => array(
					esc_html__( 'Top Site Rated', 'gauge' ) => 'site_rating',
					esc_html__( 'Newest', 'gauge' ) => 'newest',
					esc_html__( 'Oldest', 'gauge' ) => 'oldest',
					esc_html__( 'Title (A-Z)', 'gauge' ) => 'title_az',
					esc_html__( 'Title (Z-A)', 'gauge' ) => 'title_za',
					esc_html__( 'Most Comments', 'gauge' ) => 'comment_count',
					esc_html__( 'Most Views', 'gauge' ) => 'views',
					esc_html__( 'Most Followers', 'gauge' ) => 'followers',
					esc_html__( 'Top User Rated', 'gauge' ) => 'user_rating',
					esc_html__( 'Hub Awards', 'gauge' ) => 'hub_awards',
					esc_html__( 'Menu Order', 'gauge' ) => 'menu_order',
					esc_html__( 'Random', 'gauge' ) => 'rand',
				),
				'type' => 'dropdown',
				),					
				array( 
				'heading' => esc_html__( 'Date Posted', 'gauge' ),
				'description' => esc_html__( 'The date the items were posted.', 'gauge' ),
				'param_name' => 'date_posted',
				'value' => array(
					esc_html__( 'Any date', 'gauge' ) => 'all',
					esc_html__( 'In the last year', 'gauge' ) => 'year',
					esc_html__( 'In the last month', 'gauge' ) => 'month',
					esc_html__( 'In the last week', 'gauge' ) => 'week',
					esc_html__( 'In the last day', 'gauge' ) => 'day',
				),
				'type' => 'dropdown',
				),
				array( 
				'heading' => esc_html__( 'Date Modified', 'gauge' ),
				'description' => esc_html__( 'The date the items were modified.', 'gauge' ),
				'param_name' => 'date_modified',
				'value' => array(
					esc_html__( 'Any date', 'gauge' ) => 'all',
					esc_html__( 'In the last year', 'gauge' ) => 'year',
					esc_html__( 'In the last month', 'gauge' ) => 'month',
					esc_html__( 'In the last week', 'gauge' ) => 'week',
					esc_html__( 'In the last day', 'gauge' ) => 'day',
				),
				'type' => 'dropdown',
				),					 
				array( 
				'heading' => esc_html__( 'Items', 'gauge' ),
				'description' => esc_html__( 'The number of items.', 'gauge' ),
				'param_name' => 'per_page',
				'value' => '5',
				'type' => 'textfield',
				),
				array( 
				'heading' => esc_html__( 'Offset', 'gauge' ),
				'description' => esc_html__( 'The number of posts to offset by e.g. set to 3 to exclude the first 3 posts.', 'gauge' ),
				'param_name' => 'offset',
				'value' => '',
				'type' => 'textfield',
				),
				array( 
				'heading' => esc_html__( 'Featured Image', 'gauge' ),
				'description' => esc_html__( 'Display the featured images.', 'gauge' ),
				'param_name' => 'featured_image',
				'value' => array( esc_html__( 'Enabled', 'gauge' ) => 'enabled', esc_html__( 'Disabled', 'gauge' ) => 'disabled' ),
				'type' => 'dropdown',
				),	
				array( 
				'heading' => esc_html__( 'Top Ranked Image Width', 'gauge' ),
				'description' => esc_html__( 'The width of the top ranked featured image.', 'gauge' ),
				'param_name' => 'large_image_width',
				'value' => '120',
				'type' => 'textfield',
				'dependency' => array( 'element' => 'featured_image', 'value' => 'enabled' ),
				),		 
				array( 
				'heading' => esc_html__( 'Top Ranked Image Height', 'gauge' ),
				'description' => esc_html__( 'The height of the top ranked featured image.', 'gauge' ),
				'param_name' => 'large_image_height',
				'value' => '120',
				'type' => 'textfield',
				'dependency' => array( 'element' => 'featured_image', 'value' => 'enabled' ),
				),	
				array( 
				'heading' => esc_html__( 'Small Image Width', 'gauge' ),
				'description' => esc_html__( 'The width of the small featured images.', 'gauge' ),
				'param_name' => 'small_image_width',
				'value' => '80',
				'type' => 'textfield',
				'dependency' => array( 'element' => 'featured_image', 'value' => 'enabled' ),
				),		 
				array( 
				'heading' => esc_html__( 'Small Image Height', 'gauge' ),
				'description' => esc_html__( 'The height of the small featured images.', 'gauge' ),
				'param_name' => 'small_image_height',
				'value' => '80',
				'type' => 'textfield',
				'dependency' => array( 'element' => 'featured_image', 'value' => 'enabled' ),
				),	
				array( 
				'heading' => esc_html__( 'Background Image Width', 'gauge' ),
				'description' => esc_html__( 'The width of the background images.', 'gauge' ),
				'param_name' => 'bg_image_width',
				'value' => '370',
				'type' => 'textfield',
				),		 
				array( 
				'heading' => esc_html__( 'Background Image Height', 'gauge' ),
				'description' => esc_html__( 'The height of the background images.', 'gauge' ),
				'param_name' => 'bg_image_height',
				'value' => '255',
				'type' => 'textfield',
				),					
				array( 
				'heading' => esc_html__( 'Hard Crop', 'gauge' ),
				'description' => esc_html__( 'Images are cropped even if it is smaller than the dimensions you want to crop it to.', 'gauge' ),
				'param_name' => 'hard_crop',
				'value' => array( esc_html__( 'Enabled', 'gauge' ) => 'enabled', esc_html__( 'Disabled', 'gauge' ) => 'disabled' ),
				'type' => 'dropdown',
				'dependency' => array( 'element' => 'featured_image', 'value' => 'enabled' ),
				),
				array(
				'heading' => esc_html__( 'Post Meta', 'gauge' ),
				'param_name' => 'meta_hub_cats',
				'value' => array( esc_html__( 'Hub Categories', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				),					
				array(
				'description' => esc_html__( 'Select the meta data you want to display.', 'gauge' ),
				'param_name' => 'meta_hub_fields',
				'value' => array( esc_html__( 'Hub Fields', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				),
				array(
				'heading' => esc_html__( 'Ratings', 'gauge' ),
				'param_name' => 'display_site_rating',
				'value' => array( esc_html__( 'Site Rating', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				),					
				array(
				'description' => esc_html__( 'Select the ratings you want to display.', 'gauge' ),
				'param_name' => 'display_user_rating',
				'value' => array( esc_html__( 'User Rating', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				), 				 		   			 			 
				array( 
				'heading' => esc_html__( 'Extra Class Names', 'gauge' ),
				'param_name' => 'classes',
				'value' => '',
				'description' => esc_html__( 'If you wish to style this particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'gauge' ),
				'type' => 'textfield',
				),																																								
			 )
		) );


		/*--------------------------------------------------------------
		Team Shortcode
		--------------------------------------------------------------*/

		// Team Wrapper
		vc_map( array( 
			'name' => esc_html__( 'Team', 'gauge' ),
			'base' => 'team',
			'description' => esc_html__( 'Display your team members.', 'gauge' ),
			'as_parent' => array( 'only' => 'team_member' ), 
			'class' => 'wpb_vc_team',
			'controls' => 'full',
			'icon' => 'gp-icon-team',
			'category' => esc_html__( 'Content', 'gauge' ),
			'js_view' => 'VcColumnView',
			'params' => array( 	
				array( 
				'heading' => esc_html__( 'Columns', 'gauge' ),
				'param_name' => 'columns',
				'value' => '3',
				'description' => esc_html__( 'The number of columns.', 'gauge' ),
				'type' => 'textfield',
				),		
				array( 
				'heading' => esc_html__( 'Extra Class Names', 'gauge' ),
				'description' => esc_html__( 'If you wish to style this particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'gauge' ),
				'param_name' => 'classes',
				'value' => '',
				'type' => 'textfield',
				),																																								
			 ),
			'js_view' => 'VcColumnView',
		) );

		// Team Member
		vc_map( array( 
			'name' => esc_html__( 'Team Member', 'gauge' ),
			'base' => 'team_member',
			'icon' => 'gp-icon-team',
			'content_element' => true,
			'as_child' => array( 'only' => 'team' ),
			'params' => array( 	
				array( 
				'heading' => esc_html__( 'Image', 'gauge' ),
				'description' => esc_html__( 'The team member image.', 'gauge' ),
				'param_name' => 'image_url',
				'value' => '',
				'type' => 'attach_image'
				),
				array( 
				'heading' => esc_html__( 'Image Width', 'gauge' ),
				'description' => esc_html__( 'The width of the team member image.', 'gauge' ),
				'param_name' => 'image_width',
				'value' => '230',
				'type' => 'textfield',
				),		
				array( 
				'heading' => esc_html__( 'Image Height', 'gauge' ),
				'description' => esc_html__( 'The height of the team member image.', 'gauge' ),
				'param_name' => 'image_height',
				'value' => '230',
				'type' => 'textfield',
				),			
				array( 
				'heading' => esc_html__( 'Name', 'gauge' ),
				'description' => esc_html__( 'The name of the team member.', 'gauge' ),
				'param_name' => 'name',
				'admin_label' => true,
				'value' => '',
				'type' => 'textfield'
				),	
				array( 
				'heading' => esc_html__( 'Position', 'gauge' ),
				'description' => esc_html__( 'The position of the team member e.g. CEO', 'gauge' ),
				'param_name' => 'position',
				'value' => '',
				'type' => 'textfield',
				),
				array( 
				'heading' => esc_html__( 'Link', 'gauge' ),
				'description' => esc_html__( 'Add a link for the team member image.', 'gauge' ),
				'param_name' => 'link',
				'value' => '',
				'type' => 'textfield',
				),	
				array( 
				'heading' => esc_html__( 'Link Target', 'gauge' ),
				'description' => esc_html__( 'The link target for the team member image.', 'gauge' ),
				'param_name' => 'link_target',
				'value' => array( esc_html__( 'Same Window', 'gauge' ) => '_self', esc_html__( 'New Window', 'gauge' ) => '_blank' ),
				'type' => 'dropdown',
				'dependency' => array( 'element' => 'link', 'not_empty' => true ),
				),				
				array( 
				'heading' => esc_html__( 'Description', 'gauge' ),
				'description' => esc_html__( 'The description of the team member.', 'gauge' ),
				'param_name' => 'content',
				'value' => '',
				'type' => 'textarea_html',
				),																																								
			 )
		 ) );

		class WPBakeryShortCode_Team extends WPBakeryShortCodesContainer {}
		class WPBakeryShortCode_Team_Member extends WPBakeryShortCode {}											


		/*--------------------------------------------------------------
		Testimonials Shortcode
		--------------------------------------------------------------*/

		// Testimonial Slider
		vc_map( array( 
			'name' => esc_html__( 'Testimonial Slider', 'gauge' ),
			'base' => 'testimonial_slider',
			'description' => esc_html__( 'Show your testimonials in a slider.', 'gauge' ),
			'as_parent' => array( 'only' => 'testimonial' ), 
			'class' => 'wpb_vc_testimonial',
			'controls' => 'full',
			'icon' => 'gp-icon-testimonial-slider',
			'category' => esc_html__( 'Content', 'gauge' ),
			'js_view' => 'VcColumnView',
			'params' => array( 	
				array( 
				'heading' => esc_html__( 'Effect', 'gauge' ),
				'param_name' => 'effect',
				'value' => array( esc_html__( 'Fade', 'gauge' ) => 'fade', esc_html__( 'Slide', 'gauge' ) => 'slide' ),
				'description' => esc_html__( 'The slider effect.', 'gauge' ),
				'type' => 'dropdown'
				),
				array( 
				'heading' => esc_html__( 'Slider Speed', 'gauge' ),
				'param_name' => 'speed',
				'value' => '0',
				'description' => esc_html__( 'The number of seconds between slide transitions, set to 0 to disable the autoplay.', 'gauge' ),
				'type' => 'textfield',
				),		
				array( 
				'heading' => esc_html__( 'Buttons', 'gauge' ),
				'param_name' => 'buttons',
				'value' => array( esc_html__( 'Show', 'gauge' ) => 'true', esc_html__( 'Hide', 'gauge' ) => 'false' ),
				'description' => esc_html__( 'The slider buttons.', 'gauge' ),
				'type' => 'dropdown',
				),				
				array( 
				'heading' => esc_html__( 'Extra Class Names', 'gauge' ),
				'description' => esc_html__( 'If you wish to style this particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'gauge' ),
				'param_name' => 'classes',
				'value' => '',
				'type' => 'textfield',
				),																																								
			 ),
			'js_view' => 'VcColumnView'
		 ) );


		// Testimonial Slide
		vc_map( array( 
			'name' => esc_html__( 'Testimonial', 'gauge' ),
			'base' => 'testimonial',
			'content_element' => true,
			'as_child' => array( 'only' => 'testimonial_slider' ),
			'icon' => 'gp-icon-testimonial-slider',
			'params' => array( 	
				array( 
				'heading' => esc_html__( 'Image', 'gauge' ),
				'description' => esc_html__( 'The testimonial slide image.', 'gauge' ),
				'param_name' => 'image_url',
				'value' => '',
				'type' => 'attach_image'
				),
				array( 
				'heading' => esc_html__( 'Image Width', 'gauge' ),
				'description' => esc_html__( 'The width the testimonial slide image.', 'gauge' ),
				'param_name' => 'image_width',
				'value' => '120',
				'description' => '',
				'type' => 'textfield',
				),		
				array( 
				'heading' => esc_html__( 'Image Height', 'gauge' ),
				'description' => esc_html__( 'The height of the testimonial slide images.', 'gauge' ),
				'param_name' => 'image_height',
				'value' => '120',
				'type' => 'textfield',
				),		
				array( 
				'heading' => esc_html__( 'Quote', 'gauge' ),
				'description' => esc_html__( 'The tesitmonial quote.', 'gauge' ),
				'param_name' => 'content',
				'value' => '',
				'type' => 'textarea_html',
				),		
				array( 
				'heading' => esc_html__( 'Name', 'gauge' ),
				'description' => esc_html__( 'The name of the person who gave the testimonial.', 'gauge' ),
				'param_name' => 'name',
				'value' => '',
				'type' => 'textfield',
				),																																								
			 )
		 ) );

		class WPBakeryShortCode_Testimonial_Slider extends WPBakeryShortCodesContainer {}
		class WPBakeryShortCode_Testimonial extends WPBakeryShortCode {}																																


		/*--------------------------------------------------------------
		User Rating Box Shortcode
		--------------------------------------------------------------*/

		require_once( ghostpool_vc . 'gp_vc_user_rating_box.php' );

		vc_map( array( 
			'name' => esc_html__( 'User Rating Box', 'gauge' ),
			'base' => 'user_rating_box',
			'description' => esc_html__( 'Display the user rating box.', 'gauge' ),
			'class' => 'wpb_vc_user_rating_box',
			'controls' => 'full',
			"show_settings_on_create" => false,
			'icon' => 'gp-icon-user-rating-box',
			'category' => esc_html__( 'Content', 'gauge' ),
			'params' => array(),
		) );


		/*--------------------------------------------------------------
		Videos Shortcode
		--------------------------------------------------------------*/

		require_once( ghostpool_vc . 'gp_vc_videos.php' );

		vc_map( array( 
			'name' => esc_html__( 'Videos', 'gauge' ),
			'base' => 'videos',
			'description' => esc_html__( 'Display your video posts in a variety of ways.', 'gauge' ),
			'class' => 'wpb_vc_videos',
			'controls' => 'full',
			'icon' => 'gp-icon-videos',
			'category' => esc_html__( 'Content', 'gauge' ),
			'front_enqueue_js' => array( ghostpool_scripts_uri . 'isotope.pkgd.min.js', ghostpool_scripts_uri . 'imagesLoaded.min.js' ),
			'params' => array( 		
				array( 
				'heading' => esc_html__( 'Title', 'gauge' ),
				'description' => esc_html__( 'The title at the top of the element.', 'gauge' ),
				'param_name' => 'widget_title',
				'type' => 'textfield',
				'admin_label' => true,
				'value' => '',
				),		
				array( 
				'heading' => esc_html__( 'Categories', 'gauge' ),
				'description' => esc_html__( 'Enter the slugs or IDs separating each one with a comma e.g. xbox,ps3,pc.', 'gauge' ),
				'param_name' => 'cats',
				'type' => 'textfield',
				),				
				array( 
				'heading' => esc_html__( 'Post Association', 'gauge' ),
				'description' => esc_html__( 'Only show posts associated with the parent hub page.', 'gauge' ),
				'param_name' => 'post_association',
				'value' => array( esc_html__( 'Enabled', 'gauge' ) => 'enabled', esc_html__( 'Disabled', 'gauge' ) => 'disabled' ),
				'type' => 'dropdown',
				),
				array( 
				'heading' => esc_html__( 'Format', 'gauge' ),
				'description' => esc_html__( 'The format to display the items in.', 'gauge' ),
				'param_name' => 'format',
				'value' => array( esc_html__( 'Standard', 'gauge' ) => 'blog-standard', esc_html__( 'Large', 'gauge' ) => 'blog-large', esc_html__( '1 Column', 'gauge' ) => 'blog-columns-1', esc_html__( '2 Columns', 'gauge' ) => 'blog-columns-2', esc_html__( '3 Columns', 'gauge' ) => 'blog-columns-3', esc_html__( '4 Columns', 'gauge' ) => 'blog-columns-4', esc_html__( '5 Columns', 'gauge' ) => 'blog-columns-5', esc_html__( '6 Columns', 'gauge' ) => 'blog-columns-6', esc_html__( 'Masonry', 'gauge' ) => 'blog-masonry' ),
				'type' => 'dropdown',
				),				
				array( 
				'heading' => esc_html__( 'Size', 'gauge' ),
				'description' => esc_html__( 'The size to display the items at.', 'gauge' ),
				'param_name' => 'size',
				'value' => array( esc_html__( 'Small', 'gauge' ) => 'blog-small-size', esc_html__( 'Standard', 'gauge' ) => 'blog-standard-size'  ),
				'type' => 'dropdown',
				),	
				array( 
				'heading' => esc_html__( 'Order By', 'gauge' ),
				'description' => esc_html__( 'The criteria which the items are ordered by.', 'gauge' ),
				'param_name' => 'orderby',
				'value' => array(
					esc_html__( 'Newest', 'gauge' ) => 'newest',
					esc_html__( 'Oldest', 'gauge' ) => 'oldest',
					esc_html__( 'Title (A-Z)', 'gauge' ) => 'title_az',
					esc_html__( 'Title (Z-A)', 'gauge' ) => 'title_za',
					esc_html__( 'Most Comments', 'gauge' ) => 'comment_count',
					esc_html__( 'Most Views', 'gauge' ) => 'views',
					esc_html__( 'Menu Order', 'gauge' ) => 'menu_order',
					esc_html__( 'Random', 'gauge' ) => 'rand',
				),
				'type' => 'dropdown',
				),	
				array( 
				'heading' => esc_html__( 'Date Posted', 'gauge' ),
				'description' => esc_html__( 'The date the items were posted.', 'gauge' ),
				'param_name' => 'date_posted',
				'value' => array(
					esc_html__( 'Any date', 'gauge' ) => 'all',
					esc_html__( 'In the last year', 'gauge' ) => 'year',
					esc_html__( 'In the last month', 'gauge' ) => 'month',
					esc_html__( 'In the last week', 'gauge' ) => 'week',
					esc_html__( 'In the last day', 'gauge' ) => 'day',
				),
				'type' => 'dropdown',
				),
				array( 
				'heading' => esc_html__( 'Date Modified', 'gauge' ),
				'description' => esc_html__( 'The date the items were modified.', 'gauge' ),
				'param_name' => 'date_modified',
				'value' => array(
					esc_html__( 'Any date', 'gauge' ) => 'all',
					esc_html__( 'In the last year', 'gauge' ) => 'year',
					esc_html__( 'In the last month', 'gauge' ) => 'month',
					esc_html__( 'In the last week', 'gauge' ) => 'week',
					esc_html__( 'In the last day', 'gauge' ) => 'day',
				),
				'type' => 'dropdown',
				),	
				array( 
				'heading' => esc_html__( 'Filter', 'gauge' ),
				'description' => esc_html__( 'Add a dropdown filter menu to the page.', 'gauge' ),
				'param_name' => 'filter',
				'value' => array( esc_html__( 'Disabled', 'gauge' ) => 'disabled', esc_html__( 'Enabled', 'gauge' ) => 'enabled' ),
				'type' => 'dropdown',
				),	
				array(
				'heading' => esc_html__( 'Filter Options', 'gauge' ),
				'param_name' => 'filter_cats',
				'value' => array( esc_html__( 'Categories', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				'dependency' => array( 'element' => 'filter', 'value' => 'enabled' ),
				),	
				array(
				'param_name' => 'filter_date',
				'value' => array( esc_html__( 'Date', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				'dependency' => array( 'element' => 'filter', 'value' => 'enabled' ),
				),	
				array(
				'param_name' => 'filter_title',
				'value' => array( esc_html__( 'Title', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				'dependency' => array( 'element' => 'filter', 'value' => 'enabled' ),
				),								
				array(
				'param_name' => 'filter_comment_count',
				'value' => array( esc_html__( 'Comment Count', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				'dependency' => array( 'element' => 'filter', 'value' => 'enabled' ),
				),
				array(
				'param_name' => 'filter_views',
				'value' => array( esc_html__( 'Views', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				'dependency' => array( 'element' => 'filter', 'value' => 'enabled' ),
				),
				array( 
				'param_name' => 'filter_date_posted',
				'value' => array( esc_html__( 'Date Posted', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				'dependency' => array( 'element' => 'filter', 'value' => 'enabled' ),
				),				
				array( 
				'description' => esc_html__( 'Choose what options to display in the dropdown filter menu.', 'gauge' ),
				'param_name' => 'filter_date_modified',
				'value' => array( esc_html__( 'Date Modified', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				'dependency' => array( 'element' => 'filter', 'value' => 'enabled' ),
				),				
				array( 
				'heading' => esc_html__( 'Filter Category', 'gauge' ),
				'description' => esc_html__( 'Enter the slug or ID number of the category you want to filter by, leave blank to display all categories - the sub categories of this category will also be displayed.', 'gauge' ),
				'param_name' => 'filter_cats_id',
				'type' => 'textfield',
				'dependency' => array( 'element' => 'filter', 'value' => 'enabled' ),
				),
				array( 
				'heading' => esc_html__( 'Items Per Page', 'gauge' ),
				'description' => esc_html__( 'The number of items on each page.', 'gauge' ),
				'param_name' => 'per_page',
				'value' => '5',
				'type' => 'textfield',
				),
				array( 
				'heading' => esc_html__( 'Offset', 'gauge' ),
				'description' => esc_html__( 'The number of posts to offset by e.g. set to 3 to exclude the first 3 posts.', 'gauge' ),
				'param_name' => 'offset',
				'value' => '',
				'type' => 'textfield',
				),
				array( 
				'heading' => esc_html__( 'Featured Image', 'gauge' ),
				'description' => esc_html__( 'Display the featured images.', 'gauge' ),
				'param_name' => 'featured_image',
				'value' => array( esc_html__( 'Enabled', 'gauge' ) => 'enabled', esc_html__( 'Disabled', 'gauge' ) => 'disabled' ),
				'type' => 'dropdown',
				),	
				array( 
				'heading' => esc_html__( 'Image Width', 'gauge' ),
				'description' => esc_html__( 'The width of the featured images.', 'gauge' ),
				'param_name' => 'image_width',
				'value' => '75',
				'type' => 'textfield',
				'dependency' => array( 'element' => 'featured_image', 'value' => 'enabled' ),
				),		 
				array( 
				'heading' => esc_html__( 'Image Height', 'gauge' ),
				'description' => esc_html__( 'The height of the featured images.', 'gauge' ),
				'param_name' => 'image_height',
				'value' => '75',
				'type' => 'textfield',
				'dependency' => array( 'element' => 'featured_image', 'value' => 'enabled' ),
				),	
				array( 
				'heading' => esc_html__( 'Hard Crop', 'gauge' ),
				'description' => esc_html__( 'Images are cropped even if it is smaller than the dimensions you want to crop it to.', 'gauge' ),
				'param_name' => 'hard_crop',
				'value' => array( esc_html__( 'Enabled', 'gauge' ) => 'enabled', esc_html__( 'Disabled', 'gauge' ) => 'disabled' ),
				'type' => 'dropdown',
				'dependency' => array( 'element' => 'featured_image', 'value' => 'enabled' ),
				),	
				array( 
				'heading' => esc_html__( 'Image Alignment', 'gauge' ),
				'description' => esc_html__( 'Choose how the image aligns with the content.', 'gauge' ),
				'param_name' => 'image_alignment',
				'value' => array( esc_html__( 'Left Align', 'gauge' ) => 'image-align-left', esc_html__( 'Left Wrap', 'gauge' ) => 'image-wrap-left', esc_html__( 'Right Wrap', 'gauge' ) => 'image-wrap-right', esc_html__( 'Above Content', 'gauge' ) => 'image-above', esc_html__( 'Right Align', 'gauge' ) => 'image-align-right' ),
				'type' => 'dropdown',
				'dependency' => array( 'element' => 'featured_image', 'value' => 'enabled' ),
				),			
				array( 
				'heading' => esc_html__( 'Title Position', 'gauge' ),
				'description' => esc_html__( 'The position of the title.', 'gauge' ),
				'param_name' => 'title_position',
				'value' => array( esc_html__( 'Next To Thumbnail', 'gauge' ) => 'title-next-to-thumbnail', esc_html__( 'Over Thumbnail', 'gauge' ) => 'title-over-thumbnail' ),
				'type' => 'dropdown',
				),
				array( 
				'heading' => esc_html__( 'Content Display', 'gauge' ),
				'description' => esc_html__( 'The amount of content displayed.', 'gauge' ),
				'param_name' => 'content_display',
				'value' => array( esc_html__( 'Excerpt', 'gauge' ) => 'excerpt', esc_html__( 'Full Content', 'gauge' ) => 'full_content' ),
				'type' => 'dropdown',
				),
				array( 
				'heading' => esc_html__( 'Excerpt Length', 'gauge' ),
				'description' => esc_html__( 'The number of characters in excerpts.', 'gauge' ),
				'param_name' => 'excerpt_length',
				'value' => '0',
				'type' => 'textfield',
				'dependency' => array( 'element' => 'content_display', 'value' => 'excerpt' ),
				),	
				array(
				'heading' => esc_html__( 'Post Meta', 'gauge' ),
				'param_name' => 'meta_author',
				'value' => array( esc_html__( 'Author Name', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				),	
				array(
				'param_name' => 'meta_date',
				'value' => array( esc_html__( 'Post Date', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				),				
				array(
				'param_name' => 'meta_views',
				'value' => array( esc_html__( 'Views', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				),		
				array(
				'param_name' => 'meta_comment_count',
				'value' => array( esc_html__( 'Comment Count', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				),
				array( 
				'param_name' => 'meta_cats',
				'value' => array( esc_html__( 'Post Categories', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				),	
				array(
				'description' => esc_html__( 'Select the meta data you want to display.', 'gauge' ),
				'param_name' => 'meta_tags',
				'value' => array( esc_html__( 'Post Tags', 'gauge' ) => '1' ),
				'type' => 'checkbox',
				),	
				array( 
				'heading' => esc_html__( 'Read More Link', 'gauge' ),
				'description' => esc_html__( 'Add a read more link below the content.', 'gauge' ),
				'param_name' => 'read_more_link',
				'value' => array( esc_html__( 'Enabled', 'gauge' ) => 'enabled', esc_html__( 'Disabled', 'gauge' ) => 'disabled' ),
				'type' => 'dropdown',
				),								 
				array( 
				'heading' => esc_html__( 'Pagination', 'gauge' ),
				'description' => esc_html__( 'Add pagination.', 'gauge' ),
				'param_name' => 'page_numbers',
				'value' => array( esc_html__( 'Disabled', 'gauge' ) => 'disabled', esc_html__( 'Enabled', 'gauge' ) => 'enabled' ),
				'type' => 'dropdown',
				),
				array( 
				'heading' => esc_html__( 'See All', 'gauge' ),
				'description' => esc_html__( 'Add a "See All" link.', 'gauge' ),
				'param_name' => 'see_all',
				'value' => array( esc_html__( 'Disabled', 'gauge' ) => 'disabled', esc_html__( 'Enabled', 'gauge' ) => 'enabled' ),
				'type' => 'dropdown',
				),				
				array( 
				'heading' => esc_html__( 'See All Link', 'gauge' ),
				'description' => esc_html__( 'URL for the "See All" link.', 'gauge' ),
				'param_name' => 'see_all_link',
				'type' => 'textfield',
				'dependency' => array( 'element' => 'see_all', 'value' => 'enabled' ),
				),					 			 
				array( 
				'heading' => esc_html__( 'See All Text', 'gauge' ),
				'description' => esc_html__( 'Custom text for the "See All" link.', 'gauge' ),
				'param_name' => 'see_all_text',
				'type' => 'textfield',
				'value' => esc_html__( 'See All Videos', 'gauge' ),
				'dependency' => array( 'element' => 'see_all', 'value' => 'enabled' ),
				),	 			 				 		   			 			 
				array( 
				'heading' => esc_html__( 'Extra Class Names', 'gauge' ),
				'description' => esc_html__( 'If you wish to style this particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'gauge' ),
				'type' => 'textfield',
				'param_name' => 'classes',
				'value' => '',
				),																																								
			 )
		) );
		
				
	}
	
}

add_action( 'init', 'ghostpool_custom_shortcodes' );

?>
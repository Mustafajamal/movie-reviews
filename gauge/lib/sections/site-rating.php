<?php global $gp;

// Get loop hub ID
ghostpool_get_loop_hub_id( get_the_ID() );
$gem_ratings_critics_avg_persent = get_post_meta(get_the_ID(),'gem_ratings_critics_avg_persent');
$gem_ratings_member_avg_persent = get_post_meta(get_the_ID(),'gem_ratings_member_avg_persent');
$review_scores_member_avg = get_post_meta(get_the_ID(),'review_scores_member_avg');
$gem_ratings_critics_avg_persent = $gem_ratings_critics_avg_persent[0];
$gem_ratings_critics_avg_persent_int = $gem_ratings_critics_avg_persent / 100;
$gem_ratings_critics_avg_persent_degree = $gem_ratings_critics_avg_persent_int * 360;
?>

<div class="gp-site-rating-wrapper gp-large-rating gp-rating-<?php echo sanitize_html_class( $gp['review_site_rating_style'] ); ?>"<?php if ( isset( $GLOBALS['ghostpool_site_rich_snippets'] ) && $GLOBALS['ghostpool_site_rich_snippets'] == true ) { ?> itemscope itemtype="http://schema.org/Review"<?php } ?>>

	<?php if ( isset( $GLOBALS['ghostpool_site_rich_snippets'] ) && $GLOBALS['ghostpool_site_rich_snippets'] == true ) { ?> 
		<meta itemprop="datePublished" content="<?php the_time( 'Y-m-d' ); ?>">
		<meta itemprop="dateModified" content="<?php the_modified_date( 'Y-m-d' ); ?>">
		<?php if ( get_post_meta( $GLOBALS['ghostpool_loop_hub_id'], 'hub_synopsis', true ) ) { ?><meta itemprop="description" content="<?php echo strip_tags( esc_attr( get_post_meta( $GLOBALS['ghostpool_loop_hub_id'], 'hub_synopsis', true ) ) ); ?>"><?php } ?>
	<?php } ?>
	
	<div class="gp-rating-inner">
		<div class="gp-score-clip <?php echo sanitize_html_class( $GLOBALS['ghostpool_site_clip_class_1'] ); ?>">
			<div class="gp-score-spinner" style="-webkit-transform: rotate(<?php echo $gem_ratings_critics_avg_persent_degree; ?>deg); transform: rotate(<?php echo $gem_ratings_critics_avg_persent_degree; ?>deg);"></div>
		</div>
		<div class="gp-score-clip <?php echo sanitize_html_class( $GLOBALS['ghostpool_site_clip_class_2'] ); ?>">
			<div class="gp-score-filler"></div>
		</div>		
		<div class="gp-score-inner global">
			<div class="gp-score-table">
				<div class="gp-score-cell">					
					<?php if ( isset( $GLOBALS['ghostpool_site_rich_snippets'] ) && $GLOBALS['ghostpool_site_rich_snippets'] == true ) { ?> 
						<meta itemprop="itemReviewed" content="<?php the_title_attribute( array( 'post' => $GLOBALS['ghostpool_loop_hub_id'] ) ); ?>">
						<meta itemprop="author" content="<?php echo the_author_meta( 'display_name', $post->post_author ); ?>">
					<?php } ?>	
					<span>
					<?php 
					//
						if($gem_ratings_critics_avg_persent == 'NAN%'){
						$gem_ratings_critics_avg_persent = '0%';
						}
					//echo $gem_ratings_critics_avg_persent_degree;
					echo $gem_ratings_critics_avg_persent.'%'; ?>
					</span>
				</div>
			</div>
		</div>
	</div>
	<!--
	<?php //if ( isset( $GLOBALS['ghostpool_site_rating_text'] ) ) { ?><h4 class="gp-rating-text"><?php //echo esc_attr( $GLOBALS['ghostpool_site_rating_text'] ); ?></h4><?php // } ?>  -->
	
	<?php 
	if($gem_ratings_critics_avg_persent >= 0 && $gem_ratings_critics_avg_persent <= 50 ){
		$rating_comments = "Common";
	}
	if($gem_ratings_critics_avg_persent >= 51 && $gem_ratings_critics_avg_persent <= 80 ){
		$rating_comments = "Good";
	}
	if($gem_ratings_critics_avg_persent >= 81 && $gem_ratings_critics_avg_persent <= 95 ){
		$rating_comments = "Rare";
	}
	if($gem_ratings_critics_avg_persent >= 95 && $gem_ratings_critics_avg_persent <= 100 ){
		$rating_comments = "Epic";
	}	
	
	?>
	
	
	<?php if ( isset( $GLOBALS['ghostpool_site_rating_text'] ) ) { ?><h4 class="gp-rating-text"><?php echo $rating_comments; ?></h4><?php } ?>  

</div>
<!-- New code Public score -->
		<div class="rating_parent custom">
			<div class="public_score new">
			
			<?php 
			$gem_ratings_member_avg_persent = $gem_ratings_member_avg_persent[0];
			if(is_nan($gem_ratings_member_avg_persent) == true){ 
					echo '<div class="to_be_rated"><span>To be Rated !</span></div>';
			}else{ ?>					
			<div class="gem_ratings_member_avg"><span><?php echo round($gem_ratings_member_avg_persent,1).'%';?></span></div>
			<?php	
			} 
			?>
			<div class="scores_member_avg">
			<?php 
			$review_scores_member_avg = $review_scores_member_avg[0];
			if($review_scores_member_avg == 'NAN/5'){ 
				echo '<span >Average Rating: 0</span>';
			}else{
				?>
				<span >Average Rating: <?php echo $review_scores_member_avg; ?></span>
				<?php
			}?>
			</div>
			<div class="public_score_content">
				<div class="gem_content_image">
				<img src="<?php echo $panel_image_public; ?>">
				</div>
				<div class="gem_content_text">
					<?php echo $panel_text_public; ?>
				</div>
			</div>
		</div>
		</div>
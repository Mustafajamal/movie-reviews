<?php 

global $gp;

if ( ! is_user_logged_in() ) { ?>

	<div id="login">

		<div id="gp-login-box">		
		
			<a href="#" id="gp-login-close" class="button"></a>
		
			<div class="gp-login-form-wrapper">

				<h3><?php _e( 'Sign In' ,'gauge' ); ?></h3>		

				<form name="loginform" class="gp-login-form" action="<?php echo esc_url( site_url( 'wp-login.php', 'login_post' ) ); ?>" method="post">
		
					<div class="gp-login-content">
	
						<div<?php if ( has_action ( 'oa_social_login' ) ) { ?> class="gp-standard-login"<?php } ?>>
					
							<?php if ( has_action ( 'oa_social_login' ) ) { ?><div class="gp-standard-login-header"><?php _e( 'Login via your site account', 'gauge' ); ?></div><?php } ?>
					
							<p class="username"><input type="text" name="log" class="user_login" value="<?php if ( ! empty( $user_login ) ) { echo esc_html( stripslashes( $user_login ), 1 ); } ?>" size="20" placeholder="<?php _e( 'Username', 'gauge' ); ?>" /></p>
			
							<p class="password"><input type="password" name="pwd" class="user_pass" size="20" placeholder="<?php _e( 'Password', 'gauge' ); ?>" /></p>
		
							<p class="rememberme"><input name="rememberme" class="rememberme" type="checkbox" checked="checked" value="forever" /> <?php _e( 'Remember Me', 'gauge' ); ?></p>
					
							<?php if ( function_exists( 'cptch_display_captcha_custom' ) ) { ?>
								<input type='hidden' name='cntctfrm_contact_action' value='true' />
								<p>
									<?php echo cptch_display_captcha_custom(); ?>
								</p>
							<?php } ?>	
							<?php if ( function_exists( 'cptchpr_display_captcha_custom' ) ) { ?>
								<input type='hidden' name='cntctfrm_contact_action' value='true' />
								<p>
									<?php echo cptchpr_display_captcha_custom(); ?>
								</p>	
							<?php } ?>
										
							<?php if ( has_action ( 'oa_social_login' ) ) { ?><p><a href="#" class="gp-social-login-link"><?php _e( 'Or login via a social network account', 'gauge' ); ?></a></p><?php } ?>
													
						</div>						
					
						<?php if ( has_action ( 'oa_social_login' ) ) { ?>
							<div class="gp-or-divider"><span><?php _e( 'OR', 'gauge' ); ?></span></div>
							<div class="gp-social-login">
								<?php do_action( 'oa_social_login' ); ?>
							</div>
						<?php } ?>	
					
						<span class="gp-login-results"></span>
			
					</div>
			
					<input type="submit" name="wp-submit" class="wp-submit" value="<?php _e( 'Sign In', 'gauge' ); ?>" />

					<span class="gp-login-links">
						<?php if ( get_option( 'users_can_register' ) ) { ?><a href="<?php if ( function_exists( 'bp_is_active' ) ) { echo esc_url( bp_get_signup_page( false ) ); } else { echo '#'; } ?>" class="gp-register-link"><?php _e( 'Register', 'gauge' ); ?></a><?php } ?>
						<a href="#" class="gp-lost-password-link"><?php _e( 'Lost Password', 'gauge' ); ?></a>
					</span>
		
					<input type="hidden" name="action" value="ghostpool_login" />
			
				</form>
			
			</div>
			
					
			<div class="gp-lost-password-form-wrapper">

				<h3><?php _e( 'Lost Password', 'gauge' ); ?></h3>

				<form name="lostpasswordform" class="gp-lost-password-form" action="#" method="post">
			
					<div class="gp-login-content">
			
						<p><?php _e( 'Please enter your username or email address. You will receive a link to create a new password via email.', 'gauge' ); ?></p>	
					
						<p><input type="text" name="user_login" class="user_login" value="" size="20" placeholder="<?php _e( 'Username or Email', 'gauge' ); ?>" /></p>
				
						<span class="gp-login-results"></span>
				
					</div>
		
					<input type="submit" name="wp-submit" class="wp-submit" value="<?php _e( 'Reset Password', 'gauge' ); ?>" />
			
					<span class="gp-login-links">
						<?php if ( get_option( 'users_can_register' ) ) { ?><a href="<?php if ( function_exists( 'bp_is_active' ) ) { echo esc_url( bp_get_signup_page( false ) ); } else { echo '#'; } ?>" class="gp-register-link"><?php _e( 'Register', 'gauge' ); ?></a><?php } ?>
						<a href="#" class="gp-login-link"><?php _e( 'Sign In', 'gauge' ); ?></a>
					</span>
		
					<input type="hidden" name="action" value="ghostpool_lost_password" />
					<input type="hidden" name="gp_pwd_nonce" value="<?php echo wp_create_nonce("gp_pwd_nonce"); ?>" />
							
				</form>

			</div>

			<?php if ( has_action ( 'oa_social_login' ) ) { ?>
		
				<div class="gp-social-login-form-wrapper">

					<h3><?php _e( 'Sign In', 'gauge' ); ?></h3>

					<form class="gp-social-login-form" action="#" method="post">	
			
						<div class="gp-login-content">
							<?php do_action( 'oa_social_login' ); ?>
						</div>
							
						<span class="gp-login-links">
							<?php if ( get_option( 'users_can_register' ) ) { ?><a href="<?php if ( function_exists( 'bp_is_active' ) ) { echo esc_url( bp_get_signup_page( false ) ); } else { echo '#'; } ?>" class="gp-register-link"><?php _e( 'Register', 'gauge' ); ?></a><?php } ?>
							<a href="#" class="gp-lost-password-link"><?php _e( 'Lost Password', 'gauge' ); ?></a>
						</span>
						
					</form>

				</div>
			
			<?php } ?>
			
						
			<?php if ( get_option( 'users_can_register' ) && ! function_exists( 'bp_is_active' ) ) { ?>
			
				<div class="gp-register-form-wrapper">

					<h3><?php _e( 'Sign Up' ,'gauge' ); ?></h3>		

					<form name="registerform" class="gp-register-form" action="<?php echo esc_url( site_url( 'wp-login.php?action=register', 'login_post' ) ); ?>" method="post">
			
						<div class="gp-login-content">
  
							<p class="user_login"><input type="text" name="user_login" class="user_login" value="<?php if ( ! empty( $user_login ) ) { echo esc_html( stripslashes( $user_login ), 1 ); } ?>" size="20" placeholder="<?php _e( 'Username', 'gauge' ); ?>" /></p>
			
							<p class="user_email"><input type="email" name="user_email" class="user_email" size="25" placeholder="<?php _e( 'Email', 'gauge' ); ?>" /></p>
							
							<p class="user_confirm_pass"><span class="gp-password-icon"></span><input type="password" name="user_confirm_pass" class="user_confirm_pass" size="25" placeholder="<?php esc_attr_e( 'Password', 'gauge' ); ?>" /></p>
							
							<p class="user_pass"><span class="gp-password-icon"></span><input type="password" name="user_pass" class="user_pass" size="25" placeholder="<?php esc_attr_e( 'Confirm Password', 'gauge' ); ?>" /></p>

							<?php if ( function_exists( 'cptch_display_captcha_custom' ) ) { ?>
								<input type='hidden' name='cntctfrm_contact_action' value='true' />
								<p>
									<?php echo cptch_display_captcha_custom(); ?>
								</p>
							<?php } ?>	
							<?php if ( function_exists( 'cptchpr_display_captcha_custom' ) ) { ?>
								<input type='hidden' name='cntctfrm_contact_action' value='true' />
								<p>
									<?php echo cptchpr_display_captcha_custom(); ?>
								</p>	
							<?php } ?>					

							<span class="gp-login-results"></span>
				
						</div>
									
						<input type="submit" name="wp-submit" class="wp-submit" value="<?php _e( 'Sign Up', 'gauge' ); ?>" />

						<span class="gp-login-links">
							<a href="#" class="gp-login-link"><?php _e( 'Sign In', 'gauge' ); ?></a>
						</span>
					
						<input type="hidden" name="action" value="ghostpool_register" />
				
					</form>
				
				</div>
			
			<?php } ?>
								
		</div>
	
		<script>  	

		jQuery( document ).ready(function( $ ) {							

			'use strict';

			$( '.gp-login-form' ).submit(function() {
				var loginform = $( this ); 
				loginform.find( '.gp-login-results' ).html( '<span class="gp-verify-form"><?php _e( "Verifying...", "gauge" ); ?></span>' ).fadeIn();
				var input_data = loginform.serialize();
				$.ajax({
					type: "POST",
					<?php if ( is_ssl() ) { ?>
						url:  "<?php echo esc_url( 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ); ?>",
					<?php } else { ?>
						url:  "<?php echo esc_url( 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ); ?>",
					<?php } ?>	
					data: input_data,
					success: function(msg) {
						loginform.find( '.gp-verify-form' ).remove();
						$( '<span>' ).html(msg).appendTo( loginform.find( '.gp-login-results' ) ).hide().fadeIn( 'slow' );
					},
					error: function(xhr, status, error) {
						loginform.find( '.gp-verify-form' ).remove();
						$( '<span>' ).html( xhr.responseText ).appendTo( loginform.find( '.gp-login-results' ) ).hide().fadeIn( 'slow' );
					}
				});
				return false;
			});
		
			$( '.gp-lost-password-form' ).submit(function() {
				var lostpasswordform = $( this ); 
				lostpasswordform.find( '.gp-login-results' ).html( '<span class="gp-verify-form"><?php _e( "Verifying...", "gauge" ); ?></span>' ).fadeIn();
				var input_data = lostpasswordform.serialize();
				$.ajax({
					type: "POST",
					<?php if ( is_ssl() ) { ?>
						url:  "<?php echo esc_url( 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ); ?>",
					<?php } else { ?>
						url:  "<?php echo esc_url( 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ); ?>",
					<?php } ?>
					data: input_data,
					success: function(msg) {
						lostpasswordform.find( '.gp-verify-form' ).remove();
						$( '<span>' ).html(msg).appendTo( lostpasswordform.find( '.gp-login-results' ) ).hide().fadeIn( 'slow' );
					}
				});
				return false;
			});
			
			<?php if ( get_option( 'users_can_register' ) && ! function_exists( 'bp_is_active' ) ) { ?>
				$( '.gp-register-form' ).submit(function() {
					var registerform = $( this ); 
					registerform.find( '.gp-login-results' ).html( '<span class="gp-verify-form"><?php _e( "Verifying...", "gauge" ); ?></span>' ).fadeIn();
					var input_data = registerform.serialize();
					$.ajax({
						type: "POST",
						<?php if ( is_ssl() ) { ?>
							url:  "<?php echo esc_url( 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ); ?>",
						<?php } else { ?>
							url:  "<?php echo esc_url( 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ); ?>",
						<?php } ?>	
						data: input_data,
						success: function(msg) {
							registerform.find( '.gp-verify-form' ).remove();
							$( '<span>' ).html(msg).appendTo( registerform.find( '.gp-login-results' ) ).hide().fadeIn( 'slow' );
							if ( ! registerform.find( '.gp-login-results > span > span' ).hasClass( 'error' ) ) {						
								registerform.find( 'input[type="text"]' ).val( '' );
								registerform.find( 'p' ).hide();
								registerform.find( '.wp-submit' ).hide();
							}  
						}
					});
					return false;
				});
			<?php } ?>
		
			// Close modal window when clicking close button
			$( '#gp-login-close' ).click(function() {		
				$( '#login' ).hide();
				$( '.gp-login-results > span' ).remove();
			});	
			
			// Open login modal window when clicking links		
			$( '.gp-login-link, a[href="#login"]' ).click(function() {
				$( '#login' ).show();
				$( '.gp-login-form-wrapper' ).show();
				$( '.gp-register-form-wrapper, .gp-lost-password-form-wrapper, .gp-social-login-form-wrapper' ).hide();
				$( '.gp-login-results > span' ).remove();
			});		

			// Open login modal window directly from URL
			if ( /#login/.test( window.location.href ) ) {
				$( '#login' ).show();
				$( '.gp-login-form-wrapper' ).show();
				$( '.gp-register-form-wrapper, .gp-lost-password-form-wrapper, .gp-social-login-form-wrapper' ).hide();
				$( '.gp-login-results > span' ).remove();
			}
						
			// Open lost password modal window when clicking link								
			$( '.gp-lost-password-link' ).click(function() {
				$( '.gp-lost-password-form-wrapper' ).show();
				$( '.gp-register-form-wrapper, .gp-login-form-wrapper, .gp-social-login-form-wrapper' ).hide();
				$( '.gp-login-results > span' ).remove();
			});		

			// Open lost password modal window directly from URL
			if ( /#lost-password/.test( window.location.href ) ) {
				$( '#login' ).show();
				$( '.gp-lost-password-form-wrapper' ).show();
				$( '.gp-register-form-wrapper, .gp-login-form-wrapper, .gp-social-login-form-wrapper' ).hide();
				$( '.gp-login-results > span' ).remove();
			}
			
			<?php if ( get_option( 'users_can_register' ) && ! function_exists( 'bp_is_active' ) ) { ?>

				// Open registration modal window when clicking links		
				$( '.gp-register-link, a[href="#register"]' ).click(function() {
					$( '#login' ).show();
					$( '.gp-register-form-wrapper, .gp-register-form .login-form > p, .gp-register-form .wp-submit' ).show();
					$( '.gp-register-form .login-form p > input[type="text"]' ).val( '' );
					$( '.gp-login-form-wrapper, .gp-lost-password-form-wrapper, .gp-social-login-form-wrapper' ).hide();
					$( '.gp-login-results > span' ).remove();
				});

				// Open registration modal window directly from URL
				if ( /#register/.test( window.location.href ) ) {
					$( '#login' ).show();
					$( '.gp-register-form-wrapper, .gp-register-form .login-form > p, .gp-register-form .wp-submit' ).show();
					$( '.gp-register-form .login-form p > input[type="text"]' ).val( '' );
					$( '.gp-login-form-wrapper, .gp-lost-password-form-wrapper, .gp-social-login-form-wrapper' ).hide();
					$( '.gp-login-results > span' ).remove();
				}
						
			<?php } ?>
										
			<?php if ( has_action ( 'oa_social_login' ) ) { ?>
				$( '.gp-social-login-link' ).click(function() {
					$( '.gp-social-login-form-wrapper' ).show();
					$( '.gp-login-form-wrapper, .gp-register-form-wrapper, .gp-lost-password-form-wrapper' ).hide();
					$( '.gp-login-results > span' ).remove();
				});		
			<?php } ?>

		});
		
		</script>
				
	</div>
	
<?php } ?>
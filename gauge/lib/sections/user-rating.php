<?php global $gp;

// Get loop hub ID
ghostpool_get_loop_hub_id( get_the_ID() );
$product_name = get_the_ID();
	$params = array('posts_per_page' => -1,
					'post_type' => 'urp_review',
					'post_status' => 'publish',
					'meta_query' => array(
				        array(
				            'key'       => 'EWD_URP_Product_Name',
				            'value'     => $product_name,
				        ),
				    )
			);
			
$Review_Query = new WP_Query($params);
while ( $Review_Query->have_posts() ): $Review_Query->the_post(); 
global $post;
		$Review = get_post();
		$Product_Name = get_post_meta($Review->ID, 'EWD_URP_Product_Name', true);
		$Review_Author = get_post_meta($Review->ID, 'EWD_URP_Post_Author', true);
		$user_info = get_userdata($Review_Author);
		$user_role = $user_info->roles[0];
		$Review_gem_rating= get_post_meta($Review->ID, "EWD_URP_Gem rating", true);
		$Review_score = get_post_meta($Review->ID, "EWD_URP_Score", true);
		if($user_role == 'member'){
		$Review_gem_ratings_member[] = $Review_gem_rating;
		$Review_scores_member[] = $Review_score;
		}else if($user_role == 'critics'){
		$Review_gem_ratings_critics[] = $Review_gem_rating;
		$Review_scores_critics[] = $Review_score;
		}
	endwhile;
wp_reset_postdata();

		$Review_count_member = count($Review_scores_member);
		$Review_scores_member_sum = array_sum($Review_scores_member);
		$Review_scores_member_avg = $Review_scores_member_sum/$Review_count_member;
		
		$gem_ratings_member_count = count($Review_gem_ratings_member);
		$gem_ratings_member_scores_sum = array_sum($Review_gem_ratings_member);
		$gem_ratings_member_avg_persent = $gem_ratings_member_scores_sum/$gem_ratings_member_count * 100;
		
		
		$Review_count_critics = count($Review_scores_critics);
		$Review_scores_critics_sum = array_sum($Review_scores_critics);
		$Review_scores_critics_avg = $Review_scores_critics_sum/$Review_count_critics;
		
		$gem_ratings_critics_count = count($Review_gem_ratings_critics);
		$gem_ratings_critics_scores_sum = array_sum($Review_gem_ratings_critics);
		$gem_ratings_critics_avg_persent = $gem_ratings_critics_scores_sum/$gem_ratings_critics_count * 100;
		
?>
	
	<div class="rating_parent">
		
		<div class="gem_meter"> 
			<h4 class="gem_heading">Gem-O-Meter</h4>
			<div class="gem_ratings_critics_avg">
				<span><?php echo round($gem_ratings_critics_avg_persent,1).'%';?></span>
			</div>
			<div class="scores_critics_avg">
				<span class="gem_text">Average Rating: <?php echo round($Review_scores_critics_avg,1).'/5'; ?></span>
			</div>
			<div class="total_critic_reviews">
				<span class="gem_text">Total Critic Reviews: <?php echo $Review_count_critics; ?></span>
			</div>
		</div>

		<div class="public_score">
			<h4 class="gem_heading">Public Score:</h4>
			<div class="gem_ratings_member_avg">
				<span><?php echo round($gem_ratings_member_avg_persent,1).'%';?></span>
			</div>
			<div class="scores_member_avg">
				<span >Average Rating: <?php echo round($Review_scores_member_avg,1).'/5'; ?></span>
			</div>
		</div>
	</div>



</div>
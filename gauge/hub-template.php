<?php
/*
Template Name: Hub
*/

get_header(); global $gp; ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>		
		
	<?php ghostpool_page_header( get_the_ID() ); ?>
		
	<div id="gp-content-wrapper"<?php if ( $GLOBALS['ghostpool_layout'] != 'gp-fullwidth' ) { ?> class="gp-container"<?php } ?>>

		<div id="gp-content">

			<article <?php post_class(); ?> itemscope itemtype="http://schema.org/Article">
						
				<?php echo ghostpool_meta_data( get_the_ID() ); ?>
				
				<div class="gp-entry-content">
					
					<?php get_template_part( 'lib/sections/hub-details' ); ?>
				
					<?php if ( ( $GLOBALS['ghostpool_layout'] == 'gp-no-sidebar' OR $GLOBALS['ghostpool_layout'] == 'gp-fullwidth' ) && $GLOBALS['ghostpool_user_rating_box'] == 'enabled' ) { ?>
						<?php get_template_part( 'lib/sections/user-rating-box' ); ?>
					<?php } ?>
					<?php $post_id = get_the_ID();?>
					<?php echo do_shortcode('[ultimate-reviews] ');?>	
					<?php echo do_shortcode('[submit-review] ');?>	
					<?php the_content(); ?>
			
				</div>

			</article>	

		</div>

		<?php get_sidebar(); ?>

	</div>
		
<?php endwhile; endif; ?>	
			
<?php get_footer(); ?>
<?php include_once( ghostpool_inc . 'login-settings.php' ); ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<?php global $gp; ?>
<?php if ( $gp['responsive'] == 'gp-responsive' ) { ?>
	<meta name="viewport" content="width=device-width, initial-scale=1">
<?php } else { ?>
	<meta name="viewport" content="width=1200">	
<?php } ?>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">

<?php if ( ! is_page_template( 'blank-page-template.php' ) ) { ?>

	<div id="gp-site-wrapper">
				
		<?php if ( has_nav_menu( 'header-nav' ) ) { ?>		
			<nav id="gp-mobile-nav" itemscope itemtype="http://schema.org/SiteNavigationElement">
				<div id="gp-mobile-nav-close-button"></div>
				<?php get_search_form(); ?>
				<?php wp_nav_menu( array( 'theme_location' => 'header-nav', 'sort_column' => 'menu_order', 'container' => '', 'items_wrap' => '<ul class="menu">%3$s</ul>', 'fallback_cb' => 'null', 'walker' => new ghostpool_custom_menu ) ); ?>
			</nav>
			<div id="gp-mobile-nav-bg"></div>
		<?php } ?>
			
		<div id="gp-page-wrapper">

			<?php if ( $gp['top_header'] != 'gp-main-header' ) { ?>
	
				<header id="gp-top-header" itemscope itemtype="http://schema.org/WPHeader">
	
					<div class="gp-container">

						<nav id="gp-left-top-nav" class="gp-nav" itemscope itemtype="http://schema.org/SiteNavigationElement">	
							<?php wp_nav_menu( array( 'theme_location' => 'top-nav', 'sort_column' => 'menu_order', 'container' => 'ul', 'fallback_cb' => 'null', 'walker' => new ghostpool_custom_menu ) ); ?>
						</nav>
					
						<div id="gp-right-top-nav" class="gp-nav" itemscope itemtype="http://schema.org/SiteNavigationElement">
							<?php wp_nav_menu( array( 'theme_location' => 'right-top-nav', 'sort_column' => 'menu_order', 'container' => 'ul', 'fallback_cb' => 'null', 'walker' => new ghostpool_custom_menu ) ); ?>
						</div>
																
						<?php if ( function_exists( 'is_woocommerce' ) && $gp['cart_button'] != 'gp-cart-disabled' ) { echo ghostpool_dropdown_cart(); } ?>
						
						<?php get_template_part( 'lib/sections/social', 'icons' ); ?>
					
					</div>
		
				</header>
	
			<?php } ?>

			<header id="gp-main-header" itemscope itemtype="http://schema.org/WPHeader">

				<div class="gp-container">
	
					<div id="gp-logo">
						<?php if ( $gp['logo']['url'] ) { ?>
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php bloginfo( 'name' ); ?>">
								<img src="<?php echo esc_url( $gp['logo']['url'] ); ?>" alt="<?php bloginfo( 'name' ); ?>" width="<?php echo absint( $gp['logo_dimensions']['width'] ); ?>" height="<?php echo absint( $gp['logo_dimensions']['height'] ); ?>" />
							</a>
						<?php } ?>
					</div>

					<?php if ( has_nav_menu( 'header-nav' ) ) { ?>
						<nav id="gp-main-nav" class="gp-nav" itemscope itemtype="http://schema.org/SiteNavigationElement">
							<?php wp_nav_menu( array( 'theme_location' => 'header-nav', 'sort_column' => 'menu_order', 'container' => 'ul', 'fallback_cb' => 'null', 'walker' => new ghostpool_custom_menu ) ); ?>
							<a id="gp-mobile-nav-button"></a>
						</nav>
					<?php } ?>

					<?php if ( $gp['search'] == 'enabled' ) { ?>
						<?php get_search_form(); ?>
					<?php } ?>

				</div>
	
			</header>

			<div id="gp-fixed-header-padding"></div>

<?php } ?>
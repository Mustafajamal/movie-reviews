<?php global $gp;

// Get loop hub ID
ghostpool_get_loop_hub_id( get_the_ID() );

?>

<div class="gp-site-rating-wrapper gp-large-rating gp-rating-<?php echo sanitize_html_class( $gp['review_site_rating_style'] ); ?>"<?php if ( isset( $GLOBALS['ghostpool_site_rich_snippets'] ) && $GLOBALS['ghostpool_site_rich_snippets'] == true ) { ?> itemscope itemtype="http://schema.org/Review"<?php } ?>>

	<?php if ( isset( $GLOBALS['ghostpool_site_rich_snippets'] ) && $GLOBALS['ghostpool_site_rich_snippets'] == true ) { ?> 
		<meta itemprop="datePublished" content="<?php the_time( 'Y-m-d' ); ?>">
		<meta itemprop="dateModified" content="<?php the_modified_date( 'Y-m-d' ); ?>">
		<?php if ( get_post_meta( $GLOBALS['ghostpool_loop_hub_id'], 'hub_synopsis', true ) ) { ?><meta itemprop="description" content="<?php echo strip_tags( esc_attr( get_post_meta( $GLOBALS['ghostpool_loop_hub_id'], 'hub_synopsis', true ) ) ); ?>"><?php } ?>
	<?php } ?>
	
	<div class="gp-rating-inner">
		<div class="gp-score-clip <?php echo sanitize_html_class( $GLOBALS['ghostpool_site_clip_class_1'] ); ?>">
			<div class="gp-score-spinner" style="-webkit-transform: rotate(<?php echo esc_attr( $GLOBALS['ghostpool_site_deg'] ); ?>deg); transform: rotate(<?php echo esc_attr( $GLOBALS['ghostpool_site_deg'] ); ?>deg);"></div>
		</div>
		<div class="gp-score-clip <?php echo sanitize_html_class( $GLOBALS['ghostpool_site_clip_class_2'] ); ?>">
			<div class="gp-score-filler"></div>
		</div>		
		<div class="gp-score-inner">
			<div class="gp-score-table">
				<div class="gp-score-cell">					
					<?php if ( isset( $GLOBALS['ghostpool_site_rich_snippets'] ) && $GLOBALS['ghostpool_site_rich_snippets'] == true ) { ?> 
						<meta itemprop="itemReviewed" content="<?php the_title_attribute( array( 'post' => $GLOBALS['ghostpool_loop_hub_id'] ) ); ?>">
						<meta itemprop="author" content="<?php echo the_author_meta( 'display_name', $post->post_author ); ?>">
					<?php } ?>	
					<span<?php if ( isset( $GLOBALS['ghostpool_site_rich_snippets'] ) && $GLOBALS['ghostpool_site_rich_snippets'] == true ) { ?> itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating"<?php } ?>>
						<?php if ( isset( $GLOBALS['ghostpool_site_rich_snippets'] ) && $GLOBALS['ghostpool_site_rich_snippets'] == true ) { ?> 
							<meta itemprop="worstRating" content="1">
							<meta itemprop="bestRating" content="<?php echo floatval( $gp['review_rating_number'] ); ?>">
						<?php } ?>
						<span<?php if ( isset( $GLOBALS['ghostpool_site_rich_snippets'] ) && $GLOBALS['ghostpool_site_rich_snippets'] == true ) { ?> itemprop="ratingValue"<?php } ?>><?php echo floatval( $GLOBALS['ghostpool_total_score'] ); ?></span>
					</span>
				</div>
			</div>
		</div>						
	</div>
	<?php if ( isset( $GLOBALS['ghostpool_site_rating_text'] ) ) { ?><h4 class="gp-rating-text"><?php echo esc_attr( $GLOBALS['ghostpool_site_rating_text'] ); ?></h4><?php } ?>
		
</div>
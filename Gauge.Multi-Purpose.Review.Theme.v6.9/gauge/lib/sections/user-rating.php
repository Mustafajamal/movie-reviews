<?php global $gp;

// Get loop hub ID
ghostpool_get_loop_hub_id( get_the_ID() );

?>

<?php if ( $GLOBALS['ghostpool_site_rating_enabled'] == true && isset( $GLOBALS['ghostpool_review_loop'] ) ) { ?>

	<div class="gp-user-rating-wrapper gp-small-rating">
		<div class="gp-average-rating-data">
			<div class="gp-average-rating">
				<?php echo floatval( $GLOBALS['ghostpool_user_rating'] ); ?>
			</div>
			<div class="gp-user-average-text">
				<?php esc_html_e( 'User Avg', 'gauge' ); ?>
			</div>						
		</div>
	</div>
						
<?php } else { ?>

	<div class="gp-user-rating-wrapper gp-large-rating gp-rating-<?php echo sanitize_html_class( $gp['review_user_rating_style'] ); ?>"<?php if ( isset( $GLOBALS['ghostpool_user_rich_snippets'] ) && $GLOBALS['ghostpool_user_rich_snippets'] == true ) { ?> itemscope itemtype="http://schema.org/Product"<?php } ?>>
	
		<?php if ( isset( $GLOBALS['ghostpool_user_rich_snippets'] ) && $GLOBALS['ghostpool_user_rich_snippets'] == true ) { ?>
			<meta itemprop="name" content="<?php the_title_attribute( array( 'post' => $GLOBALS['ghostpool_loop_hub_id'] ) ); ?>">
		<?php } ?>

		<div<?php if ( isset( $GLOBALS['ghostpool_user_rich_snippets'] ) && $GLOBALS['ghostpool_user_rich_snippets'] == true ) { ?> itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating"<?php } ?>>

			<div class="gp-rating-inner">
				<div class="gp-score-clip <?php echo sanitize_html_class( $GLOBALS['ghostpool_user_clip_class_1'] ); ?>">
					<div class="gp-score-spinner" style="-webkit-transform: rotate(<?php echo esc_attr( $GLOBALS['ghostpool_user_deg'] ); ?>deg); transform: rotate(<?php echo esc_attr( $GLOBALS['ghostpool_user_deg'] ); ?>deg);"></div>
				</div>
				<div class="gp-score-clip <?php echo sanitize_html_class( $GLOBALS['ghostpool_user_clip_class_2'] ); ?>">
					<div class="gp-score-filler"></div>
				</div>		
				<div class="gp-score-inner">
					<div class="gp-score-table">
						<div class="gp-score-cell">
							<span<?php if ( isset( $GLOBALS['ghostpool_user_rich_snippets'] ) && $GLOBALS['ghostpool_user_rich_snippets'] == true ) { ?> itemprop="ratingValue"<?php } ?>><?php echo floatval( $GLOBALS['ghostpool_user_rating'] ); ?></span>
							<?php if ( isset( $GLOBALS['ghostpool_user_rich_snippets'] ) && $GLOBALS['ghostpool_user_rich_snippets'] == true ) { ?>
								<meta itemprop="worstRating" content="1">
								<meta itemprop="bestRating" content="<?php echo floatval( $gp['review_rating_number'] ); ?>">	
								<meta itemprop="ratingCount" content="<?php echo floatval( get_post_meta( $GLOBALS['ghostpool_loop_hub_id'], '_gp_user_votes', true ) ); ?>">
							<?php } ?>	
						</div>
					</div>
				</div>						
			</div>
			<h4 class="gp-rating-text"><?php esc_html_e( 'User Avg', 'gauge' ); ?></h4>
	
		</div>	

	</div>

<?php } ?>
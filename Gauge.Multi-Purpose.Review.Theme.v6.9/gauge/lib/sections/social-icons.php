<?php global $gp; ?>

<?php if ( $gp['rss_button'] == 'enabled' OR ( isset( $gp['rss'] ) && $gp['rss'] != '' ) OR ( isset( $gp['twitter'] ) && $gp['twitter'] != '' ) OR ( isset( $gp['facebook'] ) && $gp['facebook'] != '' ) OR ( isset( $gp['youtube'] ) && $gp['youtube'] != '' ) OR ( isset( $gp['googleplus'] ) && $gp['googleplus'] != '' ) OR ( isset( $gp['linkedin'] ) && $gp['linkedin'] != '' ) OR ( isset( $gp['flickr'] ) && $gp['flickr'] != '' ) OR ( isset( $gp['pinterest'] ) && $gp['pinterest'] != '' ) OR ( isset( $gp['additional_social_icons'] ) && $gp['additional_social_icons'] != '' ) ) { ?>

	<div class="gp-social-icons">
	
		<?php if ( $gp['rss_button'] == 'enabled' ) { ?><a href="<?php if ( isset( $gp['rss'] ) && $gp['rss'] != '' ) { echo esc_url( $gp['rss'] ); } else { bloginfo( 'rss2_url' ); } ?>" class="fa fa-rss" title="<?php esc_html_e( 'RSS Feed', 'gauge' ); ?>" rel="me" target="_blank"></a><?php } ?>

		<?php if ( isset( $gp['twitter'] ) && $gp['twitter'] != '' ) { ?><a href="<?php echo esc_url( $gp['twitter'] ); ?>" class="fa fa-twitter" title="<?php esc_html_e( 'Twitter', 'gauge' ); ?>" rel="me" target="_blank"></a><?php } ?>

		<?php if ( isset( $gp['facebook'] ) && $gp['facebook'] != '' ) { ?><a href="<?php echo esc_url( $gp['facebook'] ); ?>" class="fa fa-facebook" title="<?php esc_html_e( 'Facebook', 'gauge' ); ?>" rel="me" target="_blank"></a><?php } ?>

		<?php if ( isset( $gp['youtube'] ) && $gp['youtube'] != '' ) { ?><a href="<?php echo esc_url( $gp['youtube'] ); ?>" class="fa fa-youtube" title="<?php esc_html_e( 'YouTube', 'gauge' ); ?>" rel="me" target="_blank"></a><?php } ?>

		<?php if ( isset( $gp['linkedin'] ) && $gp['linkedin'] != '' ) { ?><a href="<?php echo esc_url( $gp['linkedin'] ); ?>" class="fa fa-linkedin" title="<?php esc_html_e( 'LinkedIn', 'gauge' ); ?>" rel="me" target="_blank"></a><?php } ?>

		<?php if ( isset( $gp['googleplus'] ) && $gp['googleplus'] != '' ) { ?><a href="<?php echo esc_url( $gp['googleplus'] ); ?>" class="fa fa-google-plus" title="<?php esc_html_e( 'Google+', 'gauge' ); ?>" rel="me" target="_blank"></a><?php } ?>
					
		<?php if ( isset( $gp['flickr'] ) && $gp['flickr'] != '' ) { ?><a href="<?php echo esc_url( $gp['flickr'] ); ?>" class="fa fa-flickr" title="<?php esc_html_e( 'Flickr', 'gauge' ); ?>" rel="me" target="_blank"></a><?php } ?>

		<?php if ( isset( $gp['pinterest'] ) && $gp['pinterest'] != '' ) { ?><a href="<?php echo esc_url( $gp['pinterest'] ); ?>" class="fa fa-pinterest" title="<?php esc_html_e( 'Pinterest', 'gauge' ); ?>" rel="me" target="_blank"></a><?php } ?>
			
		<?php if ( isset( $gp['additional_social_icons'] ) ) { echo do_shortcode( $gp['additional_social_icons'] ); } ?>

	</div>

<?php } ?>
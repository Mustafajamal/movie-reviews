<?php

$output = $el_class = $image = $img_size = $img_link = $img_link_target = $img_link_large = $title = $css = $align = $animation = $scroll_animation = $delay = $style = '';

extract(shortcode_atts(array(
    'title' => '',
    'image' => $image,
    'img_size'  => 'thumbnail',
    'img_link_large' => false,
    'img_link' => '',
	'link' => '',
    'img_link_target' => '_self',
    //'alignment' => 'left',
    'el_class' => '',
    //'css_animation' => '',
    'style' => '',
    'border_color' => '',
    'css' => '',
  	'align' => '',
    'animation' => 'no-animation',
    'scroll_animation' => 'disabled',
    'delay' => '0.1',
    'styling' => '',    
), $atts));

$style = ($style!='') ? $style : '';
$border_color = ($border_color!='') ? ' vc_box_border_' . $border_color : '';
		
$img_id = preg_replace('/[^\d]/', '', $image);
$img = wpb_getImageBySize(array( 'attach_id' => $img_id, 'thumb_size' => $img_size, 'class' => $style.$border_color ));
if ( $img == NULL ) $img['thumbnail'] = '<img class="' . $style . $border_color . '" src="' . vc_asset_url( 'vc/no_image.png' ) . '" />'; //' <small>'.esc_html__('This is image placeholder, edit your page to replace it.', 'js_composer').'</small>';

$el_class = $this->getExtraClass($el_class);

$a_class = '';
if ( $el_class != '' ) {
    $tmp_class = explode(" ", strtolower($el_class));
    $tmp_class = str_replace(".", "", $tmp_class);
    if ( in_array("prettyphoto", $tmp_class) ) {
        wp_enqueue_script( 'prettyphoto' );
        wp_enqueue_style( 'prettyphoto' );
        $a_class = ' class="prettyphoto"';
        $el_class = str_ireplace(" prettyphoto", "", $el_class);
    }
}

// BEGIN MODIFIED

$name = 'image_with_animation_' . $img_id;

// Animation

if( $animation != 'no-animation' ) {		
	if( $scroll_animation == 'enable' OR $delay != '0' ) {
		$parse_animation = ' invisible';
		$parse_scroll_animation = $animation;
	} elseif( $scroll_animation == 'disabled' OR $delay == '0' ) {
		$parse_animation = ' ' . $animation;
		$parse_scroll_animation = '';
	}	
} else {
	$parse_animation = '';
	$parse_scroll_animation = '';
}

// END MODIFIED

$link_to = '';
if ($img_link_large==true) {
    $link_to = wp_get_attachment_image_src( $img_id, 'large');
    $link_to = $link_to[0];
} else if ( strlen($link) > 0 ) {
	$link_to = $link;
} else if ( ! empty( $img_link ) ) {
    $link_to = $img_link;
	if ( ! preg_match( '/^(https?\:\/\/|\/\/)/', $link_to ) ) $link_to = 'http://' . $link_to;
}
//to disable relative links uncomment this..


$img_output = ($style=='vc_box_shadow_3d') ? '<span class="vc_box_shadow_3d_wrap">' . $img['thumbnail'] . '</span>' : $img['thumbnail'];
$image_string = ! empty( $link_to ) ? '<a' . $a_class . ' href="' . $link_to . '"' . ' target="' . $img_link_target . '"'. '>' . $img_output . '</a>' : $img_output;
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'wpb_single_image wpb_content_element' . $el_class . vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );
//MODIFIED$css_class .= $this->getCSSAnimation($css_animation);

//MODIFIED$css_class .= ' vc_align_'.$alignment;

// Alignment	
$align = ' ' . $align; // MODIFIED
		
$output .= "\n\t".'<div class="'.$css_class.$align.$parse_animation.'" id="'.$name.'" style="'.$styling.'">';
$output .= "\n\t\t".'<div class="wpb_wrapper">';
$output .= "\n\t\t\t".wpb_widget_title(array('title' => $title, 'extraclass' => 'wpb_singleimage_heading'));
$output .= "\n\t\t\t".$image_string;
$output .= "\n\t\t".'</div> '.$this->endBlockComment('.wpb_wrapper');
$output .= "\n\t".'</div> '.$this->endBlockComment('.wpb_single_image');


// BEGIN MODIFIED

if ( $animation != 'no-animation' ) {

	if ( $scroll_animation == 'disabled' && $delay != '0' ) {	
		
		$output .= '<script>
		jQuery( document ).ready( function( $ ) {
			jQuery( "#' . $name . '" ).each( function() {
				jQuery( this ).delay( ' . ( $delay * 1000 ) . ' ).queue( function( next ) {
					jQuery( this ).addClass( "' . $parse_scroll_animation . '" );
					next();
				});	
			});
		});</script>';

	} elseif ( $scroll_animation == 'enable' ) {	

		$output .= '
		<script>
		function isElementInViewport( elem ) {
			var elem = jQuery( elem );
			var scrollElem = ( ( navigator.userAgent.toLowerCase().indexOf( "webkit" ) != -1 ) ? "body" : "html" );
			var viewportTop = jQuery( scrollElem ).scrollTop();
			var viewportBottom = viewportTop + jQuery( window ).height();
			var elemTop = Math.round( elem.offset().top );
			var elemBottom = elemTop + elem.height();
			return ( ( elemTop < viewportBottom ) && ( elemBottom > viewportTop ) );
		}

		function checkAnimation_' . $name . '() {
			var elem = jQuery( "#' . $name . '" );
			if ( elem.hasClass( "' . $parse_scroll_animation . '" ) ) return;
			if ( isElementInViewport( elem ) ) {
				elem.delay( ' . ( $delay * 1000 ) . ' ).queue(function( next ) {
					elem.addClass( "' . $parse_scroll_animation . '" );
					next();
				});	
			}
		}

		jQuery( window ).scroll( function() {
			checkAnimation_' . $name . '();
		});	
		</script>';
	
	}	

}

// END MODIFIED	
		
echo $output;
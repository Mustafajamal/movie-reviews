<?php

if ( ! function_exists( 'ghostpool_button' ) ) {

	function ghostpool_button( $atts, $content = null ) {
	
		extract( shortcode_atts( array( 
			'text' => 'Button Text',
			'link' => '',
			'target' => '',
			'text_color' => '#ffffff',
			'text_size' => '14px',
			'background_color' => '#f84103',
			'border_color' => '',
			'border_radius' => '3px',
			'align' => '',
			'width' => 'auto',
			'padding' => '14px 20px',
			'margins' => '',
			'icon' => '',			
			'classes' => ''
		 ), $atts ) );

		// Unique Name	
		STATIC $gp_i = 0;
		$gp_i++;
		$name = 'gp_button_' . $gp_i;
		
		// Target		
		if ( $target == '_blank' ) {
			$target	= ' target="_blank"';
		} else {
			$target = '';
		}	

		// Text Color		
		if ( $text_color != '' ) {
			$text_color = 'color: ' . $text_color . ';';
		} else {
			$text_color = '';
		}	

		// Text Size		
		if ( $text_size != '' ) {
			$text_size = 'font-size: ' . $text_size . ';';
		} else {
			$text_size = '';
		}		
		
		// Background Color		
		if ( $background_color != '' ) {
			$background_color = 'background-color: ' . $background_color . ';';
		} else {
			$background_color = '';
		}			
					
		// Border Color
		if ( $border_color != '' ) {
			$border_color = 'border: 1px solid ' . $border_color . ';';
		} else {
			$border_color = '';
		}
						
		// Border Radius	
		if ( $border_radius != '' ) {
			$border_radius = 'border-radius: ' . $border_radius . ';';
		} else {
			$border_radius = '';
		}
				
		// Width	
		if ( $width != '' ) {
			$width = 'width: ' . $width . ';';
		} else {
			$width = '';
		}							
		
		// Padding 	
		if ( $padding != '' ) {
			$padding = 'padding: ' . $padding . ';';
		} else {
			$padding = '';
		}	

		// Margins 	
		if ( $margins != '' ) {
			$margins = 'margin: ' . $margins . ';';
		} else {
			$margins = '';
		}	
				
		// Icon
		if ( $icon != '' ) {
			$icon = '<i class="fa ' . $icon . '"></i>';
		} else { 
			$icon = '';
		}
		
		$button = '<span class="gp-button ' . $classes . '" style="' . $text_color . $text_size . $background_color . $border_color . $border_radius . $width . $padding . $margins . '">' . $icon . $text . '</span>';

		$out = '';
		
		$out .= '<div class="' . $align . '">';
		
		if ( $link != '' ) {
			$out  .= '<a href="' . $link . '" title="' . $text . '"' . $target . '>' . $button . '</a>';
		} else {
			$out .= $button;
		}
		
		$out .= '</div>';

		return $out;

		
	}
	
}

add_shortcode( 'button', 'ghostpool_button' );

?>
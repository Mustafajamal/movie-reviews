<?php

if ( ! function_exists( 'ghostpool_icon' ) ) {

	function ghostpool_icon( $atts, $content = null ) {
		
		extract( shortcode_atts( array( 
			'image' => 'fa-rocket',
			'link' => '',
			'target' => '',
			'size' => 'medium',
			'color' => '#fff',
			'animation' => 'no-animation',
			'scroll_animation' => 'disabled',
			'delay' => '0.1',
			'spin' => 'false',
			'background_color_1' => '#42caf6',
			'background_color_2' => '#00c0ff',
			'background_size' => '98px',
			'border_color' => '',
			'border_radius' => '100%',
			'margins' => '',
			'align' => 'aligncenter',
			'classes' => ''
		 ), $atts ) );

		// Unique Name
		STATIC $gp_i = 0;
		$gp_i++;
		$name = 'gp_icon_' . $gp_i;
		
		// Target		
		if ( $target == '_blank' ) {
			$target	= ' target="_blank"';
		} else {
			$target = '';
		}	
			
		// Size
		if ( $size == 'xsmall' ) {
			$size = ' fa-lg';
		} elseif ( $size == 'small' ) {
			$size = ' fa-2x';
		} elseif ( $size == 'medium' ) {
			$size = ' fa-3x';
		} elseif ( $size == 'large' ) {
			$size = ' fa-4x';
		} elseif ( $size == 'xlarge' ) {
			$size = ' fa-5x';
		}	

		// Color
		$color = 'color: ' . $color . ';';
				
		// Animation
		if ( $animation != 'no-animation' ) {		
			if ( $scroll_animation == 'enabled' OR $delay != '0' ) {
				$parse_animation = ' invisible';
				$parse_scroll_animation = $animation;
			} elseif ( $scroll_animation == 'disabled' OR $delay == '0' ) {
				$parse_animation = ' ' . $animation;
				$parse_scroll_animation = '';
			}	
		} else {
			$parse_animation = '';
			$parse_scroll_animation = '';
		}
		
		// Background Colors
		if ( $background_color_1 != '' && $background_color_2 != '' ) {
			$background_color = 'background: ' . $background_color_1 . '; background: -moz-linear-gradient( -45deg, ' . $background_color_1 . ' 0%, ' . $background_color_1 . ' 50%, ' . $background_color_2 . ' 51%, ' . $background_color_2 . ' 100% ); background: -webkit-linear-gradient( -45deg, ' . $background_color_1 . ' 0%,' . $background_color_1 . ' 50%,' . $background_color_2 . ' 51%,' . $background_color_2 . ' 100% ); background: -o-linear-gradient( -45deg, ' . $background_color_1 . ' 0%,' . $background_color_1 . ' 50%,' . $background_color_2 . ' 51%,' . $background_color_2 . ' 100% );';		
		} else {
			$background_color = '';
		}
		
		// Background Size
		if ( $background_size != '0' ) {
			$width = 'width: ' . $background_size . ';';
			$height = 'height: ' . $background_size . ';';
			$line_height = 'line-height: ' . $background_size . ';';
		} else {
			$width = '';
			$height = '';
			$line_height = '';
		}	
				
		// Border Color
		if ( $border_color != '' ) {
			$border_color = 'border: 1px solid ' . $border_color . ';';
		} else {
			$border_color = '';
		}
					
		// Border Radius
		if ( $border_radius != '' ) {
			$border_radius = 'border-radius: ' . $border_radius . ';';
		} else {
			$border_radius = '';
		}

		// Margins 
		if ( $margins != '' ) {
			$margins = 'margin: ' . $margins . ';';
		} else {
			$margins = '';
		}
		
		// Alignment	
		$align = ' ' . $align;
			
		// Spin
		if ( $spin == 'true' ) {
			$spin = ' fa-spin';
		} else {
			$spin = '';
		}	
		
		$out = '';
		
		if ( $link != '' ) {
			$out .= '<a href="' . $link . '" ' . $target . '>';	
		}
					
		$out .= '<span id="' . $name . '" class="gp-icon' . $align . '"><i class="fa ' . $gp_image . $size . $parse_animation . $spin . $classes . '" style="' . $background_color . $border_color . $border_radius . $width . $height . $line_height . $margins . $color . '"></i></span>';		

		if ( $link != '' ) {
			$out .= '</a>';			
		}
			
		if ( $animation != 'no-animation' ) {

			if ( $scroll_animation == 'disabled' && $delay != '0' ) {	
					
				$out .= '<script>
				jQuery( document ).ready( function(){
					jQuery( "#' . $name . ' i" ).each( function(){
						jQuery( this ).delay( ' . ( $delay * 1000 ) . ' ).queue( function( next ) {
							jQuery( this ).addClass( "' . $parse_scroll_animation . '" );
							next();
						});	
					});
				});</script>';
		
			} elseif ( $scroll_animation == 'enabled' ) {	
		
				$out .= '
				<script>
				jQuery( document ).ready( function( $ ) {
			
					function isElementInViewport( elem ) {
						var elem = jQuery( elem );
						var scrollElem = ( ( navigator.userAgent.toLowerCase().indexOf( "webkit" ) != -1 ) ? "body" : "html" );
						var viewportTop = jQuery( scrollElem ).scrollTop();
						var viewportBottom = viewportTop + jQuery( window ).height();
						var elemTop = Math.round(  elem.offset().top  );
						var elemBottom = elemTop + elem.height();
						return ( ( elemTop < viewportBottom ) && ( elemBottom > viewportTop ) );
					}

					function checkAnimation_' . $name . '() {
						var elem = jQuery( "#' . $name . ' i" );
						if ( elem.hasClass( "' . $parse_scroll_animation . '" ) ) return;
						if ( isElementInViewport( elem ) ) {
							elem.delay( '. ( $delay * 1000 ) . ' ).queue( function( next ) {
								elem.addClass( "' . $parse_scroll_animation . '" );
								next();
							});	
						}
					}

					jQuery( window ).scroll( function(){
						checkAnimation_' . $name . '();
					});
			
				});
				</script>';
			
			}
		
		}
				
		return $out;
		
	}
}

add_shortcode( 'icon', 'ghostpool_icon' );

?>
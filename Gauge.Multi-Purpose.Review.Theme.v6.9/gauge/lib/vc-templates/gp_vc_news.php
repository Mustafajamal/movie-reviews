<?php 

if ( ! function_exists( 'ghostpool_news' ) ) {

	function ghostpool_news( $atts, $content = null ) {	
		
		extract( shortcode_atts( array(
			'widget_title' => '',
			'cats' => '', 
			'post_association' => 'enabled',
			'format' => 'blog-standard',
			'size' => 'blog-standard-size',
			'orderby' => 'newest',
			'date_posted' => 'all',
			'date_modified' => 'all',				
			'filter' => 'disabled',
			'filter_cats' => '',
			'filter_date' => '',
			'filter_title' => '',					
			'filter_comment_count' => '',
			'filter_views' => '',
			'filter_date_posted' => '',
			'filter_date_modified' => '',
			'filter_cats_id' => '',										
			'per_page' => '5',
			'offset' => '',
			'featured_image' => 'enabled',
			'image_width' => '140',
			'image_height' => '140',
			'hard_crop' => true,
			'image_alignment' => 'image-align-left',
			'title_position' => 'title-next-to-thumbnail',
			'content_display' => 'excerpt',
			'excerpt_length' => '100',
			'meta_author' => '',
			'meta_date' => '',
			'meta_views' => '',
			'meta_comment_count' => '',
			'meta_cats' => '',
			'meta_tags' => '',
			'read_more_link' => 'enabled',
			'page_numbers' => 'disabled',
			'see_all' => 'disabled',
			'see_all_link' => '',
			'see_all_text' => esc_html__( 'See All News', 'gauge' ),
			'classes' => '',
		), $atts ) );
		
		global $gp, $post;

		// Temporary fix for getting hub page ID instead of custom sidebar ID
		if ( $post->post_type == 'epx_vcsb' ) {
			$GLOBALS['ghostpool_hub_id'] = $GLOBALS['ghostpool_hub_id'];
		} else {
			$GLOBALS['ghostpool_hub_id'] = get_the_ID();
		}
				
		// Detect shortcode
		$GLOBALS['ghostpool_shortcode'] = 'news';
				
		// Load page variables
		ghostpool_shortcode_options( $atts );
		ghostpool_category_variables();

		// Load scripts
		if ( $GLOBALS['ghostpool_format'] == 'blog-masonry' ) {
			wp_enqueue_script( 'gp-isotope' );
			wp_enqueue_script( 'gp-images-loaded' );
		}
		
		// Unique Name	
		STATIC $gp_i = 0;
		$gp_i++;
		$gp_name = 'gp_news_wrapper_' . $gp_i;

		// Get news page permalink if none specified
		if ( $see_all == 'enabled' && empty ( $see_all_link ) && ( get_post_meta( $GLOBALS['ghostpool_hub_id'], '_wp_page_template', true ) == 'hub-template.php' OR get_post_meta( $GLOBALS['ghostpool_hub_id'], '_wp_page_template', true ) == 'hub-review-template.php' ) ) {		
			$gp_pages = get_pages( 'child_of=' . $GLOBALS['ghostpool_hub_id'] );
			foreach ( $gp_pages as $gp_page ) {
				if ( get_post_meta( $gp_page->ID, '_wp_page_template', true ) == 'news-template.php' ) {
					$see_all_link = get_permalink( $gp_page->ID );
				}
			}	
		}
		
		// Get news posts associated with hub
		if ( $post_association == 'enabled' && ( get_post_meta( $GLOBALS['ghostpool_hub_id'], '_wp_page_template', true ) == 'hub-template.php' OR get_post_meta( $GLOBALS['ghostpool_hub_id'], '_wp_page_template', true ) == 'hub-review-template.php' ) ) {
			$GLOBALS['ghostpool_meta_query'] = array( 'relation' => 'OR', array( 'key' => 'post_association', 'value' => sprintf( ' "%s" ', $GLOBALS['ghostpool_hub_id'] ), 'compare' => 'LIKE' ), array( 'key' => '_hub_page_id', 'value' => $GLOBALS['ghostpool_hub_id'], 'compare' => '=' ) );
		} 
				
		$gp_args = array(
			'post_status'       => 'publish',
			'post_type' 		=> 'post',
			'tax_query' 		=> array(
				'relation' 		=> 'AND',
				$GLOBALS['ghostpool_post_cats'],
				array(
					'taxonomy' 	=> 'post_format',
					'field' 	=> 'slug',
					'terms' 	=> array( 'post-format-quote', 'post-format-audio', 'post-format-gallery', 'post-format-image', 'post-format-link', 'post-format-video' ),
					'operator' 	=> 'NOT IN',
				)			
			),
			'orderby' 			=> $GLOBALS['ghostpool_orderby_value'],
			'order'  			=> $GLOBALS['ghostpool_order'],	
			'meta_query' 		=> $GLOBALS['ghostpool_meta_query'],
			'meta_key' 			=> $GLOBALS['ghostpool_meta_key'],
			'posts_per_page' 	=> $GLOBALS['ghostpool_per_page'],
			'offset' 			=> $GLOBALS['ghostpool_offset'],	
			'paged' 			=> $GLOBALS['ghostpool_paged'],   
			'date_query' 	 	=> array( $GLOBALS['ghostpool_date_posted_value'], $GLOBALS['ghostpool_date_modified_value'] ),
		);

		ob_start(); $gp_query = new wp_query( $gp_args ); ?>
									
		<div id="<?php echo sanitize_html_class( $gp_name ); ?>" class="gp-blog-wrapper gp-vc-element gp-<?php echo sanitize_html_class( $GLOBALS['ghostpool_format'] ); ?> gp-<?php echo sanitize_html_class( $size ); ?> <?php echo esc_attr( $classes ); ?>"<?php if ( function_exists( 'ghostpool_data_properties' ) ) { echo ghostpool_data_properties( 'news' ); } ?>>

			<?php if ( $widget_title OR $see_all == 'enabled' ) { ?>
				<div class="gp-element-title">
					<?php if ( $widget_title ) { ?><h3><?php echo esc_attr( $widget_title ); ?></h3><?php } ?>
					<?php if ( $see_all == 'enabled' ) { ?>
						<div class="gp-see-all-link"><a href="<?php echo esc_url( $see_all_link ); ?>"><?php echo esc_attr( $see_all_text ); ?></a></div>
					<?php } ?>
					<div class="gp-element-title-line"></div>
				</div>
			<?php } ?>
			
			<?php get_template_part( 'lib/sections/filter' ); ?>
					
			<?php if ( $gp_query->have_posts() ) : ?>

				<div class="gp-inner-loop <?php echo sanitize_html_class( $gp['ajax'] ); ?>">
					
					<?php if ( $GLOBALS['ghostpool_format'] == 'blog-masonry' ) { ?><div class="gp-gutter-size"></div><?php } ?>
			
					<?php while ( $gp_query->have_posts() ) : $gp_query->the_post(); ?>

						<?php get_template_part( 'post', 'loop' ); ?>
	
					<?php endwhile; ?>
			
				</div>

				<?php if ( $page_numbers == 'enabled' ) { ?>
					<?php echo ghostpool_pagination( $gp_query->max_num_pages ); ?>
				<?php } ?>

			<?php else : ?>

				<strong class="gp-no-items-found"><?php esc_html_e( 'No items found.', 'gauge' ); ?></strong>

			<?php endif; wp_reset_postdata(); ?>
			
		</div>
				
		<?php

		$output_string = ob_get_contents();
		ob_end_clean();  		
		$GLOBALS['ghostpool_shortcode'] = null;
		return $output_string;

	}

}

add_shortcode( 'news', 'ghostpool_news' );
	
?>
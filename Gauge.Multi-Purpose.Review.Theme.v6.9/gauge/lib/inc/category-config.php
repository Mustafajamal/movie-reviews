<?php
/**
 * Category options
 *
 * @since Gauge 6.9
 */

// Enable options on hub fields 
$gp_hub_field_array = array();
if ( isset( $gp['hub_fields'] ) && ! empty( $gp['hub_fields'] ) ) {
	$GLOBALS['ghostpool_char_table'] = array();
	if ( function_exists( 'ghostpool_hub_field_characters' ) ) {
		ghostpool_hub_field_characters();
	}
	foreach( $gp['hub_fields'] as $gp_hub_field ) {
		$gp_hub_field_slug = strtr( $gp_hub_field, $GLOBALS['ghostpool_char_table'] );
		if ( function_exists( 'iconv' ) ) {
			$gp_hub_field_slug = iconv( 'UTF-8', 'UTF-8//TRANSLIT//IGNORE', $gp_hub_field_slug );
		}
		$gp_hub_field_slug = sanitize_title( $gp_hub_field_slug );
		$gp_hub_field_slug = substr( $gp_hub_field_slug, 0, 32 );	
		$gp_hub_field_slugs[] = $gp_hub_field_slug;
	}
	$gp_hub_field_array = $gp_hub_field_slugs;
}

// Taxonomy Arrays
$gp_color_array = array( 'gp_hubs', 'gp_videos' );
$gp_page_header_array = array( 'category', 'post_tag', 'gp_portfolios', 'gp_hubs', 'gp_videos' );
$gp_bg_image_array = array( 'category', 'post_tag', 'gp_portfolios', 'gp_hubs', 'gp_videos' );	
$gp_layout_array = array( 'category', 'post_tag', 'gp_portfolios', 'gp_hubs', 'gp_videos' );	
$gp_sidebar_array = array( 'category', 'post_tag', 'gp_portfolios', 'gp_hubs', 'gp_videos' );
$gp_format_array_1 = array( 'category', 'post_tag', 'gp_hubs', 'gp_videos' );
$gp_format_array_2 = array( 'gp_portfolios' );

// Enable options on hub fields 
if ( isset( $gp['hub_fields'] ) && ! empty( $gp['hub_fields'] ) ) {
	$GLOBALS['ghostpool_char_table'] = array();
	if ( function_exists( 'ghostpool_hub_field_characters' ) ) {
		ghostpool_hub_field_characters();
	}
	foreach( $gp['hub_fields'] as $gp_hub_field ) {
		$gp_hub_field_slug = strtr( $gp_hub_field, $GLOBALS['ghostpool_char_table'] );
		if ( function_exists( 'iconv' ) ) {
			$gp_hub_field_slug = iconv( 'UTF-8', 'UTF-8//TRANSLIT//IGNORE', $gp_hub_field_slug );
		}
		$gp_hub_field_slug = sanitize_title( $gp_hub_field_slug );
		$gp_hub_field_slug = substr( $gp_hub_field_slug, 0, 32 );		
		add_action( $gp_hub_field_slug . '_add_form_fields', 'ghostpool_add_tax_fields' );
		add_action( $gp_hub_field_slug . '_edit_form_fields', 'ghostpool_edit_tax_fields' );
		add_action( 'created_' . $gp_hub_field_slug, 'ghostpool_save_tax_fields' );	
		add_action( 'edited_' . $gp_hub_field_slug, 'ghostpool_save_tax_fields' );			
	}
}	

// Category Options	
$gp_options = array( 

	array( 
		'id'      => 'color',
		'name'    => esc_html__( 'Color', 'gauge' ),
		'desc'    => esc_html__( 'Select a color to associate with this category.', 'gauge' ),
		'type'    => 'color',
		'tax'     => array_merge( $gp_color_array, $gp_hub_field_array ),
		'default' => '',
	),
	
	array( 
		'id'      => 'page_header',
		'name'    => esc_html__( 'Page Header', 'gauge' ),
		'desc'    => esc_html__( 'The page header on the page.', 'gauge' ),
		'type'    => 'select',
		'tax'     => $gp_page_header_array,
		'options' => array( 
			'default' => esc_html__( 'Default', 'gauge' ), 
			'gp-large-title' => esc_html__( 'Enabled', 'gauge' ), 
			'gp-no-large-title' => esc_html__( 'Disabled', 'gauge' ), 
		),
		'default' => 'default',
	),

	array( 
		'id'      => 'bg_image',
		'name'    => esc_html__( 'Page Header Background', 'gauge' ),
		'desc'    => esc_html__( 'The background of the page header. Enter an image URL that must be uploaded to the Media Library.', 'gauge' ),
		'type'    => 'text',
		'tax'     => $gp_bg_image_array,
		'default' => '',
	),

	array( 
		'id'      => 'layout',
		'name'    => esc_html__( 'Page Layout', 'gauge' ),
		'desc'    => esc_html__( 'The page header on the page.', 'gauge' ),
		'type'    => 'select',
		'tax'     => $gp_layout_array,
		'options' => array( 
			'default' => esc_html__( 'Default', 'gauge' ), 
			'gp-left-sidebar' => esc_html__( 'Left Sidebar', 'gauge' ), 
			'gp-right-sidebar' => esc_html__( 'Right Sidebar', 'gauge' ),
			'gp-no-sidebar' => esc_html__( 'No Sidebars', 'gauge' ), 
			'gp-fullwidth' => esc_html__( 'Fullwidth', 'gauge' ),
		),
		'default' => 'default',
	),

	array( 
		'id'      => 'sidebar',
		'name'    => esc_html__( 'Sidebar', 'gauge' ),
		'desc'    => esc_html__( 'The sidebar to display.', 'gauge' ),
		'type'    => 'sidebars',
		'tax'     => $gp_sidebar_array,
		'options' => array( 
			'default' => esc_html__( 'Default', 'gauge' ),
		),
		'default' => 'default',
	),

	array( 
		'id'      => 'format',
		'name'    => esc_html__( 'Format', 'gauge' ),
		'desc'    => esc_html__( 'The format to display the items in.', 'gauge' ),
		'type'    => 'select',
		'tax'     => $gp_format_array_1,
		'options' => array( 
			'default' => esc_html__( 'Default', 'gauge' ),
			'blog-standard' => esc_html__( 'Standard', 'gauge' ),  
			'blog-large' => esc_html__( 'Large', 'gauge' ), 
			'blog-columns-1' => esc_html__( '1 Column', 'gauge' ),
			'blog-columns-2' => esc_html__( '2 Columns', 'gauge' ), 
			'blog-columns-3' => esc_html__( '3 Columns', 'gauge' ), 
			'blog-columns-4' => esc_html__( '4 Columns', 'gauge' ), 
			'blog-columns-5' => esc_html__( '5 Columns', 'gauge' ), 
			'blog-columns-6' => esc_html__( '6 Columns', 'gauge' ), 
			'blog-masonry' => esc_html__( 'Masonry', 'gauge' ), 
		),
		'default' => 'default',
	),
	
	array( 
		'id'      => 'format',
		'name'    => esc_html__( 'Format', 'gauge' ),
		'desc'    => esc_html__( 'The format to display the items in.', 'gauge' ),
		'type'    => 'select',
		'tax'     => $gp_format_array_2,
		'options' => array( 
			'default' => esc_html__( 'Default', 'gauge' ),
			'portfolio-columns-2' => esc_html__( '2 Columns', 'gauge' ), 
			'portfolio-columns-3' => esc_html__( '3 Columns', 'gauge' ), 
			'portfolio-columns-4' => esc_html__( '4 Columns', 'gauge' ), 
			'portfolio-columns-5' => esc_html__( '5 Columns', 'gauge' ), 
			'portfolio-columns-6' => esc_html__( '6 Columns', 'gauge' ), 
			'portfolio-masonry' => esc_html__( 'Masonry', 'gauge' ), 
		),
		'default' => 'default',
	),
													
); 
 
// New category options 
if ( ! function_exists( 'ghostpool_add_tax_fields' ) ) {
	function ghostpool_add_tax_fields( $gp_tag ) {		

		global $gp_options;
		
		// Get current screen
		$gp_screen = get_current_screen();

		// Get category option
		if ( isset( $gp_tag->term_id ) ) {
			$gp_term_id = $gp_tag->term_id;
			$gp_term_meta = get_option( "taxonomy_$gp_term_id" );
		} else {
			$gp_term_meta = null;
		}

		foreach ( $gp_options as $gp_option ) {
		
			switch( $gp_option['type'] ) {
			
				case 'select' :
				
					// Checking what category pages to show this option on
					$gp_add_field = false;
					foreach ( $gp_option['tax'] as $type ) {
						if ( $gp_screen->taxonomy == $type ) {
							$gp_add_field = true;
						}
					}

					if ( $gp_add_field == true ) { ?>
		
						<div class="form-field">
							<label for="category-<?php echo esc_attr( $gp_option['id'] ); ?>"><?php echo esc_attr( $gp_option['name'] ); ?></label>
							<select id="gp_term_meta_<?php echo esc_attr( $gp_option['id'] ); ?>" name="gp_term_meta[<?php echo esc_attr( $gp_option['id'] ); ?>]">
								<?php foreach ( $gp_option['options'] as $key => $option ) { ?>
									<?php if ( $gp_term_meta[$gp_option['id']] != '' ) { ?>
										<option value="<?php echo esc_attr( $key ); ?>" <?php if ( $gp_term_meta[$gp_option['id']] == $key ) { echo ' selected="selected"'; } ?>><?php echo esc_attr( $option ); ?></option>
									<?php } else { ?>
										<option value="<?php echo esc_attr( $key ); ?>" <?php if ( $gp_option['default'] == $key ) { echo ' selected="selected"'; } ?>><?php echo esc_attr( $option ); ?></option>
									<?php } ?>
								<?php } ?>
							</select>
							<p class="description"><?php echo esc_attr( $gp_option['desc'] ); ?></p>
						</div>
			
					<?php }
					
				break;

				case 'sidebars' :
			
					// Checking what category pages to show this option on
					$gp_add_field = false;
					foreach ( $gp_option['tax'] as $type ) {
						if ( $gp_screen->taxonomy == $type ) {
							$gp_add_field = true;
						}
					}

					if ( $gp_add_field == true ) { ?>
	
						<div class="form-field">
							<label for="category-<?php echo esc_attr( $gp_option['id'] ); ?>"><?php echo esc_attr( $gp_option['name'] ); ?></label>
							<select id="gp_term_meta_<?php echo esc_attr( $gp_option['id'] ); ?>" name="gp_term_meta[<?php echo esc_attr( $gp_option['id'] ); ?>]">
								
								<?php foreach ( $gp_option['options'] as $key => $option ) { ?>
									<?php if ( $gp_term_meta[$gp_option['id']] != '' ) { ?>
										<option value="<?php echo esc_attr( $key ); ?>" <?php if ( $gp_term_meta[$gp_option['id']] == $key ) { echo ' selected="selected"'; } ?>><?php echo esc_attr( $option ); ?></option>
									<?php } else { ?>
										<option value="<?php echo esc_attr( $key ); ?>" <?php if ( $gp_option['default'] == $key ) { echo ' selected="selected"'; } ?>><?php echo esc_attr( $option ); ?></option>
									<?php } ?>
								<?php } ?>
								
								<?php foreach ( $GLOBALS['wp_registered_sidebars'] as $gp_sidebar ) { ?>
									<option value="<?php echo sanitize_title( $gp_sidebar['id'] ); ?>"<?php if ( isset( $gp_term_meta[$gp_option['id']] ) && $gp_term_meta[$gp_option['id']] == $gp_sidebar['id'] ) { ?>selected="selected"<?php } ?>>
										<?php echo ucwords( $gp_sidebar['name'] ); ?>
									</option>
								<?php } ?>
							</select>
							<p class="description"><?php echo esc_attr( $gp_option['desc'] ); ?></p>
						</div>
		
					<?php } 
					
				break;
				
				case 'text' :
			
					// Checking what category pages to show this option on
					$gp_add_field = false;
					foreach ( $gp_option['tax'] as $type ) {
						if ( $gp_screen->taxonomy == $type ) {
							$gp_add_field = true;
						}
					}

					if ( $gp_add_field == true ) { ?>
	
						<div class="form-field">
							<label for="category-<?php echo esc_attr( $gp_option['id'] ); ?>"><?php echo esc_attr( $gp_option['name'] ); ?></label>
							<input name="gp_term_meta[<?php echo esc_attr( $gp_option['id'] ); ?>]" id="gp_term_meta_<?php echo esc_attr( $gp_option['id'] ); ?>" type="text" value="<?php echo esc_url( $gp_term_meta[$gp_option['id']] ? $gp_term_meta[$gp_option['id']] : '' ); ?>" />
							<p class="description"><?php echo esc_attr( $gp_option['desc'] ); ?></p>
						</div>
		
					<?php }
					
				break;

				case 'color' :

					// Load scripts
					wp_enqueue_style( 'wp-color-picker' );
					wp_enqueue_script( 'wp-color-picker' );
				
					// Checking what category pages to show this option on
					$gp_add_field = false;
					foreach ( $gp_option['tax'] as $type ) {
						if ( $gp_screen->taxonomy == $type ) {
							$gp_add_field = true;
						}
					}

					if ( $gp_add_field == true ) { ?>
		
						<div class="form-field">
							<label for="category-<?php echo esc_attr( $gp_option['id'] ); ?>"><?php echo esc_attr( $gp_option['name'] ); ?></label>
							<script>
								jQuery( document ).ready( function($){  
									$( '#gp_term_meta_<?php echo esc_attr( $gp_option["id"] ); ?>' ).wpColorPicker();
								});
							</script>
							<input name="gp_term_meta[<?php echo esc_attr( $gp_option['id'] ); ?>]" id="gp_term_meta_<?php echo esc_attr( $gp_option['id'] ); ?>" type="text" value="<?php echo esc_attr( $gp_term_meta[$gp_option['id']] ? $gp_term_meta[$gp_option['id']] : '' ); ?>" />
							<p class="description"><?php echo esc_attr( $gp_option['desc'] ); ?></p>
						</div>
			
					<?php }
					
				break;
				
			}
						
		}
		
	}
}
add_action( 'category_add_form_fields', 'ghostpool_add_tax_fields' );	
add_action( 'post_tag_add_form_fields', 'ghostpool_add_tax_fields' );
add_action( 'gp_portfolios_add_form_fields', 'ghostpool_add_tax_fields' );	
add_action( 'gp_hubs_add_form_fields', 'ghostpool_add_tax_fields' );
add_action( 'gp_videos_add_form_fields', 'ghostpool_add_tax_fields' );	

// Edit category options
if ( ! function_exists( 'ghostpool_edit_tax_fields' ) ) {
	function ghostpool_edit_tax_fields( $gp_tag ) {

		global $gp_options;

		// Get current screen
		$gp_screen = get_current_screen();

		// Get category option
		if ( isset( $gp_tag->term_id ) ) {
			$gp_term_id = $gp_tag->term_id;
			$gp_term_meta = get_option( "taxonomy_$gp_term_id" );
		} else {
			$gp_term_meta = null;
		}
		
		foreach ( $gp_options as $gp_option ) {
		
			switch( $gp_option['type'] ) {
			
				case 'select' :
				
					// Checking what category pages to show this option on
					$gp_add_field = false;
					foreach ( $gp_option['tax'] as $type ) {
						if ( $gp_screen->taxonomy == $type ) {
							$gp_add_field = true;
						}
					}

					if ( $gp_add_field == true ) { ?>
		
						<tr class="form-field">
							<th scope="row" valign="top">
								<label for="category-<?php echo esc_attr( $gp_option['id'] ); ?>"><?php echo esc_attr( $gp_option['name'] ); ?></label>
							</th>
							<td>	
								<select id="gp_term_meta_<?php echo esc_attr( $gp_option['id'] ); ?>" name="gp_term_meta[<?php echo esc_attr( $gp_option['id'] ); ?>]">
									<?php foreach ( $gp_option['options'] as $key => $option ) { ?>
										<?php if ( $gp_term_meta[$gp_option['id']] != '' ) { ?>
											<option value="<?php echo esc_attr( $key ); ?>" <?php if ( $gp_term_meta[$gp_option['id']] == $key ) { echo ' selected="selected"'; } ?>><?php echo esc_attr( $option ); ?></option>
										<?php } else { ?>
											<option value="<?php echo esc_attr( $key ); ?>" <?php if ( $gp_option['default'] == $key ) { echo ' selected="selected"'; } ?>><?php echo esc_attr( $option ); ?></option>
										<?php } ?>
									<?php } ?>
								</select>
								<p class="description"><?php echo esc_attr( $gp_option['desc'] ); ?></p>
							</td>
						</tr>
			
					<?php }
					
				break;

				case 'sidebars' :
			
					// Checking what category pages to show this option on
					$gp_add_field = false;
					foreach ( $gp_option['tax'] as $type ) {
						if ( $gp_screen->taxonomy == $type ) {
							$gp_add_field = true;
						}
					}

					if ( $gp_add_field == true ) { ?>
	
						<tr class="form-field">
							<th scope="row" valign="top">
								<label for="category-<?php echo esc_attr( $gp_option['id'] ); ?>"><?php echo esc_attr( $gp_option['name'] ); ?></label>
							</th>
							<td>	
								<select id="gp_term_meta_<?php echo esc_attr( $gp_option['id'] ); ?>" name="gp_term_meta[<?php echo esc_attr( $gp_option['id'] ); ?>]">
								
									<?php foreach ( $gp_option['options'] as $key => $option ) { ?>
										<?php if ( $gp_term_meta[$gp_option['id']] != '' ) { ?>
											<option value="<?php echo esc_attr( $key ); ?>" <?php if ( $gp_term_meta[$gp_option['id']] == $key ) { echo ' selected="selected"'; } ?>><?php echo esc_attr( $option ); ?></option>
										<?php } else { ?>
											<option value="<?php echo esc_attr( $key ); ?>" <?php if ( $gp_option['default'] == $key ) { echo ' selected="selected"'; } ?>><?php echo esc_attr( $option ); ?></option>
										<?php } ?>
									<?php } ?>
								
									<?php foreach ( $GLOBALS['wp_registered_sidebars'] as $gp_sidebar ) { ?>
										<option value="<?php echo sanitize_title( $gp_sidebar['id'] ); ?>"<?php if ( isset( $gp_term_meta[$gp_option['id']] ) && $gp_term_meta[$gp_option['id']] == $gp_sidebar['id'] ) { ?>selected="selected"<?php } ?>>
											<?php echo ucwords( $gp_sidebar['name'] ); ?>
										</option>
									<?php } ?>
								</select>
								<p class="description"><?php echo esc_attr( $gp_option['desc'] ); ?></p>
							</td>
						</tr>
		
					<?php } 
					
				break;
				
				case 'text' :
			
					// Checking what category pages to show this option on
					$gp_add_field = false;
					foreach ( $gp_option['tax'] as $type ) {
						if ( $gp_screen->taxonomy == $type ) {
							$gp_add_field = true;
						}
					}

					if ( $gp_add_field == true ) { ?>
	
						<tr class="form-field">
							<th scope="row" valign="top">
								<label for="category-<?php echo esc_attr( $gp_option['id'] ); ?>"><?php echo esc_attr( $gp_option['name'] ); ?></label>
							</th>
							<td>
								<input name="gp_term_meta[<?php echo esc_attr( $gp_option['id'] ); ?>]" id="gp_term_meta_<?php echo esc_attr( $gp_option['id'] ); ?>" type="text" value="<?php echo esc_url( $gp_term_meta[$gp_option['id']] ? $gp_term_meta[$gp_option['id']] : '' ); ?>" />
								<p class="description"><?php echo esc_attr( $gp_option['desc'] ); ?></p>
							</td>
						</tr>
		
					<?php }
					
				break;

				case 'color' :

					// Load scripts
					wp_enqueue_style( 'wp-color-picker' );
					wp_enqueue_script( 'wp-color-picker' );
					
					// Checking what category pages to show this option on
					$gp_add_field = false;
					foreach ( $gp_option['tax'] as $type ) {
						if ( $gp_screen->taxonomy == $type ) {
							$gp_add_field = true;
						}
					}

					if ( $gp_add_field == true ) { ?>
	
						<tr class="form-field">
							<th scope="row" valign="top">
								<label for="category-<?php echo esc_attr( $gp_option['id'] ); ?>"><?php echo esc_attr( $gp_option['name'] ); ?></label>
							</th>
							<td>
								<script>
									jQuery( document ).ready( function($){  
										$( '#gp_term_meta_<?php echo esc_attr( $gp_option["id"] ); ?>' ).wpColorPicker();
									});
								</script>
								<input name="gp_term_meta[<?php echo esc_attr( $gp_option['id'] ); ?>]" id="gp_term_meta_<?php echo esc_attr( $gp_option['id'] ); ?>" type="text" value="<?php echo esc_attr( $gp_term_meta[$gp_option['id']] ? $gp_term_meta[$gp_option['id']] : '' ); ?>" />
								<p class="description"><?php echo esc_attr( $gp_option['desc'] ); ?></p>
							</td>
						</tr>
		
					<?php }
					
				break;
												
			}
			
		}	
	
	}
}
add_action( 'edit_category_form_fields', 'ghostpool_edit_tax_fields' );	
add_action( 'post_tag_edit_form_fields', 'ghostpool_edit_tax_fields' );
add_action( 'gp_portfolios_edit_form_fields', 'ghostpool_edit_tax_fields' );
add_action( 'gp_hubs_edit_form_fields', 'ghostpool_edit_tax_fields' );
add_action( 'gp_videos_edit_form_fields', 'ghostpool_edit_tax_fields' );

// Save category options
if ( ! function_exists( 'ghostpool_save_tax_fields' ) ) {	
	function ghostpool_save_tax_fields( $gp_term_id ) {
		if ( isset( $_POST['gp_term_meta'] ) ) {
			$gp_term_id = $gp_term_id;
			$gp_term_meta = get_option( "taxonomy_$gp_term_id" );
			$gp_cat_keys = array_keys( $_POST['gp_term_meta'] );
				foreach ( $gp_cat_keys as $gp_key ) {
				if ( isset( $_POST['gp_term_meta'][$gp_key] ) ) {
					$gp_term_meta[$gp_key] = $_POST['gp_term_meta'][$gp_key];
				}
			}
			update_option( "taxonomy_$gp_term_id", $gp_term_meta );
		}
	}			
}
add_action( 'created_category', 'ghostpool_save_tax_fields' );		
add_action( 'edit_category', 'ghostpool_save_tax_fields' );
add_action( 'created_post_tag', 'ghostpool_save_tax_fields' ); 
add_action( 'edited_post_tag', 'ghostpool_save_tax_fields' );
add_action( 'created_gp_portfolios', 'ghostpool_save_tax_fields' );	
add_action( 'edited_gp_portfolios', 'ghostpool_save_tax_fields' );	
add_action( 'created_gp_hubs', 'ghostpool_save_tax_fields' );	
add_action( 'edited_gp_hubs', 'ghostpool_save_tax_fields' );	
add_action( 'created_gp_videos', 'ghostpool_save_tax_fields' );	
add_action( 'edited_gp_videos', 'ghostpool_save_tax_fields' );			

?>
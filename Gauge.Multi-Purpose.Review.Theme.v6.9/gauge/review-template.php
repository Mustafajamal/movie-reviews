<?php
/*
Template Name: Review
*/
get_header(); global $gp;

ghostpool_page_header( get_the_ID() );
            
?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>		

	<div id="gp-review-content-wrapper" class="gp-container<?php if ( $gp['review_first_letter_styling'] == 'enabled' ) { ?> gp-review-first-letter<?php } ?><?php if ( $GLOBALS['ghostpool_sidebar_position'] == 'top' ) { ?> gp-top-sidebar<?php } ?>">

		<div id="gp-review-content">
	
			<article <?php post_class(); ?> itemscope itemtype="http://schema.org/Article">
				
				<?php echo ghostpool_meta_data( get_the_ID() ); ?>						
	
				<?php if ( ! empty( $gp['review_subtitle'] ) ) { ?>
					<h2 class="gp-subtitle"><?php echo esc_attr( $gp['review_subtitle'] ); ?></h2>
				<?php } ?>

				<?php if ( ! empty( $gp['review_share_icons'] ) ) { ?>
					<div class="gp-share-icons"><?php echo do_shortcode( $gp['review_share_icons'] ); ?></div>
				<?php } ?>

				<div class="gp-entry-content">

					<?php get_template_part( 'lib/sections/hub-details' ); ?>
	
					<div class="gp-entry-text" itemprop="text">
						<?php the_content(); ?>
					</div>

					<?php wp_link_pages( 'before=<div class="gp-pagination gp-pagination-numbers gp-standard-pagination gp-entry-pagination"><ul class="page-numbers">&pagelink=<span class="page-numbers">%</span>&after=</ul></div>' ); ?>	
		
				</div>

				<?php if ( $gp['review_meta']['tags'] == '1' ) { ?>
					<?php the_tags( '<div class="gp-entry-tags">', ' ', '</div>' ); ?>
				<?php } ?>

			</article>

		</div>
		
		<?php if ( $GLOBALS['ghostpool_sidebar_position'] == 'top' ) {
			get_sidebar();
		} ?>
	
	</div>
	
	<?php get_template_part( 'lib/sections/review', 'results' ); ?>
			
	<div id="gp-content-wrapper" class="<?php if ( $GLOBALS['ghostpool_layout'] != 'gp-fullwidth' ) { ?> gp-container<?php } ?><?php if ( $GLOBALS['ghostpool_sidebar_position'] == 'top' ) { ?> gp-top-sidebar<?php } ?>">

		<div id="gp-content">

			<?php if ( $gp['review_author_info'] == 'enabled' ) { ?>
				<?php get_template_part( 'lib/sections/author', 'info' ); ?>
			<?php } ?>

			<?php if ( $gp['review_related_items'] == 'enabled' ) { ?>
				<?php get_template_part( 'lib/sections/related', 'items' ); ?>
			<?php } ?>

			<?php if ( $GLOBALS['ghostpool_user_rating_box'] == 'enabled' && ( $GLOBALS['ghostpool_layout'] == 'gp-no-sidebar' OR $GLOBALS['ghostpool_layout'] == 'gp-fullwidth' ) ) { ?>
				<?php get_template_part( 'lib/sections/user-rating-box' ); ?>
			<?php } ?>
						
			<?php comments_template(); ?>

		</div>
		
		<?php if ( $GLOBALS['ghostpool_sidebar_position'] == 'bottom' ) { ?>
			<?php get_sidebar(); ?>
		<?php } ?>
			
	</div>
									
<?php endwhile; endif; ?>			

<?php get_footer(); ?>
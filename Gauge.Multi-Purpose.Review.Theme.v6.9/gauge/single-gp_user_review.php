<?php get_header(); global $gp; 

// Page header
ghostpool_page_header( get_the_ID() );

// Load page variables		
ghostpool_loop_variables();

// Get hub ID
ghostpool_get_hub_id( get_the_ID() ); 

?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<div id="gp-content-wrapper"<?php if ( $GLOBALS['ghostpool_layout'] != 'gp-fullwidth' ) { ?> class="gp-container"<?php } ?>>

		<div id="gp-content">
		
			<article <?php post_class(); ?> itemscope itemtype="http://schema.org/Article">

				<header class="gp-entry-header">

					<h1 class="gp-entry-title" itemprop="headline"><?php the_title(); ?></h1>

					<h2 class="gp-subtitle">
						<?php esc_html_e( 'User Rating:' , 'gauge' ) ?> 
						<span class="user-review-rating" itemscope itemtype="http://schema.org/Review">
							<span itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
								<meta itemprop="worstRating" content="1">
								<meta itemprop="bestRating" content="<?php echo floatval( $gp['review_rating_number'] ); ?>">
								<span itemprop="ratingValue"><?php echo floatval( get_post_meta( get_the_ID(), '_user_review_rating', true ) ); ?></span>
							</span>
							<meta itemprop="itemReviewed" content="<?php the_title_attribute( array( 'post' => $GLOBALS['ghostpool_hub_id'] ) ); ?>">
							<meta itemprop="author" content="<?php echo the_author_meta( 'display_name', $post->post_author ); ?>">
						</span>
					</h2>
								
					<?php get_template_part( 'lib/sections/entry', 'meta' ); ?>

					<?php if ( ! empty( $gp['post_share_icons'] ) ) { ?>
						<div class="gp-share-icons"><?php echo do_shortcode( $gp['post_share_icons'] ); ?></div>
					<?php } ?>

				</header>
		
				<?php if ( has_post_thumbnail() && $GLOBALS['ghostpool_featured_image'] == 'enabled' ) { ?>

					<div class="gp-post-thumbnail gp-entry-featured">
					
						<div class="gp-<?php echo sanitize_html_class( $GLOBALS['ghostpool_image_alignment'] ); ?>">

							<?php $gp_image = aq_resize( wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ) ), $GLOBALS['ghostpool_image_width'], $GLOBALS['ghostpool_image_height'], $GLOBALS['ghostpool_hard_crop'], false, true ); ?>
							<?php if ( $gp['retina'] == 'gp-retina' ) {
								$gp_retina = aq_resize( wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ) ), $GLOBALS['ghostpool_image_width'] * 2, $GLOBALS['ghostpool_image_height'] * 2, $GLOBALS['ghostpool_hard_crop'], true, true );
							} else {
								$gp_retina = '';
							} ?>

							<img src="<?php echo esc_url( $gp_image[0] ); ?>" data-rel="<?php echo esc_url( $gp_retina ); ?>" width="<?php echo absint( $gp_image[1] ); ?>" height="<?php echo absint( $gp_image[2] ); ?>" alt="<?php if ( get_post_meta( get_post_thumbnail_id(), '_wp_attachment_image_alt', true ) ) { echo esc_attr( get_post_meta( get_post_thumbnail_id(), '_wp_attachment_image_alt', true ) ); } else { the_title_attribute(); } ?>" class="gp-post-image" />

						</div>
							
					</div>

				<?php } ?>
				
				<div class="gp-entry-content gp-<?php echo sanitize_html_class( $GLOBALS['ghostpool_image_alignment'] ); ?>">
					
					<div class="gp-entry-text" itemprop="text"><?php the_content(); ?></div>
	
					<?php wp_link_pages( 'before=<div class="gp-pagination gp-pagination-numbers gp-standard-pagination gp-entry-pagination"><ul class="page-numbers">&pagelink=<span class="page-numbers">%</span>&after=</ul></div>' ); ?>
		
				</div>

				<?php if ( $GLOBALS['ghostpool_meta_tags'] == '1' ) { ?>
					<?php the_tags( '<div class="gp-entry-tags">', ' ', '</div>' ); ?>
				<?php } ?>

				<?php if ( $gp['post_author_info'] == 'enabled' && ! get_post_meta( get_the_ID(), '_user_review_name', true ) ) { ?>
					<?php get_template_part( 'lib/sections/author', 'info' ); ?>
				<?php } ?>

				<?php if ( $gp['post_related_items'] == 'enabled' ) { ?>
					<?php get_template_part( 'lib/sections/related', 'items' ); ?>
				<?php } ?>

				<?php comments_template(); ?>

			</article>	

		</div>

		<?php get_sidebar(); ?>

	</div>
	
	<?php endwhile; endif; ?>	

<?php get_footer(); ?>
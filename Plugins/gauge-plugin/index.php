<?php
/*
Plugin Name: Gauge Plugin
Plugin URI: 
Description: A required plugin for the Gauge theme you purchased from ThemeForest. It includes a number of features that you can still use if you switch to another theme.
Version: 2.0
Author: GhostPool
Author URI: http://themeforest.net/user/GhostPool/portfolio?ref=GhostPool
License: You should have purchased a license from ThemeForest.net
Text Domain: gauge
*/

if ( ! class_exists( 'GhostPool_Gauge' ) ) {

	class GhostPool_Gauge {

		public function __construct() {

			// Load plugin translations
			add_action( 'plugins_loaded', array( &$this, 'ghostpool_plugin_load_textdomain' ) );

			if ( ! class_exists( 'GhostPool_Custom_Sidebars' ) ) {
				require_once( sprintf( "%s/custom-sidebars/custom-sidebars.php", dirname( __FILE__ ) ) );
			}

			if ( ! post_type_exists( 'gp_portfolio' ) && ! class_exists( 'GhostPool_Portfolios' ) ) {
				require_once( sprintf( "%s/post-types/portfolio-tax.php", dirname( __FILE__ ) ) );
				$GhostPool_Portfolios = new GhostPool_Portfolios();
			}

			if ( ! post_type_exists( 'gp_slide' ) && ! class_exists( 'GhostPool_Slides' ) ) {
				require_once( sprintf( "%s/post-types/slide-tax.php", dirname( __FILE__ ) ) );
				$GhostPool_Slides = new Ghostpool_Slides();
			}

			if ( ! post_type_exists( 'gp_user_review' ) && ! class_exists( 'GhostPool_User_Reviews' ) ) {
				require_once( sprintf( "%s/post-types/user-review-tax.php", dirname( __FILE__ ) ) );
				$GhostPool_User_Reviews = new GhostPool_User_Reviews();
			}
			
			if ( ! class_exists( 'GhostPool_Hubs' ) ) {
				require_once( sprintf( "%s/post-types/hub-tax.php", dirname( __FILE__ ) ) );
				$GhostPool_Hubs = new GhostPool_Hubs();
			}

			if ( ! class_exists( 'GhostPool_Videos' ) ) {
				require_once( sprintf( "%s/post-types/video-tax.php", dirname( __FILE__ ) ) );
				$GhostPool_Videos = new GhostPool_Videos();
			}	
																		
		} 
		
		public static function ghostpool_activate() {} 		
		
		public static function ghostpool_deactivate() {}
		
		public function ghostpool_plugin_load_textdomain() {
			load_plugin_textdomain( 'gauge', false, trailingslashit( WP_LANG_DIR ) . 'plugins/' );
			load_plugin_textdomain( 'gauge', false, plugin_basename( dirname( __FILE__ ) ) . '/languages' );
		}
				
	}
	
}

// User registration emails
$gp_theme_variable = get_option( 'gp' );
if ( ! function_exists( 'wp_new_user_notification' ) && ! function_exists( 'bp_is_active' ) && ( isset ( $gp_theme_variable['popup_box'] ) && $gp_theme_variable['popup_box'] == 'enabled' ) ) {
	function wp_new_user_notification( $gp_user_id, $gp_deprecated = null, $gp_notify = 'both' ) {

		if ( $gp_deprecated !== null ) {
			_deprecated_argument( __FUNCTION__, '4.3.1' );
		}
	
		global $wpdb;
		$gp_user = get_userdata( $gp_user_id );
		
		$gp_user_login = stripslashes( $gp_user->user_login );
		$gp_user_email = stripslashes( $gp_user->user_email );

		// The blogname option is escaped with esc_html on the way into the database in sanitize_option
		// we want to reverse this for the plain text arena of emails.
		$gp_blogname = wp_specialchars_decode( get_option( 'blogname' ), ENT_QUOTES );
		
		// Admin email
		$gp_message  = sprintf( esc_html__( 'New user registration on your blog %s:', 'gauge' ), $gp_blogname ) . "\r\n\r\n";
		$gp_message .= sprintf( esc_html__( 'Username: %s', 'gauge' ), $gp_user_login ) . "\r\n\r\n";
		$gp_message .= sprintf( esc_html__( 'Email: %s', 'gauge' ), $gp_user_email ) . "\r\n";
		$gp_message = apply_filters( 'gp_registration_notice_message', $gp_message, $gp_blogname, $gp_user_login, $gp_user_email );
		@wp_mail( get_option( 'admin_email' ), sprintf( apply_filters( 'gp_registration_notice_subject', esc_html__( '[%s] New User Registration', 'gauge' ), $gp_blogname ), $gp_blogname ), $gp_message );

		if ( 'admin' === $gp_notify || empty( $gp_notify ) ) {
			return;
		}
		
		// User email
		$gp_message  = esc_html__( 'Hi there,', 'gauge' ) . "\r\n\r\n";
		$gp_message .= sprintf( esc_html__( 'Welcome to %s.', 'gauge' ), $gp_blogname ) . "\r\n\r\n";
		$gp_message .= sprintf( esc_html__( 'Username: %s', 'gauge' ), $gp_user_login ) . "\r\n";
		$gp_message .= esc_html__( 'Password: [use the password you entered when signing up]', 'gauge' ) . "\r\n\r\n";
		$gp_message .= 'Please login at ' . home_url( '/#login' ) . "\r\n\r\n";	
		$gp_message = apply_filters( 'gp_registered_user_message', $gp_message, $gp_blogname, $gp_user_login, $gp_user_email );
		wp_mail( $gp_user_email, sprintf( apply_filters( 'gp_registered_user_subject', esc_html__( '[%s] Your username and password', 'gauge' ), $gp_blogname ), $gp_blogname ), $gp_message );

	}
}

// Active/deactivate plugin
if ( class_exists( 'GhostPool_Gauge' ) ) {

	register_activation_hook( __FILE__, array( 'GhostPool_Gauge', 'ghostpool_activate' ) );
	register_deactivation_hook( __FILE__, array( 'GhostPool_Gauge', 'ghostpool_deactivate' ) );

	$ghostpool_plugin = new GhostPool_Gauge();

}

?>
<?php

if ( ! class_exists( 'GhostPool_Hubs' ) ) {

	class GhostPool_Hubs {

		public function __construct() {
			add_action( 'init', array( &$this, 'ghostpool_post_type_hub' ), 1 );
		}
			
		public function ghostpool_post_type_hub() {
		
			global $gp;
			
			if ( ! isset( $gp['hub_cat_slug'] ) ) {
				$gp['hub_cat_slug'] = 'hubs';
			}
			
			/*--------------------------------------------------------------
			Hub Categories Taxonomy
			--------------------------------------------------------------*/
			
			register_taxonomy( 'gp_hubs', 'page', array( 
				'labels' => array( 
					'name' => esc_html__( 'Hub Categories', 'gauge' ),
					'singular_name' => esc_html__( 'Hub Category', 'gauge' ),
					'all_items' => esc_html__( 'All Hub Categories', 'gauge' ),
					'add_new' => _x( 'Add New', 'hub', 'gauge' ),
					'add_new_item' => esc_html__( 'Add New Hub Category', 'gauge' ),
					'edit_item' => esc_html__( 'Edit Hub Category', 'gauge' ),
					'new_item' => esc_html__( 'New Hub Category', 'gauge' ),
					'view_item' => esc_html__( 'View Hub Category', 'gauge' ),
					'search_items' => esc_html__( 'Search Hub Categories', 'gauge' ),
					'menu_name' => esc_html__( 'Hub Categories', 'gauge' )
				 ),
				'show_in_nav_menus' => true,
				'hierarchical' => true,
				'show_admin_column' => true,
				'rewrite' => array( 'slug' => sanitize_title( $gp['hub_cat_slug'] ) )
			 ) );

			register_taxonomy_for_object_type( 'gp_hubs', 'page' );


			/*--------------------------------------------------------------
			Hub Field Taxonomies
			--------------------------------------------------------------*/
			
			if ( isset( $gp['hub_fields'] ) && ! empty( $gp['hub_fields'] ) ) {
			
				$GLOBALS['ghostpool_char_table'] = array();

				// Support for foreign characters
				if ( function_exists( 'ghostpool_hub_field_characters' ) ) {
					ghostpool_hub_field_characters();
				}	
				
				foreach( $gp['hub_fields'] as $gp_hub_field ) {
	
					$gp_hub_field_slug = strtr( $gp_hub_field, $GLOBALS['ghostpool_char_table'] );
					if ( function_exists( 'iconv' ) ) {
						$gp_hub_field_slug = iconv( 'UTF-8', 'UTF-8//TRANSLIT//IGNORE', $gp_hub_field_slug );
					}
					$gp_hub_field_slug = sanitize_title( $gp_hub_field_slug );
					$gp_hub_field_slug = substr( $gp_hub_field_slug, 0, 32 );
					
					register_taxonomy( $gp_hub_field_slug, 'page', array(
						'labels' => array(
							'name' => $gp_hub_field,
							'singular_name' => $gp_hub_field,
							'all_items' => esc_html__( 'All ', 'gauge' ) . $gp_hub_field,
							'add_new' => _x( 'Add New', 'hub', 'gauge' ),
							'add_new' => _x( 'Add New', 'hub', 'gauge' ),
							'add_new_item' => esc_html__( 'Add New ', 'gauge' ) . $gp_hub_field,
							'edit_item' => esc_html__( 'Edit ', 'gauge' ) . $gp_hub_field,
							'new_item' => esc_html__( 'New ', 'gauge' ) . $gp_hub_field,
							'view_item' => esc_html__( 'View ', 'gauge' ) . $gp_hub_field,
							'search_items' => esc_html__( 'Search ', 'gauge' ) . $gp_hub_field,
							'menu_name' => $gp_hub_field,
						),
					) );
					
					register_taxonomy_for_object_type( $gp_hub_field_slug, 'page' );

				}
				
			}

		}

	}

}

?>
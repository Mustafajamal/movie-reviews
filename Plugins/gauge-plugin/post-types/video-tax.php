<?php

if ( ! class_exists( 'GhostPool_Videos' ) ) {

	class GhostPool_Videos {

		public function __construct() {
			add_action( 'init', array( &$this, 'ghostpool_post_type_video' ), 1 );
		}
			
		public function ghostpool_post_type_video() {
		
			global $gp;
			
			if ( ! isset( $gp['video_cat_slug'] ) ) {
				$gp['video_cat_slug'] = 'videos';
			}
			
			/*--------------------------------------------------------------
			Videos Categories Taxonomy
			--------------------------------------------------------------*/
			
			register_taxonomy( 'gp_videos', 'post', array( 
				'labels' => array( 
					'name' => esc_html__( 'Video Categories', 'gauge' ),
					'singular_name' => esc_html__( 'Video Category', 'gauge' ),
					'all_items' => esc_html__( 'All Video Categories', 'gauge' ),
					'add_new' => _x( 'Add New', 'video', 'gauge' ),
					'add_new_item' => esc_html__( 'Add New Video Category', 'gauge' ),
					'edit_item' => esc_html__( 'Edit Video Category', 'gauge' ),
					'new_item' => esc_html__( 'New Video Category', 'gauge' ),
					'view_item' => esc_html__( 'View Video Category', 'gauge' ),
					'search_items' => esc_html__( 'Search Video Categories', 'gauge' ),
					'menu_name' => esc_html__( 'Video Categories', 'gauge' )
				 ),
				'show_in_nav_menus' => true,
				'hierarchical' => true,
				'show_admin_column' => true,
				'rewrite' => array( 'slug' => sanitize_title( $gp['video_cat_slug'] ) )
			 ) );

			register_taxonomy_for_object_type( 'gp_videos', 'post' );

		}
		
	}

}

?>
<?php

/*
Plugin Name: Slate Admin Theme
Plugin URI: http://sevenbold.com/wordpress/
Description: A clean, simplified WordPress Admin theme
Author: Ryan Sommers
Version: 1.1.8
Author URI: http://sevenbold.com/
*/

function slate_files() {

	$has_role = is_user_has_role("critics");
	if($has_role)
	{	
  		wp_enqueue_style( 'slate-admin-theme', plugins_url('css/slate.css', __FILE__), array(), time() );
  		wp_enqueue_script( 'slate', plugins_url( "js/slate.js", __FILE__ ), array( 'jquery' ), '1.1.8' );
	}
	$has_role1 = is_user_has_role("member");
	if($has_role1)
	{
  		wp_enqueue_style( 'slate-admin-theme', plugins_url('css/slate.css', __FILE__), array(), time() );
  		wp_enqueue_script( 'slate', plugins_url( "js/slate.js", __FILE__ ), array( 'jquery' ), '1.1.8' );		
	}
}
add_action( 'admin_enqueue_scripts', 'slate_files' );
add_action( 'login_enqueue_scripts', 'slate_files' );

function slate_add_editor_styles() {

	$has_role = is_user_has_role("critics");
	if($has_role)
	{
    	add_editor_style( plugins_url('css/editor-style.css', __FILE__ ) );
	}
	$has_role1 = is_user_has_role("member");
	if($has_role1)
	{
		add_editor_style( plugins_url('css/editor-style.css', __FILE__ ) );
	}
}
add_action( 'after_setup_theme', 'slate_add_editor_styles' );

add_filter('admin_footer_text', 'slate_admin_footer_text_output');
function slate_admin_footer_text_output($text) {
	$text = 'WordPress Admin By Logics Buffer <a href="http://logicsbuffer.com/" target="_blank">Logics Buffer</a>';
  return $text;
}

add_action( 'admin_head', 'slate_colors' );
add_action( 'login_head', 'slate_colors' );
function slate_colors() {

$has_role = is_user_has_role("critics");
if($has_role)
{
	//The logged in user has the "author" role
	include( 'css/dynamic.php' );
}
	$has_role1 = is_user_has_role("member");
	if($has_role1)
	{
	//The logged in user has the "author" role
	include( 'css/dynamic.php' );
}
	

}
function slate_get_user_admin_color(){


	$has_role = is_user_has_role("critics");
	if($has_role)
	{
	//The logged in user has the "author" role


	$user_id = get_current_user_id();
	$user_info = get_userdata($user_id);
	if ( !( $user_info instanceof WP_User ) ) {
		return; 
	}
	$user_admin_color = $user_info->admin_color;
	return $user_admin_color;
	}
}
//User Role check
function is_user_has_role( $role, $user_id = null ) {
   
    if ( is_numeric( $user_id ) ) {
        $user = get_userdata( $user_id );
    }
    else {
        $user = wp_get_current_user();
    }
    if ( !empty( $user ) ) {
        return in_array( $role, (array) $user->roles );
    }
    else
    {
    	return false;
    }
}

//Page Slug Body Class
function add_slug_body_class( $classes ) {
global $post;
if ( isset( $post ) ) {
$classes[] = $post->post_type . '-' . $post->post_name;
}
return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );


// Remove the hyphen before the post state
add_filter( 'display_post_states', 'slate_post_state' );
function slate_post_state( $post_states ) {
	if ( !empty($post_states) ) {
		$state_count = count($post_states);
		$i = 0;
		foreach ( $post_states as $state ) {
			++$i;
			( $i == $state_count ) ? $sep = '' : $sep = '';
			echo "<span class='post-state'>$state$sep</span>";
		}
	}
}

// function kristin_send_email( $user_id, $new_role ) {
// 	$user_info = get_userdata( $user_id );
// 	$to = $user_info->user_email; 
// 	$subject = 'Role changed';
// 	$message = "Hello " .$user_info->display_name . " your role has changed, now you are " . $new_role;
// 	wp_mail( $to, $subject, $message);
// }
// add_action( 'set_user_role', 'kristin_send_email' );

function user_role_update( $user_id, $new_role ) {
        $site_url = get_bloginfo('wpurl');
        $user_info = get_userdata( $user_id );
        $to = $user_info->user_email;
        $subject = "Role changed: ".$site_url."";
        $message = "Hello " .$user_info->display_name . " your role has changed on ".$site_url.", congratulations you are now an " . $new_role;
        wp_mail($to, $subject, $message);
}
add_action( 'set_user_role', 'user_role_update', 10, 2);





?>
